import React from 'react'
import {
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  Flex,
} from '@chakra-ui/react'

export const NoOpportunity = () => {
  return (
    <Flex justifyContent="center" width="100%">
      <Alert
        status="warning"
        variant="subtle"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        textAlign="center"
        height="40vh"
        colorScheme="blue"
        border="3px solid"
        borderColor="PersonIcons.border"
        boxShadow="xl"
        borderRadius="lg"
        width="50%"
        bgColor="menuItem.bgColor"
      >
        <AlertIcon boxSize="40px" mr={0} color="general.blue" />
        <AlertTitle mt={4} mb={1} fontSize="lg">
          No Opportunities Availables!
        </AlertTitle>
        <AlertDescription maxWidth="sm">
          There are no opportunities available currently, when new opportunity
          come it will be shown to you!
        </AlertDescription>
      </Alert>
    </Flex>
  )
}
