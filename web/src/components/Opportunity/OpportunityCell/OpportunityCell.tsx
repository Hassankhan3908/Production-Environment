import type { FindOpportunityById } from 'types/graphql'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import Opportunity from 'src/components/Opportunity/Opportunity'

export const QUERY = gql`
  query FindOpportunityById($id: String!) {
    opportunity: opportunity(id: $id) {
      id
      account_id_fk
      owner_id_fk
      projected_amt
      system_amt
      projected_close_by_date
      system_date
      predicted_outcome
      system_outcome
      prediction_score
      creation_date
      mkt_nrcr_days
      sls_nrcr_days
      age
      pct_actvty_complete
      num_actvty
      num_sls_actvty
      num_mkt_actvty
      poc_duration
      industry_type
      fg_industry_type
      tasks_total
      tasks_completed
      tasks_avg_weekly_rate
      tasks_avg_daily_rate
      tasks_trend_percentage
      engagement_score
      engagement_daily_streak
      engagement_weekly_streak
      salesforce_closing_date
      signature_id_fk
      opportunity_name
      opportunity_source
      ui_opportunity_account {
        display_name
        ui_account_attribs {
          attrib_name
          attrib_value
        }
      }
      ui_opportunity_incumbent {
        display_name
      }
      ui_opportunity_competitor {
        display_name
      }

      ui_dna_signature {
        display_name
        avg_deal_size
        avg_nrtr_days
        ui_power_score {
          name
          score
          type
        }
      }
      ui_task {
        display_name
        priority
        display_status
        due
        due_in_days
      }
      ui_opportunity_recommendations {
        recommendation
      }
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Opportunity not found</div>

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({
  opportunity,
}: CellSuccessProps<FindOpportunityById>) => {
  return <Opportunity opportunity={opportunity} />
}
