import { Link, routes } from '@redwoodjs/router'
import React from 'react'
import { Box, Flex } from '@chakra-ui/react'

import { Divider, HStack, Icon, Text, VStack } from '@chakra-ui/react'
import { StarsBox } from 'src/common/components'
import { ArrowRightIcon, TaskStatusIcon } from 'src/assets/icons'

import { CurrencyText } from 'src/common/components/general/number-text/CurrencyText'
import { DateFormatter } from 'src/common/utils/DateFormatter'

import { FindFilteredOpportunities } from 'types/graphql'
import { CellSuccessProps } from '@redwoodjs/web'
import { NoOpportunity } from '../NoOpportunity/NoOpportunity'

const FilteredOpportunities = ({
  filterOpportunities,
}: CellSuccessProps<FindFilteredOpportunities>) => {
  const { opportunities } = filterOpportunities
  return (
    <>
      <Flex borderWidth="1px" flexWrap="wrap" w="100%">
        {opportunities?.length ? (
          opportunities.map((opportunity, index) => {
            return (
              <Box w="25%" minW="256px" p="15px" key={index}>
                <Link
                  to={routes.opportunity({ id: opportunity.id })}
                  key={index}
                >
                  <VStack bgColor="white" p={4} shadow="md" borderRadius="md">
                    <HStack alignSelf="stretch">
                      {/* <Text bgColor="gray.300" py={2} px={4} borderRadius="md">
                        {opportunity.opportunity_name ||
                          opportunity.ui_opportunity_account?.display_name}
                      </Text> */}
                      <VStack alignItems="flex-start">
                        <Text fontWeight="bold">
                          {opportunity.opportunity_name ||
                            opportunity.ui_opportunity_account?.display_name}
                        </Text>
                        <Text m="0px !important" fontSize="sm" pb={2}>
                          {opportunity.ui_opportunity_account?.display_name}
                        </Text>
                      </VStack>
                    </HStack>
                    <Divider />
                    <VStack width="100%" alignItems="flex-start">
                      <HStack
                        alignSelf="stretch"
                        justifyContent="space-between"
                      >
                        <Text
                          color="general.gray"
                          fontSize="small"
                          fontWeight="semibold"
                        >
                          Opportunity Amount
                        </Text>
                        <Text fontWeight="bold" fontSize="smaller">
                          <CurrencyText
                            value={opportunity.projected_amt}
                            numberType="long"
                          />
                        </Text>
                      </HStack>
                      <HStack
                        alignSelf="stretch"
                        justifyContent="space-between"
                      >
                        <Text
                          color="general.gray"
                          fontSize="small"
                          fontWeight="semibold"
                        >
                          Close by Date
                        </Text>
                        <Text fontWeight="bold" fontSize="smaller">
                          {DateFormatter(opportunity.projected_close_by_date)}
                        </Text>
                      </HStack>
                      <HStack
                        alignSelf="stretch"
                        justifyContent="space-between"
                      >
                        <Text
                          color="general.gray"
                          fontSize="small"
                          fontWeight="semibold"
                        >
                          % Tasks Complete
                        </Text>
                        <Text
                          color="general.blue"
                          fontWeight="bold"
                          fontSize="smaller"
                        >
                          <Icon
                            fontSize="x-large"
                            position="relative"
                            top={1}
                            left={1}
                          >
                            <TaskStatusIcon />
                          </Icon>
                          {opportunity.pct_actvty_complete}%
                        </Text>
                      </HStack>
                      <HStack
                        mb={2}
                        alignSelf="stretch"
                        justifyContent="space-between"
                      >
                        <Text
                          color="general.gray"
                          fontSize="small"
                          fontWeight="semibold"
                        >
                          Predicted Outcome
                        </Text>
                        <StarsBox value={opportunity.prediction_score} />
                      </HStack>
                      <HStack
                        mb={2}
                        alignSelf="stretch"
                        justifyContent="space-between"
                      ></HStack>
                      <Text
                        color="general.blue"
                        fontWeight="semibold"
                        fontSize="smaller"
                      >
                        View Details
                        <Icon ml={2} width={3} height={3} viewBox="0 0 11 11">
                          <ArrowRightIcon />
                        </Icon>
                      </Text>
                    </VStack>
                  </VStack>
                </Link>
              </Box>
            )
          })
        ) : (
          <NoOpportunity />
        )}
      </Flex>
    </>
  )
}

export default FilteredOpportunities
