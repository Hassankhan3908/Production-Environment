import { Success } from './FilteredOpportunitiesCell'
import type { CellProps } from '@redwoodjs/web'
import type {
  FindFilteredOpportunities,
  FindFilteredOpportunitiesVariables,
} from 'types/graphql'

type SuccessType = typeof Success

export * from './FilteredOpportunitiesCell'

type CellInputs = CellProps<
  SuccessType,
  FindFilteredOpportunities,
  FindFilteredOpportunitiesVariables
>

export default function (props: CellInputs): ReturnType<SuccessType>
