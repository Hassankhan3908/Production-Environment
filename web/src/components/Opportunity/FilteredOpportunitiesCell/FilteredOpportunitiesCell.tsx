import type { FindFilteredOpportunities } from 'types/graphql'
import { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import { Link, routes } from '@redwoodjs/router'

import React, { useEffect, useState } from 'react'

import FilteredOpportunities from '../FilteredOpportunities/FIlteredOpportunities'
import {
  Pagination,
  PaginationButtonFirstPage,
  PaginationButtonLastPage,
  PaginationButtonNextPage,
  PaginationButtonPrevPage,
  PaginationInfo,
} from 'src/common/components'

export const QUERY = gql`
  query FindFilteredOpportunities($page: Int, $sfId: String, $theater: String) {
    filterOpportunities(page: $page, sfId: $sfId, theater: $theater) {
      opportunities {
        id
        projected_amt
        system_amt
        projected_close_by_date
        system_date
        predicted_outcome
        system_outcome
        prediction_score
        creation_date
        mkt_nrcr_days
        sls_nrcr_days
        age
        pct_actvty_complete
        num_actvty
        num_sls_actvty
        num_mkt_actvty
        poc_duration
        industry_type
        fg_industry_type
        tasks_total
        tasks_completed
        tasks_avg_weekly_rate
        tasks_avg_daily_rate
        tasks_trend_percentage
        engagement_score
        engagement_daily_streak
        engagement_weekly_streak
        salesforce_closing_date
        signature_id_fk
        opportunity_name
        opportunity_source
        ui_opportunity_account {
          display_name
        }
      }
      count
    }
  }
`
export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No opportunities yet. '}
      <Link to={routes.newOpportunity()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({
  filterOpportunities,
  loading,
  variables,
  refetch,
}: CellSuccessProps<FindFilteredOpportunities>) => {
  const { count } = filterOpportunities
  const setPage = (page: number) => {
    refetch({ ...variables, page })
  }
  return (
    <>
      <FilteredOpportunities filterOpportunities={filterOpportunities} />
      <Pagination
        isLoadingPage={loading}
        setPage={setPage}
        page={variables.page || 1}
        pageSize={8}
        totalItems={count}
      >
        <PaginationButtonFirstPage />
        <PaginationButtonPrevPage />
        <PaginationInfo flex="1" />
        <PaginationButtonNextPage />
        <PaginationButtonLastPage />
      </Pagination>
    </>
  )
}
