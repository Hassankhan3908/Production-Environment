import type { FindOpportunities } from 'types/graphql'
import { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import { Link, routes } from '@redwoodjs/router'

import React, { useState, useEffect } from 'react'
import { useAuth } from '@redwoodjs/auth'

import { MiniHeader, ListFilters } from 'src/common/components'
import { OppurtunityIcon } from 'src/assets/icons'
import FilteredOpportunitiesCell from '../FilteredOpportunitiesCell'
export const QUERY = gql`
  query FindOpportunities($id: String!) {
    owner(id: $id) {
      family_name
      id
      first_given_name
      org_tree
    }
    accountTheaters {
      attrib_value
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No opportunities yet. '}
      <Link to={routes.newOpportunity()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({
  owner,
  accountTheaters,
}: CellSuccessProps<FindOpportunities>) => {
  const { userMetadata } = useAuth()

  const [page, setPage] = useState(1)
  const [sort, setSort] = useState('CLOSE_DATE')
  const [id, setId] = useState(userMetadata?.sub?.replace('salesforce|', ''))
  const [theater, setTheater] = useState('')
  useEffect(() => {
    setPage(1)
  }, [sort, id, theater])

  return (
    <>
      <MiniHeader title="Opportunities" icon={<OppurtunityIcon />} />
      <ListFilters
        setId={setId}
        id={id}
        setSort={setSort}
        sort={sort}
        user={owner}
        theater={theater}
        setTheater={setTheater}
        listOfTheaters={accountTheaters}
        orgTree={[owner?.org_tree]}
      />
      <FilteredOpportunitiesCell
        sfId={''}
        page={page}
        theater={theater || ''}
      />
    </>
  )
}
