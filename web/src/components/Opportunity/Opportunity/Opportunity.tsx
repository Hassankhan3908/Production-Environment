import { Link } from '@redwoodjs/router'
import {
  Spinner,
  HStack,
  VStack,
  Text,
  Flex,
  Box,
  SimpleGrid,
  Heading,
  Icon,
} from '@chakra-ui/react'
import {
  BreadcrumbComponent,
  TasksSummary,
  OpportunityEngagement,
  ParentContainer,
  IndividualOverviewContainer,
  PowerScoresComponent,
  CurrencyText,
  TotalTasksBar,
  TaskInfo,
  SummaryComponent,
} from 'src/common/components'

import { DollarIconGray, FlagIconGray } from 'src/assets/icons'
import { DateFormatterLastDayOfMonth } from 'src/common/utils/DateFormatter'
import { FindOpportunityById } from 'types/graphql'
import { CellSuccessProps } from '@redwoodjs/web'

const Opportunity = ({
  opportunity,
}: CellSuccessProps<FindOpportunityById>) => {
  const powerScores = opportunity?.ui_dna_signature?.ui_power_score || []
  const oppIncName = opportunity?.ui_opportunity_incumbent[0]?.display_name
  const oppCompName = opportunity.ui_opportunity_competitor[0]?.display_name
  const oppIndustryAttribute =
    opportunity.ui_opportunity_account?.ui_account_attribs?.find(
      (att) => att.attrib_name?.toLowerCase() === 'industry'
    )

  const sourcePowerScore =
    opportunity.opportunity_source &&
    powerScores.find(
      (score) =>
        score.type === 'Source' && score.name === opportunity.opportunity_source
    )
  const industryPowerSCore =
    oppIndustryAttribute &&
    powerScores.find(
      (score) =>
        score.type === 'Industry' &&
        score.name === oppIndustryAttribute.attrib_value
    )

  const competitorPowerScore =
    oppCompName &&
    powerScores.find(
      (score) => score.type === 'Competitor' && score.name === oppCompName
    )
  const incumbentPowerScore =
    oppIncName &&
    powerScores.find(
      (score) => score.type === 'Incumbent' && score.name === oppIncName
    )
  return (
    <>
      <div className="rw-segment">
        <Flex
          flexDir="column"
          width="100%"
          px={10}
          py={3}
          justifyContent="flex-start"
        >
          <BreadcrumbComponent
            childText={opportunity.ui_opportunity_account?.display_name}
            amount={opportunity.projected_amt}
          >
            <Link to="/opportunities">All Opportunities</Link>
          </BreadcrumbComponent>
          <HStack my={2} alignSelf="stretch" fontSize="x-large">
            <Text bgColor="gray.300" py={2} px={4} borderRadius="md">
              {opportunity.ui_opportunity_account?.display_name}
            </Text>
            <VStack alignItems="flex-start">
              <Text fontWeight="bold">{opportunity.opportunity_name}</Text>
              <Text m="0px !important" fontSize="md" pb={2}>
                {opportunity.ui_opportunity_account?.display_name}
              </Text>
            </VStack>
          </HStack>
          <Flex>
            <VStack width="20%" mr={10}>
              <Flex
                borderWidth="2px"
                borderColor={'gray.400'}
                flexDir="column"
                bgColor="white"
                width={270}
                px={5}
                py={5}
                borderRadius="md"
              >
                <Text color="general.gray">Opportunity Signature</Text>
                <Heading my={4} size="md" fontWeight="medium">
                  The{' '}
                  {opportunity.ui_dna_signature?.display_name?.split(' ')[1]}{' '}
                  Signature
                </Heading>
                <Heading color={'gray.400'} fontWeight="semibold" size="sm">
                  <CurrencyText
                    numberType="short"
                    value={opportunity.ui_dna_signature?.avg_deal_size}
                  />
                  + {opportunity.ui_dna_signature?.avg_nrtr_days} days
                </Heading>
              </Flex>
              <SummaryComponent
                data={opportunity.ui_opportunity_recommendations}
                title="Opportunity Summary"
                summaryLevel="High Prospects"
                score={opportunity.prediction_score}
              ></SummaryComponent>
              <TasksSummary
                data={{
                  completed: opportunity.tasks_completed,
                  total: opportunity.tasks_total,
                  averageDailyRate: opportunity.tasks_avg_daily_rate,
                  averageWeeklyRate: opportunity.tasks_avg_weekly_rate,
                  trendPercentage: opportunity.tasks_trend_percentage,
                  trendText: 'vs last 31 days',
                }}
              />
              <OpportunityEngagement
                data={{
                  dailyTaskStreak: opportunity.engagement_daily_streak,
                  score: opportunity.engagement_score,
                  weeklyTaskStreak: opportunity.engagement_weekly_streak,
                }}
              />
            </VStack>
            <Box width="80%">
              <ParentContainer icon="ℹ️" title="Overview">
                <SimpleGrid columns={3} width="100%">
                  <IndividualOverviewContainer title="Projected Amount">
                    <Text fontWeight="semibold">
                      <CurrencyText
                        value={opportunity.projected_amt}
                        numberType="long"
                      />
                    </Text>
                  </IndividualOverviewContainer>
                  <IndividualOverviewContainer title="Close-By Date">
                    <Text fontWeight="semibold">
                      {DateFormatterLastDayOfMonth(
                        opportunity.projected_close_by_date
                      )}
                    </Text>
                  </IndividualOverviewContainer>
                  <IndividualOverviewContainer title="Opportunity Source">
                    <Flex alignItems="flex-end">
                      <Text fontWeight="semibold">
                        {opportunity.opportunity_source}
                      </Text>

                      {sourcePowerScore?.score && (
                        <PowerScoresComponent
                          powerScores={sourcePowerScore.score}
                        />
                      )}
                    </Flex>
                  </IndividualOverviewContainer>
                  <IndividualOverviewContainer title="Industry">
                    <Flex alignItems="flex-end">
                      <Text fontWeight="semibold">
                        {opportunity.industry_type}
                      </Text>

                      {industryPowerSCore?.score && (
                        <PowerScoresComponent
                          powerScores={industryPowerSCore.score}
                        />
                      )}
                    </Flex>
                  </IndividualOverviewContainer>
                  <IndividualOverviewContainer title="Incumbent">
                    <Flex alignItems="flex-end">
                      <Text fontWeight="semibold">
                        {opportunity.ui_opportunity_incumbent[0]?.display_name}
                      </Text>

                      {incumbentPowerScore?.score && (
                        <PowerScoresComponent
                          powerScores={incumbentPowerScore.score}
                        />
                      )}
                    </Flex>
                  </IndividualOverviewContainer>
                  <IndividualOverviewContainer title="Competitor">
                    <Flex alignItems="flex-end">
                      <Text fontWeight="semibold">
                        {opportunity.ui_opportunity_competitor[0]?.display_name}
                      </Text>

                      {competitorPowerScore?.score && (
                        <PowerScoresComponent
                          powerScores={competitorPowerScore?.score}
                        />
                      )}
                    </Flex>
                  </IndividualOverviewContainer>
                </SimpleGrid>
              </ParentContainer>
              <ParentContainer title="Opportunity Tasks">
                <TotalTasksBar
                  completedTasks={
                    opportunity.ui_task?.filter(
                      (element) =>
                        element.display_status.toLowerCase() === 'complete'
                    ).length
                  }
                  totalTasks={
                    opportunity.ui_task?.filter(
                      (element) =>
                        element.display_status.toLowerCase() === 'incomplete'
                    ).length
                  }
                />
                <Flex
                  alignItems="baseline"
                  ml={3}
                  my={4}
                  py={3}
                  px={3}
                  width="50%"
                  borderRadius="md"
                  bgGradient="linear(to-l, white,gray.100)"
                  fontSize="md"
                  color="general.gray"
                >
                  <Box
                    bgColor="gray.300"
                    borderRadius="sm"
                    py={1}
                    px={2}
                    mr={2}
                  >
                    <Icon fontSize="xl" viewBox="0 0 17 21">
                      <FlagIconGray />
                    </Icon>
                  </Box>
                  <Text>Start of Opportunity</Text>
                </Flex>
                <SimpleGrid
                  columns={3}
                  px={3}
                  spacingX={5}
                  spacingY={4}
                  minChildWidth="250px"
                >
                  {opportunity.ui_task.map((t, i) => (
                    <TaskInfo task={t} key={i} />
                  ))}
                </SimpleGrid>
                <Flex
                  alignItems="baseline"
                  ml={3}
                  my={4}
                  py={3}
                  px={3}
                  width="50%"
                  borderRadius="md"
                  bgGradient="linear(to-l, white,gray.100)"
                  fontSize="md"
                  color="general.gray"
                >
                  <Box
                    bgColor="gray.300"
                    borderRadius="sm"
                    py={1}
                    px={2}
                    mr={2}
                  >
                    <Icon viewBox="0 0 10 18">
                      <DollarIconGray />
                    </Icon>
                  </Box>
                  <Text>Opportunity Won!</Text>
                </Flex>
              </ParentContainer>
            </Box>
          </Flex>
        </Flex>
      </div>
    </>
  )
}

export default Opportunity
