import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { Link, routes } from '@redwoodjs/router'

import { QUERY } from 'src/components/Opportunity/OpportunitiesCell'

const DELETE_OPPORTUNITY_MUTATION = gql`
  mutation DeleteOpportunityMutation($id: String!) {
    deleteOpportunity(id: $id) {
      id
    }
  }
`

const OpportunitiesList = ({ opportunities }) => {
  return (
    <div className="rw-segment rw-table-wrapper-responsive">
      <table className="rw-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Account id fk</th>
            <th>Owner id fk</th>
            <th>Projected amt</th>
            <th>System amt</th>
            <th>Projected close by date</th>
            <th>System date</th>
            <th>Predicted outcome</th>
            <th>System outcome</th>
            <th>Prediction score</th>
            <th>Creation date</th>
            <th>Mkt nrcr days</th>
            <th>Sls nrcr days</th>
            <th>Age</th>
            <th>Pct actvty complete</th>
            <th>Num actvty</th>
            <th>Num sls actvty</th>
            <th>Num mkt actvty</th>
            <th>Poc duration</th>
            <th>Industry type</th>
            <th>Fg industry type</th>
            <th>Tasks total</th>
            <th>Tasks completed</th>
            <th>Tasks avg weekly rate</th>
            <th>Tasks avg daily rate</th>
            <th>Tasks trend percentage</th>
            <th>Engagement score</th>
            <th>Engagement daily streak</th>
            <th>Engagement weekly streak</th>
            <th>Salesforce closing date</th>
            <th>Signature id fk</th>
            <th>Opportunity name</th>
            <th>Opportunity source</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {opportunities.map((opportunity) => (
            <tr key={opportunity.id}>
              <td>{truncate(opportunity.id)}</td>
              <td>{truncate(opportunity.account_id_fk)}</td>
              <td>{truncate(opportunity.owner_id_fk)}</td>
              <td>{truncate(opportunity.projected_amt)}</td>
              <td>{truncate(opportunity.system_amt)}</td>
              <td>{timeTag(opportunity.projected_close_by_date)}</td>
              <td>{timeTag(opportunity.system_date)}</td>
              <td>{truncate(opportunity.predicted_outcome)}</td>
              <td>{truncate(opportunity.system_outcome)}</td>
              <td>{truncate(opportunity.prediction_score)}</td>
              <td>{timeTag(opportunity.creation_date)}</td>
              <td>{truncate(opportunity.mkt_nrcr_days)}</td>
              <td>{truncate(opportunity.sls_nrcr_days)}</td>
              <td>{truncate(opportunity.age)}</td>
              <td>{truncate(opportunity.pct_actvty_complete)}</td>
              <td>{truncate(opportunity.num_actvty)}</td>
              <td>{truncate(opportunity.num_sls_actvty)}</td>
              <td>{truncate(opportunity.num_mkt_actvty)}</td>
              <td>{truncate(opportunity.poc_duration)}</td>
              <td>{truncate(opportunity.industry_type)}</td>
              <td>{truncate(opportunity.fg_industry_type)}</td>
              <td>{truncate(opportunity.tasks_total)}</td>
              <td>{truncate(opportunity.tasks_completed)}</td>
              <td>{truncate(opportunity.tasks_avg_weekly_rate)}</td>
              <td>{truncate(opportunity.tasks_avg_daily_rate)}</td>
              <td>{truncate(opportunity.tasks_trend_percentage)}</td>
              <td>{truncate(opportunity.engagement_score)}</td>
              <td>{truncate(opportunity.engagement_daily_streak)}</td>
              <td>{truncate(opportunity.engagement_weekly_streak)}</td>
              <td>{timeTag(opportunity.salesforce_closing_date)}</td>
              <td>{truncate(opportunity.signature_id_fk)}</td>
              <td>{truncate(opportunity.opportunity_name)}</td>
              <td>{truncate(opportunity.opportunity_source)}</td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.opportunity({ id: opportunity.id })}
                    title={'Show opportunity ' + opportunity.id + ' detail'}
                    className="rw-button rw-button-small"
                  >
                    Show
                  </Link>
                  <Link
                    to={routes.editOpportunity({ id: opportunity.id })}
                    title={'Edit opportunity ' + opportunity.id}
                    className="rw-button rw-button-small rw-button-blue"
                  >
                    Edit
                  </Link>
                  <button
                    type="button"
                    title={'Delete opportunity ' + opportunity.id}
                    className="rw-button rw-button-small rw-button-red"
                    onClick={() => onDeleteClick(opportunity.id)}
                  >
                    Delete
                  </button>
                </nav>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default OpportunitiesList
