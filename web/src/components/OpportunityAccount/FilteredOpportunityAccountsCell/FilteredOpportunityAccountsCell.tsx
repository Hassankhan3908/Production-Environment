import type { FindFilteredOpportunityAccounts } from 'types/graphql'
import { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import { Link, routes } from '@redwoodjs/router'

import React from 'react'

import {
  Pagination,
  PaginationButtonFirstPage,
  PaginationButtonLastPage,
  PaginationButtonNextPage,
  PaginationButtonPrevPage,
  PaginationInfo,
} from 'src/common/components'
import OpportunityAccounts from '../OpportunityAccounts'

export const QUERY = gql`
  query FindFilteredOpportunityAccounts($page: Int, $sfId: String) {
    filterOpportunityAccounts(page: $page, sfId: $sfId) {
      opportunityAccounts {
        display_name
        heat_score
        ui_opportunity {
          opportunity_name
          opportunity_source
        }
        ui_account_attribs {
          attrib_name
          attrib_value
        }
        sf_id
        id
      }
      count
    }
  }
`
export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No opportunities yet. '}
      <Link to={routes.newOpportunity()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({
  filterOpportunityAccounts,
  loading,
  variables,
  refetch,
}: CellSuccessProps<FindFilteredOpportunityAccounts>) => {
  const { count } = filterOpportunityAccounts
  const setPage = (page: number) => {
    refetch({ ...variables, page })
  }
  return (
    <>
      <OpportunityAccounts
        filterOpportunityAccounts={filterOpportunityAccounts}
      />
      <Pagination
        isLoadingPage={loading}
        setPage={setPage}
        page={variables.page || 1}
        pageSize={8}
        totalItems={count}
      >
        <PaginationButtonFirstPage />
        <PaginationButtonPrevPage />
        <PaginationInfo flex="1" />
        <PaginationButtonNextPage />
        <PaginationButtonLastPage />
      </Pagination>
    </>
  )
}
