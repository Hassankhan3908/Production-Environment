import { Link, routes } from '@redwoodjs/router'
import {
  Spinner,
  HStack,
  VStack,
  Text,
  Flex,
  Box,
  SimpleGrid,
  Divider,
} from '@chakra-ui/react'
import {
  BreadcrumbComponent,
  ParentContainer,
  CurrencyText,
  Opportunity,
  EngagementPerformance,
  SummaryComponent,
  GetIcon,
  AccountSubOverview,
} from 'src/common/components'

import React from 'react'
import { FindOpportunityAccountById } from 'types/graphql'
import { CellSuccessProps } from '@redwoodjs/web'
const OpportunityAccount = ({
  opportunityAccount,
}: CellSuccessProps<FindOpportunityAccountById>) => {
  return (
    <>
      <Flex
        flexDir="column"
        width="100%"
        px={10}
        py={3}
        justifyContent="flex-start"
      >
        {/*  <BreadcrumbComponent childText={name}>
          <Link to="/accounts"> All Accounts</Link>
        </BreadcrumbComponent>
        <HStack my={2} alignSelf="stretch" fontSize="x-large">
          <Text bgColor="gray.300" py={2} px={4} borderRadius="md">
            {name[0]}
          </Text>
          <VStack alignItems="flex-start">
            <Text fontWeight="bold">{opportunityAccount.display_name}</Text>
          </VStack>
        </HStack>
        <Flex>
          <VStack width="20%" mr={10}>
            <SummaryComponent
              title="Account Summary"
              summaryLevel="High Heat"
              heat
              data={opportunityAccount.ui_opportunity}
            >
              {response.data.response.power_score.length ? (
                <VStack
                  borderWidth="2px"
                  boxShadow="md"
                  borderRadius="md"
                  bgColor="container.lGood"
                  alignItems="flex-start"
                  px={2}
                  py={3}
                >
                  {response.data.response.power_score.map(
                    ({ name, score }, index) => (
                      <React.Fragment key={index}>
                        <Text fontWeight="semibold">{name}</Text>
                        <HStack
                          justifyContent="space-between"
                          alignSelf="stretch"
                        >
                          <Text>Power Score:</Text>
                          <Text fontWeight="semibold">{score}x</Text>
                        </HStack>
                        <Divider borderBottomWidth={2} />
                      </React.Fragment>
                    )
                  )}
                </VStack>
              ) : (
                <></>
              )}
            </SummaryComponent>
          </VStack>
          <Box width="80%">
            <ParentContainer icon="ℹ️" title="Overview">
              <SimpleGrid spacingX={16} spacingY={8} columns={3} width="100%">
                <EngagementPerformance
                  text="Num of Engaged contacts"
                  count={accountAttributes.engagedContactsPerformance}
                  performance={accountAttributes.engagedContactsPerformance}
                />
                <EngagementPerformance
                  text="Num of open opportunities"
                  count={accountAttributes.numberOfOpenOpportunities}
                  performance={accountAttributes.openOpportunitiesPerformance}
                />
                <EngagementPerformance
                  text={`Last month sales & marketing activites`}
                  count={accountAttributes.lastSixMonthSMActivities}
                  performance={
                    accountAttributes.lastSixMonthSMActivitiesPerformance
                  }
                />
                <EngagementPerformance
                  text="Account TAM"
                  count={
                    accountAttributes.tam && (
                      <CurrencyText
                        value={accountAttributes.tam}
                        numberType="long"
                      />
                    )
                  }
                  performance={accountAttributes.tamPerformance}
                />
                <EngagementPerformance
                  text="Number of lost opportunities"
                  count={accountAttributes.engagedContactsPerformance}
                  performance={accountAttributes.engagedContactsPerformance}
                />
                <EngagementPerformance
                  text="Last 6 month activities"
                  count={accountAttributes.lastSixMonthActivities}
                  performance={
                    accountAttributes.lastSixMonthActivitiesPerformance
                  }
                />
              </SimpleGrid>
              <Box px={2} py={5}>
                <Divider />
              </Box>
              <SimpleGrid
                spacingX={16}
                spacingY={8}
                columns={3}
                width="100%"
                px={2}
              >
                <AccountSubOverview
                  title="Industry"
                  value={accountAttributes.industry}
                />
                <AccountSubOverview
                  title="Theater"
                  value={accountAttributes.theater}
                />
                <AccountSubOverview
                  title="Tier"
                  value={accountAttributes.tier}
                />
                <AccountSubOverview
                  title="Annual Revenues"
                  value={
                    <CurrencyText
                      numberType="short"
                      value={accountAttributes.annualRevenue}
                    />
                  }
                />
                <AccountSubOverview
                  title="Number of Employees"
                  value={accountAttributes?.numberOfEmployee
                    ?.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                />
                <AccountSubOverview
                  title="Account Tam"
                  value={
                    accountAttributes.tam && (
                      <CurrencyText
                        numberType="long"
                        value={accountAttributes.tam}
                      />
                    )
                  }
                />
              </SimpleGrid>
            </ParentContainer>

            {opportunity_model.length !== 0 && (
              <ParentContainer title="Opportunities">
                <Box mx="-15px">
                  <Flex flexWrap="wrap" w="100%">
                    {opportunity_model.map((element, index) => (
                      <Box
                        width={['100%', '100%', '33.33%']}
                        px="15px"
                        py="20px"
                        borderWidth="0"
                        borderRadius="4px"
                        overflow="hidden"
                      >
                        <Link
                          key={index}
                          to={history.location.pathname.replace(
                            'accounts',
                            'opportunities'
                          )}
                        >
                          <Opportunity data={{ ...element, name }} />
                        </Link>
                      </Box>
                    ))}
                  </Flex>
                </Box>
              </ParentContainer>
            )}
          </Box>
        </Flex> */}
      </Flex>
    </>
  )
}

export default OpportunityAccount
