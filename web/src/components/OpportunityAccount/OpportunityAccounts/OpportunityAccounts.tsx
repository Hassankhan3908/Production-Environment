import { Link, routes } from '@redwoodjs/router'

import { FindFilteredOpportunityAccounts } from 'types/graphql'
import React from 'react'
import {
  Text,
  Divider,
  Flex,
  HStack,
  Icon,
  SimpleGrid,
  VStack,
} from '@chakra-ui/react'
import { ArrowRightIcon } from 'src/assets/icons'
import { EngagementPerformance } from 'src/common/components/accounts/individual-account/EngagementPerformance'
import { NoAccounts } from 'src/common/components/no-accounts-available/NoAccounts'
import { CellSuccessProps } from '@redwoodjs/web'

const OpportunityAccountsList = ({
  filterOpportunityAccounts,
}: CellSuccessProps<FindFilteredOpportunityAccounts>) => {
  const { opportunityAccounts } = filterOpportunityAccounts
  return (
    <Flex my={5} width="100%" m={5} px={5} pl={10}>
      {opportunityAccounts?.length > 0 ? (
        <SimpleGrid width="100%" columns={2} spacing={4}>
          {opportunityAccounts.map((oppAccount, index) => (
            <Link
              to={routes.opportunityAccount({ id: oppAccount.id })}
              key={index}
            >
              <VStack bgColor="white" p={5} borderRadius="md" shadow="lg">
                <HStack alignSelf="stretch" justifyContent="space-between">
                  <Flex>
                    <HStack alignSelf="stretch">
                      {/* <Text bgColor="gray.300" py={2} px={4} borderRadius="md">
                        {oppAccount.display_name}
                      </Text> */}
                    </HStack>
                    <VStack ml={2} alignItems="flex-start">
                      <Text fontWeight="bold">{oppAccount.display_name}</Text>
                      <Text
                        m="0px !important"
                        fontSize="small"
                        fontWeight="semibold"
                        color="general.blue"
                      >
                        View Details
                        <Icon ml={1} width={3} height={3} viewBox="0 0 11 11">
                          <ArrowRightIcon />
                        </Icon>
                      </Text>
                    </VStack>
                  </Flex>
                  <Flex>
                    <Text
                      mr={2}
                      fontSize="sm"
                      fontWeight="semibold"
                      color="general.gray"
                      display="inline-block"
                    >
                      {oppAccount.heat_score === 3
                        ? 'High Heat 🔥🔥🔥'
                        : oppAccount.heat_score === 2
                        ? 'Med Heat 🔥🔥'
                        : 'Low Heat 🔥'}
                    </Text>
                  </Flex>
                </HStack>
                <Divider />
                <HStack alignSelf="stretch" justifyContent="space-between">
                  <EngagementPerformance
                    text="Num of engaged contacts"
                    count={
                      0 //oppAccount.ui_account_attribs.number_of_engaged_contacts
                    }
                    performance={Number(
                      0 // oppAccount.ui_account_attribs.engaged_contacts_performance
                    )}
                  />
                  <EngagementPerformance
                    text="Num of open Opportunities"
                    count={
                      0 //oppAccount.ui_account_attribs.number_of_open_opportunities
                    }
                    performance={Number(
                      0 // oppAccount.ui_account_attribs.open_opportunities_performance
                    )}
                  />
                </HStack>
              </VStack>
            </Link>
          ))}
        </SimpleGrid>
      ) : (
        <NoAccounts />
      )}
    </Flex>
  )
}

export default OpportunityAccountsList
