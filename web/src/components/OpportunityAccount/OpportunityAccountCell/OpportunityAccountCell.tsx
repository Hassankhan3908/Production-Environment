import type { FindOpportunityAccountById } from 'types/graphql'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import OpportunityAccount from 'src/components/OpportunityAccount/OpportunityAccount'

export const QUERY = gql`
  query FindOpportunityAccountById($id: String!) {
    opportunityAccount: opportunityAccount(id: $id) {
      id
      display_name
      effective_date
      exp_date
      heat_score
      sf_id
      ui_account_attribs {
        id
        attrib_name
        attrib_value
      }
      ui_account_recommendations {
        id
        recommendation
      }
      ui_opportunity {
        id
        opportunity_name
        ui_dna_signature {
          ui_dna {
            ui_account_powerscore {
              id
              name
              score
              type
            }
          }
        }
      }
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>OpportunityAccount not found</div>

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({
  opportunityAccount,
}: CellSuccessProps<FindOpportunityAccountById>) => {
  return <OpportunityAccount opportunityAccount={opportunityAccount} />
}
