import type { FindOpportunityOwner } from 'types/graphql'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import React, { useState } from 'react'
import { MiniHeader, FilterListAccount } from 'src/common/components'
import { useAuth } from '@redwoodjs/auth'

import { ForecastSidebarIcon } from 'src/assets/icons'
import FilteredOpportunityAccountsCell from 'src/components/OpportunityAccount/FilteredOpportunityAccountsCell'

export const QUERY = gql`
  query FindOpportunityOwner($id: String!) {
    owner(id: $id) {
      family_name
      id
      first_given_name
      org_tree
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return <div className="rw-text-center">{'No opportunityAccounts yet. '}</div>
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ owner }: CellSuccessProps<FindOpportunityOwner>) => {
  const { userMetadata } = useAuth()
  const [page, setPage] = useState(1)
  const [id, setId] = useState(userMetadata?.sub?.replace('salesforce|', ''))
  return (
    <>
      <MiniHeader title="Accounts" icon={<ForecastSidebarIcon />} />
      <FilterListAccount id={id} setId={setId} orgTree={[owner?.org_tree]} />
      <FilteredOpportunityAccountsCell sfId={''} page={page} />
    </>
  )
}
