import type { FindDnaSignatures } from 'types/graphql'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import { Link, routes } from '@redwoodjs/router'

import DnaSignatures from 'src/components/DnaSignature/DnaSignatures'

export const QUERY = gql`
  query FindDnaSignatures {
    dnaSignatures {
      id
      dna_id_fk
      total_pipeline
      total_revenue
      pct_win_rate
      avg_deal_size
      avg_nrtr_days
      display_name
      effective_date
      exp_date
      ui_order
      ui_color
      classification_label
      ui_dna_signature_stage {
        display_name
        effective_date
        id
        metric_value
        phase
        seq_order
      }
      ui_power_score {
        description
        name
        score
        id
        type
      }
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No dnaSignatures yet. '}
      <Link to={routes.newDnaSignature()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({
  dnaSignatures,
}: CellSuccessProps<FindDnaSignatures>) => {
  return <DnaSignatures dnaSignatures={dnaSignatures} />
}
