import { Link, routes } from '@redwoodjs/router'
import React from 'react'
import {
  Spinner,
  Divider,
  SimpleGrid,
  Box,
  Icon,
  Link as ChakraLink,
  Heading,
  Flex,
  Button,
} from '@chakra-ui/react'
import {
  MiniHeader,
  SignatureContainer,
  AccordionList,
  TimelineComponent,
} from 'src/common/components'
import { Switch, Route, useLocation } from 'react-router-dom'
import { InfoIcon, YourDNA } from 'src/assets/icons'
import { DnaSignature, FindDnaSignatures } from 'types/graphql'
import { CellSuccessProps } from '@redwoodjs/web'

export const defaultStyling = {
  bgColor: 'miniHeader.ButtonColor',
  color: 'general.gray',
  fontWeight: 'normal',
  border: '1px transparent',
}
export const activeStyling = {
  variant: 'outline',
  bgColor: 'badge.textColor',
}

const DnaSignaturesList = ({
  dnaSignatures,
}: CellSuccessProps<FindDnaSignatures>) => {
  const pipelineSum = dnaSignatures.reduce(
    (a, b) => a + (b?.total_pipeline || 0),
    0
  )
  const revenueSum = dnaSignatures.reduce(
    (a, b) => a + (b?.total_revenue || 0),
    0
  )
  const transformation = (inputArray) => {
    const reducedObject = inputArray.reduce((obj, acc) => {
      if (Array.isArray(obj[acc.type])) {
        obj[acc.type] = [...obj[acc.type], { name: acc.name, score: acc.score }]
      } else {
        obj[acc.type] = [{ name: acc.name, score: acc.score }]
      }
      return obj
    }, {})
    const resultantArray = []
    for (const [key, value] of Object.entries(reducedObject)) {
      resultantArray.push({
        type: key,
        values: value,
        priority:
          key === 'Industry'
            ? 1
            : key === 'Competitor'
            ? 2
            : key === 'Incumbent'
            ? 3
            : key === 'Source'
            ? 4
            : 5,
      })
    }
    resultantArray.sort((a, b) => a.priority - b.priority)
    resultantArray.map((element) => console.log(element, ' elements are here'))
    return resultantArray
  }

  return (
    <>
      <MiniHeader
        title="Company DNA"
        subtitle=" for Nov 12, 2021 version 2"
        icon={<YourDNA />}
      >
        {[
          { url: '/yourdna', text: 'Main Signature' },
          { url: '/yourdna/winVsLoss', text: 'Win vs Loss' },
        ].map(({ url, text }) => (
          <Link to={url} key={url}>
            <Button fontSize="small" height={8} width={32} size="sm" mx={13}>
              {text}
            </Button>
          </Link>
        ))}
      </MiniHeader>

      <SimpleGrid
        width="100%"
        py={4}
        px="35"
        minChildWidth="250px"
        spacing={30}
      >
        {dnaSignatures.map((signature: DnaSignature) => {
          return (
            <SignatureContainer
              title={signature?.display_name}
              key={signature?.id}
              type={signature?.display_name}
              revenue={signature?.total_revenue}
              amount={signature?.avg_deal_size}
              days={signature?.avg_nrtr_days}
              rate={signature?.sfsdfsdf}
              pipeline={signature?.total_pipeline}
              totalPipeline={Math.round(
                (signature?.total_pipeline / pipelineSum) * 100
              )}
              totalRevenue={Math.round(
                (signature?.total_revenue / revenueSum) * 100
              )}
            >
              <Divider borderBottomWidth="2px" />
              <TimelineComponent data={signature.ui_dna_signature_stage} />
              <Divider borderBottomWidth="2px" />
              {signature.ui_power_score?.length > 0 && (
                <Box>
                  <Box my={4}>
                    <Heading size="md" fontWeight="bold" mb={8}>
                      Power Scores
                    </Heading>

                    <ChakraLink
                      href="#"
                      color="general.blue"
                      fontWeight="semibold"
                      _hover={{ underline: 'none' }}
                    >
                      What are Power Scores?
                      <Icon
                        position="relative"
                        top={1}
                        left={2}
                        fontSize="x-large"
                      >
                        <InfoIcon />
                      </Icon>
                    </ChakraLink>
                  </Box>
                  {transformation(signature.ui_power_score).map(
                    (element, key) => (
                      <AccordionList
                        list={{
                          values: element.values.sort((a, b) =>
                            a.score < b.score ? 1 : -1
                          ),
                          type: element.type,
                        }}
                        key={key}
                      />
                    )
                  )}
                </Box>
              )}
            </SignatureContainer>
          )
        })}
      </SimpleGrid>
    </>
  )
}

export default DnaSignaturesList
