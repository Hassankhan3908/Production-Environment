import React from 'react';

export const SignatureLike = () => {
  return (
    <svg
      width='35'
      height='35'
      viewBox='0 0 35 35'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <g filter='url(#filter0_d)'>
        <rect x='-305' y='-29' width='366' height='242' rx='6' fill='white' />
        <rect
          x='-305'
          y='-29'
          width='366'
          height='242'
          rx='6'
          stroke='#2766F6'
          strokeWidth='2'
        />
      </g>
      <circle cx='17.5' cy='17.5' r='17.5' fill='#4DD474' />
      <path
        d='M14.375 23.75H12.5C12.1685 23.75 11.8505 23.6183 11.6161 23.3839C11.3817 23.1495 11.25 22.8315 11.25 22.5V18.125C11.25 17.7935 11.3817 17.4755 11.6161 17.2411C11.8505 17.0067 12.1685 16.875 12.5 16.875H14.375M18.75 15.625V13.125C18.75 12.6277 18.5525 12.1508 18.2008 11.7992C17.8492 11.4475 17.3723 11.25 16.875 11.25L14.375 16.875V23.75H21.425C21.7265 23.7534 22.019 23.6478 22.2487 23.4525C22.4784 23.2573 22.6298 22.9856 22.675 22.6875L23.5375 17.0625C23.5647 16.8833 23.5526 16.7004 23.5021 16.5264C23.4516 16.3524 23.3638 16.1914 23.2449 16.0547C23.126 15.918 22.9788 15.8087 22.8135 15.7345C22.6482 15.6603 22.4687 15.6229 22.2875 15.625H18.75Z'
        stroke='white'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <defs>
        <filter
          id='filter0_d'
          x='-309'
          y='-31'
          width='374'
          height='250'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
          />
          <feOffset dy='2' />
          <feGaussianBlur stdDeviation='1.5' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.04 0'
          />
          <feBlend
            mode='normal'
            in2='BackgroundImageFix'
            result='effect1_dropShadow'
          />
          <feBlend
            mode='normal'
            in='SourceGraphic'
            in2='effect1_dropShadow'
            result='shape'
          />
        </filter>
      </defs>
    </svg>
  );
};
