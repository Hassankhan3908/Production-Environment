import React from 'react';

export const NoteIcon = () => {
  return (
    <>
      <svg
        width='12'
        height='12'
        viewBox='0 0 12 12'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          d='M1.33333 0C0.596667 0 0 0.596667 0 1.33333V10.6667C0 11.4033 0.596667 12 1.33333 12H8L12 8V1.33333C12 0.596667 11.4033 0 10.6667 0H1.33333ZM1.33333 1.33333H10.6667V7.33333H7.33333V10.6667H1.33333V1.33333ZM2.66667 2.66667V4H9.33333V2.66667H2.66667ZM2.66667 5.33333V6.66667H6V5.33333H2.66667Z'
          fill='currentColor'
        />
      </svg>
    </>
  );
};
