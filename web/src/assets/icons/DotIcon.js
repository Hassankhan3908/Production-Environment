import React from 'react';
export const DotIcon = () => {
  return (
    <svg
      width='16'
      height='16'
      viewBox='0 0 12 12'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <circle
        cx='6'
        cy='6'
        r='5'
        fill='#ECF0F8'
        stroke='#C0C8DB'
        strokeWidth='2'
      />
    </svg>
  );
};
