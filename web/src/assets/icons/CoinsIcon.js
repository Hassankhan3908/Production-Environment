import React from 'react';

export const CoinsIcon = () => {
  return (
    <svg
      width='22'
      height='22'
      viewBox='0 0 22 22'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M0.916504 3.6665V7.33317C0.916504 8.85209 3.37867 10.0832 6.4165 10.0832C9.45434 10.0832 11.9165 8.85209 11.9165 7.33317V3.6665'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M0.916504 7.33301V10.9997C0.916504 12.5186 3.37867 13.7497 6.4165 13.7497C7.82542 13.7497 9.10967 13.4838 10.0832 13.0484'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M0.916504 11V14.6667C0.916504 16.1856 3.37867 17.4167 6.4165 17.4167C7.82542 17.4167 9.11059 17.1518 10.0832 16.7163'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M6.4165 6.4165C9.45407 6.4165 11.9165 5.18529 11.9165 3.6665C11.9165 2.14772 9.45407 0.916504 6.4165 0.916504C3.37894 0.916504 0.916504 2.14772 0.916504 3.6665C0.916504 5.18529 3.37894 6.4165 6.4165 6.4165Z'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M10.083 11V14.6667C10.083 16.1856 12.5452 17.4167 15.583 17.4167C18.6208 17.4167 21.083 16.1856 21.083 14.6667V11'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M10.083 14.6665V18.3332C10.083 19.8521 12.5452 21.0832 15.583 21.0832C18.6208 21.0832 21.083 19.8521 21.083 18.3332V14.6665'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M15.583 13.75C18.6206 13.75 21.083 12.5188 21.083 11C21.083 9.48122 18.6206 8.25 15.583 8.25C12.5454 8.25 10.083 9.48122 10.083 11C10.083 12.5188 12.5454 13.75 15.583 13.75Z'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
    </svg>
  );
};
