import React from 'react';

export const CircleTick = () => {
  return (
    <svg
      width='13'
      height='14'
      viewBox='0 0 13 14'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M11.9167 6.50151V6.99985C11.916 8.16791 11.5378 9.30446 10.8384 10.24C10.139 11.1755 9.15599 11.8599 8.03586 12.1911C6.91573 12.5223 5.71856 12.4825 4.62288 12.0777C3.5272 11.6729 2.59173 10.9248 1.95598 9.94492C1.32023 8.96503 1.01826 7.80588 1.09512 6.64035C1.17197 5.47482 1.62353 4.36536 2.38244 3.47743C3.14136 2.5895 4.16697 1.97068 5.30631 1.71326C6.44565 1.45584 7.63769 1.57361 8.70463 2.04901'
        stroke='#8181A5'
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M11.9167 2.6665L6.5 8.08859L4.875 6.46359'
        stroke='#8181A5'
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
};
