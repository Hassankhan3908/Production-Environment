import React from 'react';
export const PiplineIcon = () => {
  return (
    <>
      <path
        d='M0 0L0.729649 1.21458L9 15L18 0H0ZM2.75033 1.58842H15.2497L13.8202 3.97104H4.17985L2.75033 1.58842ZM5.13286 5.55946H12.8671L11.4376 7.94209H6.56238L5.13286 5.55946ZM7.51539 9.53051H10.4846L9 12.0047L7.51539 9.53051Z'
        fill='currentColor'
      />
    </>
  );
};
