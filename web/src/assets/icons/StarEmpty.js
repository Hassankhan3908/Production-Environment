import React from 'react';

export const StarEmpty = () => {
  return (
    <svg
      width='14'
      height='13'
      viewBox='0 0 14 13'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <g filter='url(#filter0_i)'>
        <path
          d='M12.9368 4.62614L9.10125 4.06877L7.38796 0.595598C7.33815 0.511022 7.26712 0.440912 7.18191 0.392202C7.09669 0.343493 7.00024 0.317871 6.90208 0.317871C6.80393 0.317871 6.70747 0.343493 6.62226 0.392202C6.53704 0.440912 6.46602 0.511022 6.41621 0.595598L4.69967 4.06877L0.864125 4.62614C0.764101 4.64058 0.670111 4.68271 0.59278 4.74777C0.51545 4.81284 0.457863 4.89824 0.426531 4.99432C0.395199 5.0904 0.391371 5.19333 0.41548 5.29147C0.43959 5.38961 0.490674 5.47905 0.562958 5.54968L3.339 8.25477L2.68412 12.0751C2.66706 12.1748 2.6782 12.2772 2.71629 12.3708C2.75438 12.4644 2.8179 12.5455 2.89967 12.6049C2.98144 12.6643 3.07819 12.6996 3.179 12.7069C3.2798 12.7142 3.38063 12.6931 3.47008 12.6461L6.90046 10.8428L10.3308 12.6461C10.4203 12.6931 10.5211 12.7142 10.6219 12.7069C10.7227 12.6996 10.8195 12.6643 10.9012 12.6049C10.983 12.5455 11.0465 12.4644 11.0846 12.3708C11.1227 12.2772 11.1339 12.1748 11.1168 12.0751L10.4619 8.25477L13.238 5.54968C13.3102 5.47913 13.3613 5.38977 13.3855 5.29171C13.4096 5.19365 13.4059 5.09078 13.3747 4.99473C13.3435 4.89867 13.286 4.81326 13.2089 4.74813C13.1317 4.683 13.0378 4.64074 12.9379 4.62614H12.9368Z'
          fill='#8181A5'
          fillOpacity='0.4'
        />
      </g>
      <defs>
        <filter
          id='filter0_i'
          x='0.399841'
          y='0.317871'
          width='13.0014'
          height='13.3904'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feBlend
            mode='normal'
            in='SourceGraphic'
            in2='BackgroundImageFix'
            result='shape'
          />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
            result='hardAlpha'
          />
          <feOffset dy='1' />
          <feGaussianBlur stdDeviation='1' />
          <feComposite in2='hardAlpha' operator='arithmetic' k2='-1' k3='1' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0'
          />
          <feBlend mode='normal' in2='shape' result='effect1_innerShadow' />
        </filter>
      </defs>
    </svg>
  );
};
