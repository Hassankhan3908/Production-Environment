import React from 'react';

export const ForecastSidebarIcon = () => {
  return (
    <svg
      width='18'
      height='18'
      viewBox='0 0 18 18'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M13.5 4.5V0.75H4.5V6.75'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M7.5 17.25H10.5'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M7.5 9H0.75V17.25H7.5V9Z'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M17.25 6.75H10.5V17.25H17.25V6.75Z'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M13.5 9.75H14.25'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M13.5 12H14.25'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M13.5 14.25H14.25'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M3.75 14.25H4.5'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M3.75 12H4.5'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
    </svg>
  );
};
