import React from 'react';
export const SignatureFlash = () => {
  return (
    <svg
      width='35'
      height='35'
      viewBox='0 0 35 35'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <g filter='url(#filter0_d)'>
        <rect x='-305' y='-29' width='366' height='242' rx='6' fill='white' />
        <rect
          x='-305'
          y='-29'
          width='366'
          height='242'
          rx='6'
          stroke='#2766F6'
          strokeWidth='2'
        />
      </g>
      <circle cx='17.5' cy='17.5' r='17.5' fill='#7359EC' />
      <path
        d='M18.8333 9.5L10.5 18.6667H18L17.1667 24.5L25.5 15.3333H18L18.8333 9.5Z'
        stroke='white'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
        strokeLinejoin='round'
      />
      <defs>
        <filter
          id='filter0_d'
          x='-309'
          y='-31'
          width='374'
          height='250'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
          />
          <feOffset dy='2' />
          <feGaussianBlur stdDeviation='1.5' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.04 0'
          />
          <feBlend
            mode='normal'
            in2='BackgroundImageFix'
            result='effect1_dropShadow'
          />
          <feBlend
            mode='normal'
            in='SourceGraphic'
            in2='effect1_dropShadow'
            result='shape'
          />
        </filter>
      </defs>
    </svg>
  );
};
