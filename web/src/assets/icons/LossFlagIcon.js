import React from 'react';
export const LossFlagIcon = () => {
  return (
    <>
      <path
        d='M0 0V4.8H1.56643V0H0ZM2.34965 0V5.12734L5.15669 8L5.80376 7.34922L5.34484 5.2H8.61538V3.43594L7.17056 0H2.34965Z'
        fill='white'
      />
    </>
  );
};
