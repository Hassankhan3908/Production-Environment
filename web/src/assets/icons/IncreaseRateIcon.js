import React from 'react';
export const IncreaseRateIcon = () => {
  return (
    <svg
      width='19'
      height='11'
      viewBox='0 0 19 11'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M12.8333 0L14.9325 2.09917L10.4592 6.5725L6.7925 2.90583L0 9.7075L1.2925 11L6.7925 5.5L10.4592 9.16667L16.2342 3.40083L18.3333 5.5V0H12.8333Z'
        fill='currentColor'
      />
    </svg>
  );
};
