import React from 'react';

export const DropIcon = () => {
  return (
    <>
      <svg
        width='10'
        height='6'
        viewBox='0 0 10 6'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          d='M1.23552 0L0 1.18885L5 6L10 1.18885L8.76448 0L5 3.62229L1.23552 0Z'
          fill='currentColor'
        />
      </svg>
    </>
  );
};
