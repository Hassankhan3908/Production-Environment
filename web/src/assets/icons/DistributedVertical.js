import React from 'react';

export const DistributedVertical = () => {
  return (
    <svg
      width='23'
      height='23'
      viewBox='0 0 23 23'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <g clipPath='url(#clip0)'>
        <path
          d='M18.2077 8.625H4.79102V14.375H18.2077V8.625Z'
          stroke='currentColor'
          strokeWidth='2'
          strokeMiterlimit='10'
          strokeLinecap='square'
        />
        <path
          d='M22.0413 1.9165H0.958008'
          stroke='currentColor'
          strokeWidth='2'
          strokeMiterlimit='10'
          strokeLinecap='square'
        />
        <path
          d='M22.0413 21.083H0.958008'
          stroke='currentColor'
          strokeWidth='2'
          strokeMiterlimit='10'
          strokeLinecap='square'
        />
      </g>
      <defs>
        <clipPath id='clip0'>
          <rect width='23' height='23' fill='white' />
        </clipPath>
      </defs>
    </svg>
  );
};
