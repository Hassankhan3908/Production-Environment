import React from 'react';

export const NestedCircles = () => {
  return (
    <svg
      width='21'
      height='20'
      viewBox='0 0 21 20'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M10.499 18.33C15.2845 18.33 19.164 14.4505 19.164 9.665C19.164 4.87945 15.2845 1 10.499 1C5.71344 1 1.83398 4.87945 1.83398 9.665C1.83398 14.4505 5.71344 18.33 10.499 18.33Z'
        stroke='currentColor'
        strokeWidth='2'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M10.5009 15.1791C13.5463 15.1791 16.015 12.7103 16.015 9.66497C16.015 6.61962 13.5463 4.15088 10.5009 4.15088C7.45556 4.15088 4.98682 6.61962 4.98682 9.66497C4.98682 12.7103 7.45556 15.1791 10.5009 15.1791Z'
        stroke='currentColor'
        strokeWidth='2'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M10.4984 12.0281C11.8036 12.0281 12.8616 10.9701 12.8616 9.66494C12.8616 8.35979 11.8036 7.30176 10.4984 7.30176C9.19329 7.30176 8.13525 8.35979 8.13525 9.66494C8.13525 10.9701 9.19329 12.0281 10.4984 12.0281Z'
        stroke='currentColor'
        strokeWidth='2'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
    </svg>
  );
};
