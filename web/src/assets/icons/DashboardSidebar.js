import React from 'react';

export const DashboardSidebar = () => {
  return (
    <svg
      width='19'
      height='16'
      viewBox='0 0 19 16'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M9.25 1V3.25'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M15.0834 3.4165L13.4927 5.00725'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M17.5 9.25H15.25'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M5.5 5.5L8.1895 8.1895'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M1 9.25H3.25'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M9.25 10.75C10.0784 10.75 10.75 10.0784 10.75 9.25C10.75 8.42157 10.0784 7.75 9.25 7.75C8.42157 7.75 7.75 8.42157 7.75 9.25C7.75 10.0784 8.42157 10.75 9.25 10.75Z'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M15.6137 14.5C16.792 13.0735 17.5 11.2442 17.5 9.25C17.5 4.69375 13.8062 1 9.25 1C4.69375 1 1 4.69375 1 9.25C1 11.2442 1.708 13.0735 2.88625 14.5H15.6137Z'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
    </svg>
  );
};
