import React from 'react';

export const AccountActivated = () => {
  return (
    <>
      <svg
        width='19'
        height='23'
        viewBox='0 0 17 21'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <path
          d='M15.7854 10.3445L9.58449 12.6245L6.21631 3.26273L12.3999 1L15.7854 10.3445Z'
          fill='white'
        />
        <path
          d='M15.7509 10.3618L9.56724 12.6245L7.87451 7.94364L14.0754 5.68091L15.7509 10.3618Z'
          fill='#7359EC'
        />
        <path
          d='M9.87812 12.5036L9.58449 12.6073L9.87812 12.5036ZM9.06631 2.22638L6.21631 3.26274L7.89176 7.94365L10.759 6.90729L9.06631 2.22638Z'
          fill='#E5E4E4'
        />
        <path
          d='M10.7591 6.88998L7.89185 7.94362L9.58457 12.6245L9.87821 12.5036L12.4346 11.5191L10.7591 6.88998Z'
          fill='#583CD8'
        />
        <path
          d='M6.21631 3.26273L12.3999 1L15.7854 10.3445L9.58449 12.6245'
          stroke='white'
          strokeMiterlimit='10'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
        <path
          d='M10.5691 10.5L4.38545 12.7627L1 3.4182L7.20091 1.1382L10.5691 10.5Z'
          fill='white'
        />
        <path
          d='M10.569 10.5L8.99723 11.07L7.32178 6.38906L8.87632 5.81906L10.569 10.5Z'
          fill='#7359EC'
        />
        <path
          d='M7.33909 6.38909L2.69273 8.08181L1 3.41818L5.64636 1.70818L7.33909 6.38909Z'
          fill='#7359EC'
        />
        <path
          d='M9.56727 12.6246L10.5691 10.5M6.99363 20L1 3.4182L6.99363 20ZM10.5691 10.5L4.38545 12.7627L1 3.4182L7.20091 1.1382L10.5691 10.5Z'
          stroke='white'
          strokeMiterlimit='10'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
      </svg>
    </>
  );
};
