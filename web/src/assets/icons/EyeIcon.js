import React from 'react';

export const EyeIcon = () => {
  return (
    <svg
      width='21'
      height='16'
      viewBox='0 0 21 16'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M1 7.99998C1 7.99998 4.33333 1.33331 10.1667 1.33331C16 1.33331 19.3333 7.99998 19.3333 7.99998C19.3333 7.99998 16 14.6666 10.1667 14.6666C4.33333 14.6666 1 7.99998 1 7.99998Z'
        stroke='#8F92A1'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M10.1665 10.5C11.5472 10.5 12.6665 9.38071 12.6665 8C12.6665 6.61929 11.5472 5.5 10.1665 5.5C8.78579 5.5 7.6665 6.61929 7.6665 8C7.6665 9.38071 8.78579 10.5 10.1665 10.5Z'
        stroke='#8F92A1'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
};
