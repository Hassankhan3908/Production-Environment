import React from 'react';

export const SadFace = () => {
  return (
    <svg
      width='22'
      height='22'
      viewBox='0 0 22 22'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <g clipPath='url(#clip0)'>
        <path
          d='M10.9998 21.0832C16.5687 21.0832 21.0832 16.5687 21.0832 10.9998C21.0832 5.43097 16.5687 0.916504 10.9998 0.916504C5.43097 0.916504 0.916504 5.43097 0.916504 10.9998C0.916504 16.5687 5.43097 21.0832 10.9998 21.0832Z'
          stroke='currentColor'
          strokeWidth='2'
          strokeMiterlimit='10'
          strokeLinecap='square'
        />
        <path
          d='M6.875 11C7.63439 11 8.25 10.3844 8.25 9.625C8.25 8.86561 7.63439 8.25 6.875 8.25C6.11561 8.25 5.5 8.86561 5.5 9.625C5.5 10.3844 6.11561 11 6.875 11Z'
          fill='#2766F6'
        />
        <path
          d='M15.125 11C15.8844 11 16.5 10.3844 16.5 9.625C16.5 8.86561 15.8844 8.25 15.125 8.25C14.3656 8.25 13.75 8.86561 13.75 9.625C13.75 10.3844 14.3656 11 15.125 11Z'
          fill='#2766F6'
        />
        <path
          d='M8.25 16.5C8.25 15.7707 8.53973 15.0712 9.05546 14.5555C9.57118 14.0397 10.2707 13.75 11 13.75C11.7293 13.75 12.4288 14.0397 12.9445 14.5555C13.4603 15.0712 13.75 15.7707 13.75 16.5'
          stroke='currentColor'
          strokeWidth='2'
          strokeMiterlimit='10'
        />
      </g>
      <defs>
        <clipPath id='clip0'>
          <rect width='22' height='22' fill='white' />
        </clipPath>
      </defs>
    </svg>
  );
};
