import React from 'react';

export const SigntureDislike = () => {
  return (
    <svg
      width='35'
      height='35'
      viewBox='0 0 35 35'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <g filter='url(#filter0_d)'>
        <rect x='-305' y='-29' width='366' height='242' rx='3' fill='white' />
        <rect
          x='-305'
          y='-29'
          width='366'
          height='242'
          rx='3'
          stroke='#ECECF2'
        />
      </g>
      <circle cx='17.5' cy='17.5' r='17.5' fill='#EA3A50' />
      <g clipPath='url(#clip0)'>
        <path
          d='M14.75 11.625H11.625V18.5H14.75'
          stroke='white'
          strokeWidth='2'
          strokeMiterlimit='10'
        />
        <path
          d='M14.75 18.5L16.625 25.375C17.1223 25.375 17.5992 25.1775 17.9508 24.8258C18.3025 24.4742 18.5 23.9973 18.5 23.5V19.75H23.1875C23.4578 19.75 23.7249 19.6915 23.9705 19.5786C24.2162 19.4657 24.4345 19.301 24.6105 19.0959C24.7865 18.8907 24.916 18.6499 24.9903 18.39C25.0645 18.1301 25.0817 17.8572 25.0406 17.59L24.3675 13.215C24.2995 12.7726 24.0755 12.3692 23.7359 12.0776C23.3964 11.786 22.9638 11.6255 22.5163 11.625H14.75V18.5Z'
          stroke='white'
          strokeWidth='2'
          strokeMiterlimit='10'
          strokeLinecap='square'
        />
      </g>
      <defs>
        <filter
          id='filter0_d'
          x='-308.5'
          y='-30.5'
          width='373'
          height='249'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
          />
          <feOffset dy='2' />
          <feGaussianBlur stdDeviation='1.5' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.04 0'
          />
          <feBlend
            mode='normal'
            in2='BackgroundImageFix'
            result='effect1_dropShadow'
          />
          <feBlend
            mode='normal'
            in='SourceGraphic'
            in2='effect1_dropShadow'
            result='shape'
          />
        </filter>
        <clipPath id='clip0'>
          <rect
            width='15'
            height='15'
            fill='white'
            transform='translate(11 11)'
          />
        </clipPath>
      </defs>
    </svg>
  );
};
