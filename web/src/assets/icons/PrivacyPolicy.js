import React from 'react';

export const PrivacyPolicy = () => {
  return (
    <svg
      width='23'
      height='23'
      viewBox='0 0 23 23'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M4.31283 6.70817C5.63601 6.70817 6.70866 5.63552 6.70866 4.31234C6.70866 2.98915 5.63601 1.9165 4.31283 1.9165C2.98964 1.9165 1.91699 2.98915 1.91699 4.31234C1.91699 5.63552 2.98964 6.70817 4.31283 6.70817Z'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M18.6878 6.70817C20.011 6.70817 21.0837 5.63552 21.0837 4.31234C21.0837 2.98915 20.011 1.9165 18.6878 1.9165C17.3646 1.9165 16.292 2.98915 16.292 4.31234C16.292 5.63552 17.3646 6.70817 18.6878 6.70817Z'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
      />
      <path
        d='M9.5835 13.2611C7.46366 12.5423 6.59637 9.58301 3.8335 9.58301C3.071 9.58301 2.33973 9.88591 1.80056 10.4251C1.2614 10.9642 0.958496 11.6955 0.958496 12.458V21.083H6.7085V15.333'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
      <path
        d='M13.417 13.2611C15.5368 12.5423 16.4041 9.58301 19.167 9.58301C19.9295 9.58301 20.6608 9.88591 21.1999 10.4251C21.7391 10.9642 22.042 11.6955 22.042 12.458V21.083H16.292V15.333'
        stroke='currentColor'
        strokeWidth='1.5'
        strokeMiterlimit='10'
        strokeLinecap='square'
      />
    </svg>
  );
};
