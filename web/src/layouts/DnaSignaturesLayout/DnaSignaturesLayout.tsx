import { Layout } from '../layout'
type DnaSignatureLayoutProps = {
  children: React.ReactNode
}

const DnaSignaturesLayout = ({ children }: DnaSignatureLayoutProps) => {
  return (
    <div className="rw-scaffold">
      <main className="rw-main">{children}</main>
    </div>
  )
}

export default DnaSignaturesLayout
