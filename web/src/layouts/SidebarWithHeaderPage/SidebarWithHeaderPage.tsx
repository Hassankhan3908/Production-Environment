import React, { ReactNode } from 'react'
import { Link, NavLink, routes, useMatch } from '@redwoodjs/router'

import {
  IconButton,
  Box,
  CloseButton,
  Flex,
  HStack,
  Icon,
  useColorModeValue,
  Drawer,
  DrawerContent,
  Text,
  useDisclosure,
  BoxProps,
  FlexProps,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { IconType } from 'react-icons'
import { FiHome, FiTrendingUp, FiCompass, FiMenu, FiBell } from 'react-icons/fi'

import { AccountMenu } from 'src/layouts/AccountMenu/AccountMenu'
import { MainIcon } from 'src/assets/icons'
interface LinkItemProps {
  name: string
  icon: IconType
  route: string
}
const LinkItems: Array<LinkItemProps> = [
  { name: 'layout:mainMenu.companyDNA', icon: FiHome, route: '/dna' },
  {
    name: 'layout:mainMenu.opportunities',
    icon: FiTrendingUp,
    route: '/opportunities',
  },
  {
    name: 'layout:mainMenu.accounts',
    icon: FiCompass,
    route: '/accounts',
  },
  { name: 'layout:mainMenu.qbrDashboard', icon: FiCompass, route: '/admin' },
  { name: 'layout:mainMenu.simulation', icon: FiCompass, route: '/admin' },
]
export const activeStyling = {
  border: '2px',
  borderColor: 'PersonIcons.border',
  borderRadius: 'md',
  bgColor: 'menuItem.bgColor',
}
export const defaultStyling = {
  borderColor: 'badge.textColor',
  bgColor: 'badge.textColor',
  borderLeftColor: 'badge.textColor',
  color: 'gray.500',
}

export default function SidebarWithHeader({
  children,
}: {
  children: ReactNode
}) {
  const { isOpen, onOpen, onClose } = useDisclosure()
  return (
    <Box minH="100vh" bg={useColorModeValue('gray.100', 'gray.900')}>
      <SidebarContent
        onClose={() => onClose}
        display={{ base: 'none', md: 'block' }}
      />
      <Drawer
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        returnFocusOnClose={false}
        onOverlayClick={onClose}
        size="full"
      >
        <DrawerContent>
          <SidebarContent onClose={onClose} />
        </DrawerContent>
      </Drawer>
      {/* mobilenav */}
      <MobileNav onOpen={onOpen} />
      <Box ml={{ base: 0, md: 60 }} p="4" h="48">
        {children}
      </Box>
    </Box>
  )
}

interface SidebarProps extends BoxProps {
  onClose: () => void
}

const SidebarContent = ({ onClose, ...rest }: SidebarProps) => {
  const { t } = useTranslation()
  return (
    <Box
      transition="3s ease"
      bg={useColorModeValue('white', 'gray.900')}
      borderRight="1px"
      borderRightColor={useColorModeValue('gray.200', 'gray.700')}
      w={{ base: 'full', md: 56 }}
      pos="fixed"
      h="full"
      {...rest}
    >
      <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
        <Box fontSize={20}>
          <Icon fontSize="xx-large" pr="2">
            <MainIcon />
          </Icon>
          sales
          <Box display="inline-block" fontWeight="bold">
            DNA
          </Box>
        </Box>
        <CloseButton display={{ base: 'flex', md: 'none' }} onClick={onClose} />
      </Flex>
      {LinkItems.map((link) => (
        <NavItem
          key={link.name}
          icon={link.icon}
          route={link.route}
          fontSize="12"
        >
          {t(link.name)}
        </NavItem>
      ))}
    </Box>
  )
}

interface NavItemProps extends FlexProps {
  icon: IconType
  route: string
  children: string
}
const NavItem = ({ icon, route, children, ...rest }: NavItemProps) => {
  const matchInfo = useMatch(route)
  return (
    <div>
      <NavLink
        activeClassName="activeLink"
        href="#"
        style={{ textDecoration: 'none' }}
        to={route}
      >
        <Flex
          align="center"
          p="4"
          mx="4"
          borderRadius="lg"
          role="group"
          cursor="pointer"
          color="gray.600"
          {...(matchInfo.match ? activeStyling : defaultStyling)}
          _hover={{
            bg: 'gray.200',
          }}
          {...rest}
        >
          {icon && <Icon mr="4" fontSize="16" as={icon} />}
          {children}
        </Flex>
      </NavLink>
    </div>
  )
}

interface MobileProps extends FlexProps {
  onOpen: () => void
}
const MobileNav = ({ onOpen, ...rest }: MobileProps) => {
  return (
    <Flex
      ml={{ base: 0, md: 60 }}
      px={{ base: 4, md: 4 }}
      height="14"
      alignItems="center"
      bg={useColorModeValue('white', 'gray.900')}
      borderBottomWidth="1px"
      borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
      justifyContent={{ base: 'space-between', md: 'flex-end' }}
      {...rest}
    >
      <IconButton
        display={{ base: 'flex', md: 'none' }}
        onClick={onOpen}
        variant="outline"
        aria-label="open menu"
        icon={<FiMenu />}
      />

      <Box fontSize={20} display={{ base: 'flex', md: 'none' }}>
        <Icon fontSize="xx-large" pr="2">
          <MainIcon />
        </Icon>
        sales
        <Box display="inline-block" fontWeight="bold">
          DNA
        </Box>
      </Box>

      <HStack spacing={{ base: '0', md: '6' }}>
        <IconButton
          size="lg"
          variant="ghost"
          aria-label="open menu"
          icon={<FiBell />}
        />
        <AccountMenu />
      </HStack>
    </Flex>
  )
}
