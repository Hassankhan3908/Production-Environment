import { render } from '@redwoodjs/testing/web'

import SidebarWithHeaderPage from './SidebarWithHeaderPage'

describe('SidebarWithHeaderPage', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<SidebarWithHeaderPage />)
    }).not.toThrow()
  })
})
