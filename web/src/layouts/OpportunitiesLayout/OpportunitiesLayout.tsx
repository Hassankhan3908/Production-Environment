type OpportunityLayoutProps = {
  children: React.ReactNode
}

const OpportunitiesLayout = ({ children }: OpportunityLayoutProps) => {
  return (
    <div className="rw-scaffold">
      <main className="rw-main">{children}</main>
    </div>
  )
}

export default OpportunitiesLayout
