import React, { useState } from 'react'

import { Flex, useDisclosure } from '@chakra-ui/react'

import { LayoutContext } from './LayoutContext/LayoutContext'
import { Viewport } from 'src/common/components/Viewport'

import SidebarWithHeader from './SidebarWithHeaderPage/SidebarWithHeaderPage'
import { useAuth } from '@redwoodjs/auth'
import { LocationProvider } from '@redwoodjs/router'

export const Layout = ({ children }) => {
  const [isFocusMode, setIsFocusMode] = useState(false)
  const { isAuthenticated } = useAuth()

  const {
    isOpen: navIsOpen,
    onClose: navOnClose,
    onOpen: navOnOpen,
  } = useDisclosure()

  return (
    <LayoutContext.Provider
      value={{ isFocusMode, setIsFocusMode, navIsOpen, navOnClose, navOnOpen }}
    >
      <LocationProvider>
        <Viewport>
          {isAuthenticated && !isFocusMode ? (
            <SidebarWithHeader> {children}</SidebarWithHeader>
          ) : (
            <Flex flex="1" direction="column">
              {children}
            </Flex>
          )}
        </Viewport>
      </LocationProvider>
    </LayoutContext.Provider>
  )
}
