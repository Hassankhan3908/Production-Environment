import React from 'react'
import { useAuth } from '@redwoodjs/auth'

import {
  Avatar,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuGroup,
  MenuDivider,
  Spinner,
  useColorMode,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { FiLogOut, FiMoon, FiSun, FiUser } from 'react-icons/fi'
import { Link, useHistory } from 'react-router-dom'

import { Icon } from 'src/common/components'
import { useDarkMode } from 'src/common/hooks/useDarkMode'

export const AccountMenu = ({ ...rest }) => {
  const { t } = useTranslation('layout')
  const { colorModeValue } = useDarkMode()
  const { colorMode, toggleColorMode } = useColorMode()
  const { loading, userMetadata, logOut } = useAuth()
  const history = useHistory()

  return (
    <Menu placement="bottom-end" {...rest}>
      <MenuButton borderRadius="full" _focus={{ shadow: 'outline' }}>
        <Avatar
          size="sm"
          icon={<></>}
          name={!loading && `${userMetadata?.name}`}
        >
          {loading && <Spinner size="xs" />}
        </Avatar>
      </MenuButton>
      <MenuList
        zIndex="100"
        color={colorModeValue('gray.800', 'white')}
        maxW="12rem"
        overflow="hidden"
      >
        <MenuItem
          icon={
            <Icon
              icon={colorMode === 'dark' ? FiSun : FiMoon}
              fontSize="lg"
              color="gray.400"
            />
          }
          onClick={() => toggleColorMode()}
        >
          {colorMode === 'dark'
            ? t('layout:accountMenu.switchColorModeLight')
            : t('layout:accountMenu.switchColorModeDark')}
        </MenuItem>
        <MenuDivider />
        <MenuItem
          icon={<Icon icon={FiLogOut} fontSize="lg" color="gray.400" />}
          onClick={() => logOut({ returnTo: process.env.AUTH0_REDIRECT_URI })}
        >
          {t('layout:accountMenu.logout')}
        </MenuItem>
      </MenuList>
    </Menu>
  )
}
