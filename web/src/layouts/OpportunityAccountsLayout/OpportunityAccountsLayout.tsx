import { Link, routes } from '@redwoodjs/router'
import { Toaster } from '@redwoodjs/web/toast'

type OpportunityAccountLayoutProps = {
  children: React.ReactNode
}

const OpportunityAccountsLayout = ({
  children,
}: OpportunityAccountLayoutProps) => {
  return (
    <div className="rw-scaffold">
      <main className="rw-main">{children}</main>
    </div>
  )
}

export default OpportunityAccountsLayout
