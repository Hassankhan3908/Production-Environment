import React from 'react';
import { Image } from '@chakra-ui/react';
import logo1 from './pngs/bof.png';
import logo2 from './pngs/adobe.png';
import logo3 from './pngs/broadcom.png';
import logo4 from './pngs/cisco.png';
import logo5 from './pngs/intel.png';
import logo6 from './pngs/okta.png';
import logo7 from './pngs/snowflake.png';
import logo8 from './pngs/vm.png';
export const Logos = {
  'Bank of America': logo1,
  Adobe: logo2,
  Broadcom: logo3,
  Cisco: logo4,
  Intel: logo5,
  Okta: logo6,
  Snowflake: logo7,
  VMware: logo8,
};

export const GetIcon = ({ icon }) => {
  return <Image bgColor='white' src={Logos[icon]} />;
};
