import React from 'react'
import {
  Box,
  Flex,
  Heading,
  ListItem,
  UnorderedList,
  Text,
} from '@chakra-ui/react'
import { StarLevels } from 'src/common/components'
export const SummaryComponent = ({
  data,
  title,
  summaryLevel,
  children,
  heat,
  score,
}) => {
  return (
    <Flex
      flexDir="column"
      bgColor="white"
      width={270}
      px={5}
      py={5}
      shadow="md"
      borderRadius="md"
    >
      <Text color="general.gray">{title}</Text>
      <Box my={3}>
        {heat ? (
          <>{score === 3 ? '🔥🔥🔥' : score === 2 ? '🔥🔥' : '🔥'} </>
        ) : (
          <StarLevels score={score} />
        )}
      </Box>

      <Heading fontWeight="semibold" size="md">
        {summaryLevel}
      </Heading>
      <UnorderedList color="general.gray">
        {data &&
          data.map((element, index) => {
            return (
              <ListItem key={index} my={4}>
                {element.recommendation || element.name}
              </ListItem>
            )
          })}
      </UnorderedList>
      {children}
    </Flex>
  )
}
