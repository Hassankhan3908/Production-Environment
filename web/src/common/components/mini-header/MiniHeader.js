import React from 'react'
import { Spacer, Flex, Box } from '@chakra-ui/react'
import { TitleWithIcon } from '../design-components/title-with-icon/TitleWithIcon'

export const MiniHeader = ({ children, title, icon, subtitle }) => {
  return (
    <Flex
      shadow="base"
      width="100%"
      pl="35px"
      bgColor="miniHeader.bgColor"
      py={13}
    >
      <Box display="flex" alignItems="flex-end">
        <TitleWithIcon
          fontSizeText={18}
          fontSizeIcon={27}
          fontSizeBox={18}
          paddingY="0px"
          title={title}
          subtitle={subtitle}
        >
          {icon}
        </TitleWithIcon>
      </Box>
      <Spacer />
      {children}
    </Flex>
  )
}
