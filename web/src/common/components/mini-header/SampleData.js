export const ButtonData1 = [
  { text: 'Current Quarter', id: 'forecast', isActive: true },
  { text: 'Current Quarter+1', id: 'forecast_current1', isActive: false },
  { text: 'Current Quarter+2', id: 'forecast_current2', isActive: false },
];

export const ButtonData2 = [
  { text: 'Current Quarter', id: 'rep_forecast', isActive: true },
  { text: 'Current Quarter+1', id: 'rep_forecast_current1', isActive: false },
  { text: 'Current Quarter+2', id: 'rep_forecast_current2', isActive: false },
];
