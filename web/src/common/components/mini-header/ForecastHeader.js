import React from 'react';
import { Button, Box } from '@chakra-ui/react';
import { ButtonData1, ButtonData2 } from './SampleData';
import { Link, useParams } from 'react-router-dom';

/**
 * MiniHeader is the component to render Header with title and buttons
 */
export const defaultStyling = {
  size: 'sm',
  bgColor: 'miniHeader.ButtonColor',
  mx: 13,
  color: 'general.gray',
  fontWeight: 'normal',
  border: '1px transparent',
};
export const activeStyling = {
  variant: 'outline',
  size: 'sm',
  bgColor: 'badge.textColor',
  mx: 13,
};

export const ForecastHeader = ({ user }) => {
  const { id: currentId } = useParams();
  return (
    <Box>
      {(user.id === 1 ? ButtonData1 : ButtonData2).map(({ text, id }) => (
        <Link key={id} to={`/forecast/${id}`}>
          <Button
            fontSize='small'
            height={8}
            width={32}
            {...(currentId === id ? activeStyling : defaultStyling)}
          >
            {text}
          </Button>
        </Link>
      ))}
    </Box>
  );
};
