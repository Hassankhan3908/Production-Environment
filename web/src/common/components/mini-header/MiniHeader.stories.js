import React from 'react';
import { MiniHeader } from './MiniHeader';
import { IconType } from './IconType';
import { ForecastHeader } from './ForecastHeader';
export default {
  title: 'Components/MiniHeader',
  component: MiniHeader,
};

export const MiniHeaderExample = () => {
  return (
    <MiniHeader title='Forecast' iconType={IconType.FORECAST}>
      <ForecastHeader />
    </MiniHeader>
  );
};
export const MiniHeaderExample1 = () => {
  return <MiniHeader title='Forecast' iconType={IconType.DNA} />;
};
export const MiniHeaderExample2 = () => {
  return <MiniHeader title='Forecast' iconType={IconType.OPPURTUNITY} />;
};
