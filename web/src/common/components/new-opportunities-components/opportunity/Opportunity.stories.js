import React from 'react';
import { Opportunity } from './Opportunity';

export default {
  title: 'Opportunity/Opportunity',
  component: Opportunity,
};

export const OpportunityPageExample = () => {
  return <Opportunity />;
};
