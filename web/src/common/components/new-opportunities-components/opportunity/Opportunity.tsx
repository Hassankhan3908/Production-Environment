import React from 'react'
import { Divider, HStack, Icon, Text, VStack } from '@chakra-ui/react'
import { GetIcon, StarsBox } from 'src/common/components'
import { ArrowRightIcon, TaskStatusIcon } from 'src/assets/icons'

import { CurrencyText } from '../../general/number-text/CurrencyText'
import { DateFormatter } from 'src/common/utils/DateFormatter'
import { Opportunity, OpportunityAccount } from 'types/graphql'

type Override<T1, T2> = Omit<T1, keyof T2> & T2

type Detail = Override<
  Opportunity,
  {
    ui_opportunity_account: Partial<OpportunityAccount>
  }
>
export const OpportunityDetail = ({ data }: { data: Partial<Detail> }) => {
  return (
    <VStack bgColor="white" p={4} shadow="md" borderRadius="md">
      <HStack alignSelf="stretch">
        <Text bgColor="gray.300" py={2} px={4} borderRadius="md">
          {data.opportunity_name || data.ui_opportunity_account?.display_name}
        </Text>
        <VStack alignItems="flex-start">
          <Text fontWeight="bold">
            {data.opportunity_name || data.ui_opportunity_account?.display_name}
          </Text>
          <Text m="0px !important" fontSize="sm" pb={2}>
            {data.ui_opportunity_account?.display_name}
          </Text>
        </VStack>
      </HStack>
      <Divider />
      <VStack width="100%" alignItems="flex-start">
        <HStack alignSelf="stretch" justifyContent="space-between">
          <Text color="general.gray" fontSize="small" fontWeight="semibold">
            Opportunity Amount
          </Text>
          <Text fontWeight="bold" fontSize="smaller">
            <CurrencyText value={data.projected_amt} numberType="long" />
          </Text>
        </HStack>
        <HStack alignSelf="stretch" justifyContent="space-between">
          <Text color="general.gray" fontSize="small" fontWeight="semibold">
            Close by Date
          </Text>
          <Text fontWeight="bold" fontSize="smaller">
            {DateFormatter(data.projected_close_by_date)}
          </Text>
        </HStack>
        <HStack alignSelf="stretch" justifyContent="space-between">
          <Text color="general.gray" fontSize="small" fontWeight="semibold">
            % Tasks Complete
          </Text>
          <Text color="general.blue" fontWeight="bold" fontSize="smaller">
            <Icon fontSize="x-large" position="relative" top={1} left={1}>
              <TaskStatusIcon />
            </Icon>
            {data.pct_actvty_complete}%
          </Text>
        </HStack>
        <HStack mb={2} alignSelf="stretch" justifyContent="space-between">
          <Text color="general.gray" fontSize="small" fontWeight="semibold">
            Predicted Outcome
          </Text>
          <StarsBox value={data.prediction_score} />
        </HStack>
        <HStack
          mb={2}
          alignSelf="stretch"
          justifyContent="space-between"
        ></HStack>
        <Text color="general.blue" fontWeight="semibold" fontSize="smaller">
          View Details
          <Icon ml={2} width={3} height={3} viewBox="0 0 11 11">
            <ArrowRightIcon />
          </Icon>
        </Text>
      </VStack>
    </VStack>
  )
}
