import React from 'react';
import { TasksSummary } from './TasksSummary';

export default {
  title: 'Opportunity/TasksSummary',
  component: TasksSummary,
};

export const TasksSummaryPageExample = () => {
  return <TasksSummary />;
};
