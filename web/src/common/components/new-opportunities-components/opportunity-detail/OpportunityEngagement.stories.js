import React from 'react';
import { OpportunityEngagement } from './OpportunityEngagement';

export default {
  title: 'Opportunity/OpportunityEngagement',
  component: OpportunityEngagement,
};

export const OpportunityEngagementPageExample = () => {
  return <OpportunityEngagement />;
};
