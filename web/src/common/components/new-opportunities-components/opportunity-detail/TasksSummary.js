import React from 'react'
import { Box, Flex, Icon, Text, HStack, Divider } from '@chakra-ui/react'
import { IncreaseRateIcon, SmileyCircleIcon } from 'src/assets/icons'
export const TasksSummary = ({ data }) => {
  return data ? (
    <Flex
      flexDir="column"
      bgColor="white"
      width={270}
      px={5}
      py={5}
      shadow="md"
      borderRadius="md"
    >
      <HStack>
        <Icon width="14" height="14" viewBox="0 0 53 53">
          <SmileyCircleIcon />
        </Icon>
        <Box>
          <Text fontSize="sm" color="general.gray">
            Tasks Completed
          </Text>
          <Text fontSize="large" fontWeight="semibold">
            {data.completed}/{data.total}
          </Text>
        </Box>
      </HStack>
      <Box py={2}>
        <Text color="general.gray">Avg weekly rate:</Text>
        <Text fontWeight="semibold">{data.averageWeeklyRate} Tasks</Text>
      </Box>
      <Box py={2}>
        <Text color="general.gray">Avg daily rate rate:</Text>
        <Text fontWeight="semibold">{data.averageDailyRate} Tasks</Text>
      </Box>
      <Divider />
      <HStack my={2}>
        <Text color="container.green">
          <Icon fontSize="large" position="relative" top={1}>
            <IncreaseRateIcon />
          </Icon>
          {data.trendPercentage}
        </Text>
        <Text>{data.trendText}</Text>
      </HStack>
    </Flex>
  ) : null
}
