import React from 'react'
import { Flex, Text, Icon } from '@chakra-ui/react'
import { StarEmpty, StarIcon } from 'src/assets/icons'
export const OpportunityEngagement = ({ data }) => {
  return (
    <Flex
      flexDir="column"
      bgColor="white"
      width={270}
      px={5}
      py={5}
      shadow="md"
      borderRadius="md"
    >
      <Text my={2} fontSize="sm" fontWeight="semibold" color="general.gray">
        Opportunity Engagement
      </Text>
      <Flex
        px={1}
        py={2}
        bgColor="gray.50"
        borderRadius="md"
        width={20}
        justifyContent="space-around"
      >
        {data.score === 3 ? (
          <>
            <Icon viewBox="0 0 14 13">
              <StarIcon />
            </Icon>
            <Icon viewBox="0 0 14 13">
              <StarIcon />
            </Icon>

            <Icon viewBox="0 0 14 13">
              <StarIcon />
            </Icon>
          </>
        ) : data.score === 2 ? (
          <>
            <Icon viewBox="0 0 14 13">
              <StarIcon />
            </Icon>
            <Icon viewBox="0 0 14 13">
              <StarIcon />
            </Icon>

            <Icon viewBox="0 0 14 13">
              <StarEmpty />
            </Icon>
          </>
        ) : (
          <>
            <Icon viewBox="0 0 14 13">
              <StarEmpty />
            </Icon>
            <Icon viewBox="0 0 14 13">
              <StarEmpty />
            </Icon>
            <Icon viewBox="0 0 14 13">
              <StarEmpty />
            </Icon>
          </>
        )}
      </Flex>
      <Flex flexDir="column" my={4}>
        <Text color="general.gray">Daily Task Streak</Text>
        <Text fontSize="md" fontWeight="bold">
          {data.dailyTaskStreak} days
        </Text>
      </Flex>
      <Flex flexDir="column">
        <Text color="general.gray">Weekly Task Streak</Text>
        <Text fontSize="md" fontWeight="bold">
          {data.weeklyTaskStreak} week
        </Text>
      </Flex>
    </Flex>
  )
}
