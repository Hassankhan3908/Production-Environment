import React from 'react';
import { VStack, HStack, Box } from '@chakra-ui/react';
export const AccountInfo = ({ account }) => {
  const {
    accountName,
    heatLevel,
    numberOfEngagedContacts,
    numberOfOpenOpportunities,
    sengagedContactsPerformance,
    openOpportunitiesPerformance,
  } = account;
  return (
    <VStack
      bgColor='white'
      borderRadius='md'
      shadow='lg'
      border='1px'
      borderColor='gray.100'
      px={4}
      py={3}
      alignItems='flex-start'
    >
      <HStack mb={5}>
        <Box
          border='2px'
          borderRadius='md'
          borderColor='general.blueBorderColor'
          bgColor='badge.textColor'
          display='inline-block'
        >
          {accountName}, {heatLevel},{numberOfEngagedContacts},
          {numberOfOpenOpportunities},{sengagedContactsPerformance},
          {openOpportunitiesPerformance}
        </Box>
      </HStack>
    </VStack>
  );
};
