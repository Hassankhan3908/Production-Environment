import React from 'react';
import { ParentContainer } from './ParentContainer';
import { OverviewIcon } from 'src/assets/icons';
import {
  IndividualOverviewContainer,
  PowerScoresComponent,
  DateComponent,
  CurrencyText,
} from '@salesdna/ui';
import { SimpleGrid } from '@chakra-ui/layout';
export default {
  title: 'Opportunity/ParentContainer',
  component: ParentContainer,
};

const sampleData = [
  {
    title: 'Salesforce Reported Closing Date',
    type: 'date',
    data: {
      date: 'Dec 20th, 2020',
      days: '54',
    },
  },
  {
    title: 'Projected Amount',
    type: 'amount',
    data: {
      amount: 425750,
    },
  },
  {
    title: 'Industry',
    type: 'powerScores',
    data: {
      text: 'Financial Services',
      powerScores: 3.5,
    },
  },
  {
    title: 'Incumbent',
    type: 'powerScores',
    data: {
      text: 'CA',
      powerScores: 3.5,
    },
  },
  {
    title: 'Opportunity Sources',
    type: 'powerScores',
    data: {
      text: 'Marketing',
      powerScores: 3.5,
    },
  },
  {
    title: 'Competitor',
    type: 'powerScores',
    data: {
      text: 'Dynatrace',
      powerScores: 3.5,
    },
  },
];

export const ParentContainerPageExample = () => {
  return (
    <ParentContainer icon={<OverviewIcon />} title='OverView'>
      <SimpleGrid
        // minChildWidth='220px'
        columns={3}
        spacingX='40px'
        spacingY='20px'
        width='100%'
      >
        {sampleData.map(({ title, data, type }) => {
          return (
            <IndividualOverviewContainer title={title}>
              {type === 'date' ? (
                <DateComponent />
              ) : type === 'amount' ? (
                <CurrencyText value={data.amount} numberType='long' />
              ) : (
                <PowerScoresComponent />
              )}
            </IndividualOverviewContainer>
          );
        })}
      </SimpleGrid>
    </ParentContainer>
  );
};
