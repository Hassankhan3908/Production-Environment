import React from 'react';
import { Box, HStack, Text } from '@chakra-ui/react';
export const ParentContainer = ({ title, children, icon }) => {
  return (
    <Box
      width='100%'
      bgColor='white'
      borderRadius='md'
      shadow='md'
      pb={10}
      px={5}
      mb={5}
    >
      <HStack pl={4} pt={3}>
        <Text>{icon}</Text>
        <Text fontSize='large' fontWeight='semibold'>
          {title}
        </Text>
      </HStack>
      {children}
    </Box>
  );
};
