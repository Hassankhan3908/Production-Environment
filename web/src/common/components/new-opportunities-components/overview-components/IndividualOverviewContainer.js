import React from 'react';
import { Box, Divider, Text } from '@chakra-ui/react';
export const IndividualOverviewContainer = ({
  title = 'predicted',
  children,
}) => {
  return (
    <Box bgColor='white' width={72} px={4} py={2}>
      <Text color='general.gray'>{title}</Text>
      <Divider my={3} />
      {children}
    </Box>
  );
};
