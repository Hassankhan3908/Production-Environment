import React from 'react';
import { Text } from '@chakra-ui/react';
export const PowerScoresComponent = ({ powerScores }) => {
  return (
    <Text fontSize='sm' ml={1}>
      Power Score: {powerScores}x
    </Text>
  );
};
