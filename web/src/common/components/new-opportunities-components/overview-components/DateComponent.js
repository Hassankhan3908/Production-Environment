import React from 'react';
import { VStack, Box, Text } from '@chakra-ui/react';
export const DateComponent = ({ date, days }) => {
  return (
    <VStack alignItems='flex-start'>
      <Text fontWeight='semibold'>{date}</Text>
      <Box
        borderRadius='md'
        px={3}
        py={1}
        fontWeight='semibold'
        bgColor='menuItem.bgColor'
        color='general.blue'
      >
        {days} days away
      </Box>
    </VStack>
  );
};
