import React from 'react';
import { IndividualOverviewContainer } from './IndividualOverviewContainer';
import { PowerScoresComponent } from './PowerScoresComponent';
import { DateComponent } from './DateComponent';
export default {
  title: 'Opportunity/IndividualOverviewContainer',
  component: IndividualOverviewContainer,
};

export const IndividualOverviewContainerPageExample = () => {
  return (
    <IndividualOverviewContainer>
      <PowerScoresComponent powerScores={3.5} text='Financial services' />
    </IndividualOverviewContainer>
  );
};
export const IndividualOverviewContainerPageExampleAmount = () => {
  return (
    <IndividualOverviewContainer>
      <PowerScoresComponent powerScores={3.5} text='Financial services' />
    </IndividualOverviewContainer>
  );
};
export const IndividualOverviewContainerPageExampleClosingDate = () => {
  return (
    <IndividualOverviewContainer>
      <DateComponent date='Dec 20th, 2020' days='54' />
    </IndividualOverviewContainer>
  );
};
