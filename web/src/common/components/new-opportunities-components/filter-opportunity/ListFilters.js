import React, { useEffect, useState } from 'react'
import {
  Box,
  HStack,
  Select,
  PopoverTrigger,
  Popover,
  Button,
  PopoverHeader,
  PopoverArrow,
  PopoverContent,
  PopoverCloseButton,
  PopoverFooter,
  PopoverBody,
  ButtonGroup,
} from '@chakra-ui/react'
import CheckboxTree from 'react-checkbox-tree'

import { ChevronDownIcon } from '@chakra-ui/icons'

const PopTree = ({ children, setId, id }) => {
  const [isOpen, setIsOpen] = useState(false)
  const open = () => setIsOpen(!isOpen)
  const close = () => setIsOpen(false)
  const [list, setList] = useState()
  useEffect(() => {})
  return (
    <>
      <Popover
        _focus={{ boxShadow: 'none' }}
        returnFocusOnClose={false}
        isOpen={isOpen}
        onClose={close}
        placement="right"
        closeOnBlur={false}
      >
        <PopoverTrigger>
          <Button
            fontSize="small"
            width="130"
            fontWeight="italic"
            iconColor="general.blue"
            bgColor="white"
            mr={5}
            onClick={open}
          >
            Select Owner <ChevronDownIcon color="general.blue" boxSize="20px" />
          </Button>
        </PopoverTrigger>
        <PopoverContent>
          <PopoverArrow />
          <PopoverBody>
            {React.cloneElement(children, { setList, id })}
          </PopoverBody>
          <PopoverFooter d="flex" justifyContent="flex-end">
            <ButtonGroup size="sm">
              <Button
                colorScheme="red"
                onClick={() => {
                  setId(list)
                  close()
                }}
              >
                Apply
              </Button>
            </ButtonGroup>
          </PopoverFooter>
        </PopoverContent>
      </Popover>
    </>
  )
}
export class Widget extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      checked: [],
      expanded: [],
    }
  }
  componentDidMount() {
    this.setState({ checked: this.props.id.split('&id=') })
  }

  componentDidUpdate(prevProps, prevState) {
    this.props.setList(
      this.state.checked.reduce(
        (str, current, index, ar) =>
          (str +=
            `${index === 0 ? '' : 'id='}${current}` +
            `${index < ar.length - 1 ? '&' : ''}`),
        ''
      )
    )
  }
  render() {
    return (
      <CheckboxTree
        style={({ overflow: 'scroll' }, { height: '400px' })}
        nodes={this.props.orgTree}
        checked={this.state.checked}
        expanded={this.state.expanded}
        onCheck={(checked) => {
          this.setState({ checked })
        }}
        onExpand={(expanded) => this.setState({ expanded })}
        icons={{
          expandClose: <span className="rct-icon rct-icon-expand-close" />,
          expandOpen: <span className="rct-icon rct-icon-expand-open" />,
          expandAll: <span className="rct-icon rct-icon-expand-all" />,
          collapseAll: <span className="rct-icon rct-icon-collapse-all" />,
          parentClose: (
            <span style={{ color: '#2766f6' }} className="fas fa-user-alt" />
          ),
          parentOpen: (
            <span style={{ color: '#2766f6' }} className="fas fa-user-alt" />
          ),
          leaf: (
            <span style={{ color: '#2766f6' }} className="fas fa-user-alt" />
          ),
        }}
      />
    )
  }
}
export const ListFilters = ({
  setSort,
  sort,
  listOfSalesRep,
  user,
  id,
  setId,
  orgTree,
  listOfTheaters,
  theater,
  setTheater,
}) => {
  return (
    <>
      <HStack pl={10} justifyContent="flex-start" width="100%">
        <Box color="general.gray" fontSize="small">
          List filters
        </Box>

        {user.res?.custom_permissions?.sdna_cro && (
          <Select
            fontSize="small"
            width="130"
            iconColor="general.blue"
            bgColor="white"
            placeholder="All Opportunities"
            onChange={(e) => setId(e.target.value)}
          >
            {listOfSalesRep?.map(({ first_name, last_name, sf_id }) => (
              <option
                value={sf_id}
                selected={sf_id === id ? true : false}
                key={sf_id}
              >{`${first_name} ${last_name}`}</option>
            ))}
          </Select>
        )}
        {user?.res?.custom_permissions?.sdna_cro && (
          <Select
            fontSize="small"
            width="130"
            iconColor="general.blue"
            bgColor="white"
            placeholder="All Theaters"
            onChange={(e) => setTheater(e.target.value)}
          >
            {listOfTheaters?.map(({ attrib_value }) => (
              <option
                value={attrib_value}
                selected={attrib_value === theater ? true : false}
                key={attrib_value}
              >{`${attrib_value === null ? 'N/A' : attrib_value}`}</option>
            ))}
          </Select>
        )}
        <Select
          fontSize="small"
          width="130"
          iconColor="general.blue"
          bgColor="white"
          onChange={(e) => setSort(e.target.value)}
        >
          {[
            { value: 'CLOSE_DATE', text: 'Sort by: Close by Date' },
            { value: 'TASK_COMPLETE', text: 'Sort by: Percent Complete' },
            { value: 'AMOUNT', text: 'Sort by: Amount' },
          ].map(({ value, text }, index) => (
            <option
              value={value}
              selected={value === sort ? true : false}
              key={index}
            >
              {text}
            </option>
          ))}
        </Select>
        {orgTree && (
          <PopTree setId={setId} id={id}>
            <Widget orgTree={orgTree} />
          </PopTree>
        )}
      </HStack>
    </>
  )
}
