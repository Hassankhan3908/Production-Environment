import React from 'react';
import { ListFilters } from './ListFilters';

export default {
  title: 'Opportunity/ListFilters',
  component: ListFilters,
};

export const ListFiltersPageExample = () => {
  return <ListFilters />;
};
