import React from 'react';
import { TaskInfo } from './TaskInfo';

export default {
  title: 'Opportunity/TaskInfo',
  component: TaskInfo,
};

export const TaskStatusPageExample = () => {
  return <TaskInfo />;
};
