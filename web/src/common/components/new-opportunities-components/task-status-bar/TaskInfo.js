import React from 'react';
import {
  VStack,
  Icon,
  HStack,
  Text,
  Badge,
  Box,
  Divider,
} from '@chakra-ui/react';
import { TestIcon } from './testIcon';
import { PersonIcon } from 'src/assets/icons';
export const TaskInfo = ({ task }) => {
  const {
    priority,
    name,
    current_value = 3,
    opportunity_name,
    display_status = 'incomplete',
    sla = 3.5,
    max_target_value = 4.5,
  } = task;
  const badge = priority === 1 ? 'High' : priority === 2 ? 'Med' : 'Low';
  return (
    <VStack
      justifyContent='space-between'
      bgColor='white'
      borderRadius='md'
      shadow='lg'
      border='1px'
      borderColor='gray.100'
      px={4}
      py={3}
      alignItems='flex-start'
    >
      <HStack mb={5}>
        <Box
          border='2px'
          borderRadius='md'
          borderColor='general.blueBorderColor'
          bgColor='badge.textColor'
          display='inline-block'
        >
          <Icon
            color='general.blue'
            viewBox='0 -4 14 24'
            fontSize='x-large'
            m={1}
          >
            <PersonIcon />
          </Icon>
        </Box>
        <Text fontWeight='semibold'>{name}</Text>
      </HStack>
      <Text fontStyle='italic' fontSize='sm'>
        {opportunity_name}{' '}
      </Text>
      <VStack width='100%'>
        <HStack alignSelf='stretch' justifyContent='flex-start'>
          <Badge
            borderRadius='md'
            width={24}
            textAlign='center'
            p={1}
            textTransform='capitalize'
            bgColor={`badge.Bg${badge}`}
            color={`badge.Text${badge}`}
            size='sm'
          >
            {badge} priority
          </Badge>
          <Badge
            textAlign='center'
            borderRadius='md'
            p={1}
            width={24}
            textTransform='capitalize'
            bgColor='menuItem.bgColor'
            color='general.blue'
          >
            {display_status}
          </Badge>
        </HStack>
        <Divider />
        <HStack alignSelf='stretch' justifyContent='space-between'>
          <Text color='general.gray'>Current Value</Text>
          <Text fontWeight='semibold'>{current_value}</Text>
        </HStack>
        <Divider />
        <HStack alignSelf='stretch' justifyContent='space-between'>
          <Badge
            textAlign='center'
            borderRadius='sm'
            p={1}
            width={24}
            textTransform='capitalize'
            bgColor='gray.100'
            color='general.gray'
          >
            Value
          </Badge>
          <Badge
            textAlign='center'
            borderRadius='sm'
            p={1}
            width={24}
            textTransform='capitalize'
            bgColor='gray.100'
            color='general.gray'
          >
            Target Value
          </Badge>
        </HStack>
        <Icon margin='0px !important' width={64} height='8' viewBox='0 0 245 8'>
          <TestIcon />
        </Icon>
        <HStack
          mt='-12px !important'
          alignItems='center'
          alignSelf='stretch'
          justifyContent='space-between'
        >
          <Text fontSize='small' fontWeight='semibold'>
            {sla}
          </Text>
          <Text fontSize='small' fontWeight='semibold'>
            {max_target_value}
          </Text>
        </HStack>
      </VStack>
    </VStack>
  );
};
