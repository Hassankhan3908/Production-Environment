import React from 'react';

export const TestIcon = () => {
  return (
    <svg
      width='245'
      height='8'
      viewBox='0 0 245 8'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M0 2C0 0.895431 0.895431 0 2 0H211V8H2C0.895434 8 0 7.10457 0 6V2Z'
        fill='#ECECF2'
      />
      <path
        d='M211 0H243C244.105 0 245 0.895431 245 2V6C245 7.10457 244.105 8 243 8H211V0Z'
        fill='#ECECF2'
      />
    </svg>
  );
};
