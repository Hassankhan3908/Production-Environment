import React from 'react';
import { Badge, Button, HStack, Icon } from '@chakra-ui/react';
import { TaskStatusIcon, CircleTick } from 'src/assets/icons';
export const TotalTasksBar = ({ totalTasks = 10, completedTasks = 5 }) => {
  return (
    <HStack pl={4} py={7}>
      <Button bg='gray.100' size='sm' py={5}>
        <Icon
          color='general.blue'
          fontSize='xx-large'
          position='relative'
          top={2}
        >
          <TaskStatusIcon />
        </Icon>
        Outstanding Tasks
        <Badge
          ml={2}
          bgColor='gray.300'
          color='white'
          fontSize='sm'
          fontWeight='normal'
        >
          {totalTasks}
        </Badge>
      </Button>
      <Button size='sm' color='general.gray' bg='transparent'>
        <Icon
          color='general.blue'
          fontSize='xx-large'
          position='relative'
          top={2}
        >
          <CircleTick />
        </Icon>
        Completed Tasks
        <Badge
          ml={2}
          fontSize='sm'
          fontWeight='semibold'
          bgColor='gray.300'
          color='general.gray'
        >
          {completedTasks}
        </Badge>
      </Button>
    </HStack>
  );
};
