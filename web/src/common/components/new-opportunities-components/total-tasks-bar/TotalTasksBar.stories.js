import React from 'react';
import { TotalTasksBar } from './TotalTasksBar';

export default {
  title: 'Opportunity/TotalTasksBar',
  component: TotalTasksBar,
};

export const TotalTasksBarPageExample = () => {
  return <TotalTasksBar />;
};
