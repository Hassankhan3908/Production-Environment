import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  Icon,
  Text,
  Menu,
  MenuButton,
  MenuList,
  Image,
  MenuItem,
  Button,
} from '@chakra-ui/react';
import { DropIcon } from 'src/assets/icons';
import src from './logout.png';
/**
 * Avatar is used to render profile pic with name and a dropDownButton
 */
export const UserItem = ({ imageUrl, fullName, logout, revokeToken }) => {
  return (
    <Box>
      <Text display='inline-block' pl={2} fontSize='sm'>
        <Menu>
          <Avatar size='sm' name={fullName} />
          <MenuButton
            fontWeight='normal'
            bgColor='transparent'
            as={Button}
            size='sm'
            _hover={{ bgColor: 'transparent' }}
          >
            Hi {fullName}
            <Icon ml={2} fontSize='xx-small' viewBox='0 0 10 6'>
              <DropIcon />
            </Icon>
          </MenuButton>
          <MenuList>
            {/* <Link to='/'> */}
            <MenuItem
              onClick={() => {
                revokeToken();
                logout();
              }}
            >
              <Image
                borderRadius='full'
                src={src}
                alt='Simon the pensive'
                boxSize='20px'
                mr={2}
              />
              <Text fontWeight='semibold'>Logout</Text>
            </MenuItem>
            {/* </Link> */}
          </MenuList>
        </Menu>
      </Text>
    </Box>
  );
};

UserItem.propTypes = {
  /**
   * ImageUrl is used to provide an image path
   */
  imageUrl: PropTypes.string,
  /**
   * FullName
   */
  fullName: PropTypes.string.isRequired,
};
