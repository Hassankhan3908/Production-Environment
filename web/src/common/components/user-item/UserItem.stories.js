import React from 'react';
import { UserItem } from './UserItem';

export default {
  title: 'Components/Avatar',
  component: UserItem,
};

export const AvatarExample = () => {
  return <UserItem imageUrl={src} fullName='Jason' />;
};
