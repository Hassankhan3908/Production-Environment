import React from 'react';
import { Thead, Th, Tr, Icon } from '@chakra-ui/react';
import { TheadArray } from './TheadArray';
import { DropIcon } from 'src/assets/icons';

export const IndividualOpportunityTHead = () => {
  return (
    <Thead>
      <Tr boxShadow='base'>
        {TheadArray.map((element, index) => {
          return (
            <Th py={3} textTransform='none' color='general.gray' key={index}>
              {`${element} `}
              {element === 'Due' ? (
                <Icon fontSize={18} position='relative' top={1.5}>
                  <DropIcon />
                </Icon>
              ) : (
                ''
              )}
            </Th>
          );
        })}
      </Tr>
      <Tr height={0.5} />
    </Thead>
  );
};
