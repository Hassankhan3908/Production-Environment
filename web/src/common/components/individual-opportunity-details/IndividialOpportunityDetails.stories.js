import React from 'react';
import { IndividualOpportunityDetails } from './IndividualOpportunityDetails';

export default {
  title: 'Components/IndividualOpportunityDetails',
  component: 'IndividualOpportunityDetails',
};

export const IndividualOpportunityDetailsExample = () => {
  return <IndividualOpportunityDetails />;
};
