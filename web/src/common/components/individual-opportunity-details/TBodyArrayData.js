export const TBodyArrayData = [
  {
    icontype: 'person',
    checked: true,
    task: 'Technical WhitePaper Download',
    sla: '',
    due: '06/13/20',
    status: {
      text: 'Completed',
      time: '06/13/2020',
    },
  },
  {
    icontype: 'report',
    checked: true,
    task: 'SDR Call Down',
    sla: 'Within 7 days of lead handover',
    due: '07/07/20',
    status: {
      text: 'Completed',
      time: '07/03/2020',
    },
  },
  {
    icontype: 'person',
    checked: true,
    task: 'Webinar',
    sla: '',
    due: '07/15/20',
    status: {
      text: 'Completed',
      time: '07/15/2020',
    },
  },
  {
    icontype: 'report',
    checked: true,
    task: 'First Meeting',
    sla: 'Within 4 weeks of lead creation',
    due: '07/23/20',
    status: {
      text: 'Completed',
      time: '07/25/2020',
    },
  },
  {
    icontype: 'person',
    checked: true,
    task: 'Customer Case Study Download',
    sla: 'Within 4 weeks of Opportunity Creation',
    due: '07/30/20',
    status: {
      text: 'Completed',
      time: '11/18/2020',
    },
  },
  {
    icontype: 'report',
    checked: true,
    task: 'POC',
    sla: '5 days',
    due: '',
    status: {
      text: 'Completed',
      time: '11/8/2020',
    },
  },
  {
    icontype: 'person',
    checked: false,
    task: 'Active Contacts Count',
    sla: '2.5',
    due: '12/11/20',
    status: {
      text: '1',
      time: 'Due in 13 days',
    },
  },
  {
    icontype: 'report',
    checked: false,
    task: 'EBC Visit',
    sla: 'Within 4 weeks of opportunity closing',
    due: '12/28/20',
    status: {
      text: 'Incomplete',
      time: 'Due in 23 days',
    },
  },
  {
    icontype: 'person',
    checked: false,
    task: 'CIO Dinner',
    sla: 'Within 4 weeks of opportunity closing',
    due: '12/28/20',
    status: {
      text: 'Incomplete',
    },
  },

  {
    icontype: 'report',
    checked: false,
    task: 'Contract Negotiation',
    sla: 'Within 4 weeks of opportunity closing',
    due: '12/28/20',
    status: {
      text: 'Incomplete',
    },
  },
];
