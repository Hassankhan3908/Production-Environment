import React from 'react';
import { Container, HStack, Spacer, Checkbox } from '@chakra-ui/react';
import { IndividualOpportunityTable } from './IndividualOpportunityTable';
import { DashedGap, ActivityIconComponent, HeaderContent } from '@salesdna/ui';
import { IndividualTaskStatusBox } from './IndividualTaskStatusBox';
export const IndividualOpportunityDetails = () => {
  return (
    <>
      <HStack justifyContent='flex-end'>
        <Container alignSelf='flex-start' width='80%' px='0'>
          <HeaderContent
            type='Good'
            days={203}
            numberType='short'
            amount={372899}
            containerBorderColor='container.green'
          />
        </Container>

        <IndividualTaskStatusBox />
        <IndividualTaskStatusBox />
      </HStack>
      <HStack mt={5}>
        <ActivityIconComponent iconType='activated' />
        <Spacer />
        <Checkbox
          mr={4}
          colorScheme='messenger'
          defaultChecked='true'
          color='general.gray'
          fontWeight='semibold'
          size='sm'
        >
          Show Completed tasks
        </Checkbox>
      </HStack>
      <DashedGap height={5} />
      <IndividualOpportunityTable />
      <ActivityIconComponent iconType='won' />
    </>
  );
};
