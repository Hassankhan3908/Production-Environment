import React from 'react';
import { Tr, Tbody, Td, Checkbox, Box } from '@chakra-ui/react';
import { SubActivityIconComponent, DashedGap } from '@salesdna/ui';
import { TBodyArrayData } from './TBodyArrayData';
export const IndividualOpportunityTBody = () => {
  return (
    <Tbody bgColor='badge.textColor'>
      <Tr>
        <Td p={0} border='none'>
          <DashedGap height={5} backgroundColor='body.color' marginL={4} />
        </Td>
      </Tr>
      {TBodyArrayData.map((element, index) => {
        return (
          <React.Fragment key={index}>
            <Tr>
              <Td px='0' pr={3} borderBottom='0' py={1} bgColor='body.color'>
                <SubActivityIconComponent iconType={element.icontype} />
              </Td>
              <Td
                fontSize={15}
                borderBottom='0'
                py={1}
                boxShadow='-4px 0px 5px rgb(0 0 0 / 3%)'
              >
                <Checkbox
                  colorScheme='messenger'
                  size='md'
                  defaultChecked={element.checked}
                >
                  <Box fontSize={15} fontWeight='medium'>
                    {element.task}
                  </Box>
                </Checkbox>
              </Td>
              <Td fontSize={15} borderBottom='0' py={1}>
                {element.sla}
              </Td>
              <Td fontSize={15} borderBottom='0' py={1}>
                {element.due}
              </Td>
              <Td
                fontSize={15}
                borderBottom='0'
                py={1}
                color={
                  element.status.text === 'Completed'
                    ? 'container.green'
                    : 'orange.300'
                }
              >
                {element.status.text}
                <Box ml={1} display='inline-block' color='general.gray'>
                  {element.status.time ? `(${element.status.time})` : ''}
                </Box>
              </Td>
            </Tr>
            <Tr>
              <Td fontSize={15} p={0} border='none'>
                <DashedGap
                  height={5}
                  backgroundColor='body.color'
                  marginL={4}
                />
              </Td>
            </Tr>
          </React.Fragment>
        );
      })}
    </Tbody>
  );
};
