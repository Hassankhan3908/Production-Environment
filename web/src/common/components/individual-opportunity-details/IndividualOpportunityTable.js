import React from 'react';
import { Table } from '@chakra-ui/react';
import { IndividualOpportunityTHead } from './IndividualOpportunityTHead';
import { IndividualOpportunityTBody } from './IndividualOpportunityTBody';
export const IndividualOpportunityTable = () => {
  return (
    <Table size='sm'>
      <IndividualOpportunityTHead />
      <IndividualOpportunityTBody />
    </Table>
  );
};
