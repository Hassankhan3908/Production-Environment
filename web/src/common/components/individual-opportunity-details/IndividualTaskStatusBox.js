import React from 'react';
import {
  Box,
  Container,
  Divider,
  HStack,
  VStack,
  Icon,
} from '@chakra-ui/react';
import { TitleWithIcon } from '@salesdna/ui';
import { PersonIcon, PersonReportIcon, LikeIcon } from 'src/assets/icons';
export const IndividualTaskStatusBox = ({
  completed = 7,
  total = 3,
  title = 'testing task',
  icon = 'person',
}) => {
  return (
    <Container>
      <VStack boxShadow='base' borderRadius='sm' bgColor='white' px={3} py={3}>
        <TitleWithIcon
          fontWeight='medium'
          fontSizeIcon={25}
          iconColor='general.gray'
          boxColor='general.gray'
          title={title}
          fontSizeBox={15}
          paddingY='0px'
        >
          <Icon>
            {icon === 'person' ? <PersonIcon /> : <PersonReportIcon />}
          </Icon>
        </TitleWithIcon>
        <Divider />
        <HStack alignSelf='normal' justifyContent='space-between'>
          <Box alignSelf='flex-start' fontWeight='semibold'>
            {completed} out of {total} Completed
          </Box>
          <Box alignSelf='flex-end' color='container.green'>
            <Icon
              position='relative'
              right={3}
              bottom={0.5}
              viewBox='0 0 12 12'
            >
              <LikeIcon />
            </Icon>
            0 late tasks
          </Box>
        </HStack>
      </VStack>
    </Container>
  );
};
