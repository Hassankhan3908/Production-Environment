import React from 'react';
import { Text, VStack, Divider, Box } from '@chakra-ui/react';
export const SubOpportunitySideBox = ({ title, children }) => {
  return (
    <VStack
      bgColor='badge.textColor'
      boxShadow='base'
      px={2.5}
      py={1}
      my={4}
      border='1px'
      borderColor='borderGrey'
      alignItems='flex-start'
      fontSize={13}
      fontWeight='medium'
    >
      <Text py={1} color='general.gray'>
        {title}
      </Text>
      <Divider borderBottomWidth={2} borderBottomColor='gray.300' />
      <Box py={1}>{children}</Box>
    </VStack>
  );
};
