import React from 'react';
import { Text, Box } from '@chakra-ui/react';
export const SubOpportunityDate = ({ date, days }) => {
  return (
    <>
      <Text fontSize={13}>{date}</Text>
      <Box py={1} fontSize={13}>
        {`${days} `}
        <Text color='general.gray' fontWeight='normal' display='inline-block'>
          days away
        </Text>
      </Box>
    </>
  );
};
