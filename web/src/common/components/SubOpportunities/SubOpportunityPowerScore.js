import React from 'react';
import { Text, Box } from '@chakra-ui/react';
export const SubOpportunityPowerScore = ({ text, powerScore }) => {
  return (
    <>
      <Text fontSize={13}>{text}</Text>
      <Box color='general.gray' py={1} fontWeight='normal' fontSize={13}>
        {`Power Score: `}
        <Text display='inline-block' fontWeight='medium' color='black'>
          {powerScore}x
        </Text>
      </Box>
    </>
  );
};
