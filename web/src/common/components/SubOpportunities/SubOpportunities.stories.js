import React from 'react';
import { Container } from '@chakra-ui/layout';
import { SubOpportunityDate } from './SubOpportunityDate';
import { SubOpportunityPowerScore } from './SubOpportunityPowerScore';
import { SubOpportunitySideBox } from './SubOpportunitySideBox';
export default {
  title: 'Components/SubOpportunities',
  component: SubOpportunitySideBox,
};

export const SubOpportunitySideBoxExample = () => {
  return (
    <Container width='20%'>
      <SubOpportunitySideBox title="SalesDNA's est. Closing Date">
        <SubOpportunityDate date='Jan 20, 2021' days={84} />
      </SubOpportunitySideBox>
    </Container>
  );
};
export const SubOpportunitySideBoxByIndustryExample = () => {
  return (
    <Container width='20%'>
      <SubOpportunitySideBox title="SalesDNA's est. Closing Date">
        <SubOpportunityPowerScore text='Competitor' powerScore={3.5} />
      </SubOpportunitySideBox>
    </Container>
  );
};
