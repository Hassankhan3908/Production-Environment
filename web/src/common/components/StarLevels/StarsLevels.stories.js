import React from 'react';

import { StarLevels } from './StarLevels';
export default {
  title: 'Components/StarLevels',
  component: StarLevels,
};

export const StarsBoxExample = () => {
  return <StarLevels score={1} />;
};
