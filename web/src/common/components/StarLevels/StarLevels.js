import React from 'react';
import { Icon } from '@chakra-ui/react';
import { StarIcon } from 'src/assets/icons';
export const StarLevels = ({ score }) => {
  return score === 3 ? (
    <>
      <Icon viewBox='0 0 14 13' fontSize='xl'>
        <StarIcon />
      </Icon>
      <Icon viewBox='0 0 14 13' fontSize='xl' mx={2}>
        <StarIcon />
      </Icon>
      <Icon viewBox='0 0 14 13' fontSize='xl'>
        <StarIcon />
      </Icon>
    </>
  ) : score === 2 ? (
    <>
      <Icon viewBox='0 0 14 13' fontSize='xl'>
        <StarIcon />
      </Icon>
      <Icon viewBox='0 0 14 13' fontSize='xl' mx={2}>
        <StarIcon />
      </Icon>
    </>
  ) : (
    <Icon viewBox='0 0 14 13' fontSize='xl'>
      <StarIcon />
    </Icon>
  );
};
