import React from 'react';
import { BreadcrumbComponent } from './BreadcrumbComponent';
export default {
  title: 'Components/BreadcrumbComponent',
  component: BreadcrumbComponent,
};

export const BreadcrumbExample = () => {
  return (
    <BreadcrumbComponent accountName='Bank of America' systemAmount={450232} />
  );
};
