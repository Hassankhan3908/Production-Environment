import React from 'react';
import { Breadcrumb, BreadcrumbItem, Text } from '@chakra-ui/react';
import { CurrencyText } from '../general/number-text/CurrencyText';

export const BreadcrumbComponent = ({ children, childText, amount }) => {
  return (
    <Breadcrumb spacing={2} separator='>' fontSize={13} fontWeight='medium'>
      <BreadcrumbItem color='general.blue'>{children}</BreadcrumbItem>
      <BreadcrumbItem>
        <Text color='general.gray'>
          {childText}
          {amount && (
            <>
              {` for`} <CurrencyText numberType='long' value={amount} />
            </>
          )}
        </Text>
      </BreadcrumbItem>
    </Breadcrumb>
  );
};
