import React from 'react';
import { ForecastSummarySidebar } from './ForecastSummarySidebar';
export default {
  title: 'Components/ForecastSummarySidebar',
  component: ForecastSummarySidebar,
};

export const ForecastSummarySidebarExample = () => {
  return <ForecastSummarySidebar />;
};
