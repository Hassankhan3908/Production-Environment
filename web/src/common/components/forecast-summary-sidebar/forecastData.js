import { FORECASTTYPES } from '../design-components/property-value-stack/forecastTypes';
export const forecastData = [
  { title: FORECASTTYPES.pipeline, value: 161914453 },
  { title: FORECASTTYPES.commit, value: 41097758 },
  { title: FORECASTTYPES.upside, value: 3765345 },
  { title: FORECASTTYPES.bestCase, value: 1234562 },
  { title: FORECASTTYPES.target, value: 42097758 },
  {
    title: FORECASTTYPES.predicted,
    value: 42097758,
    percentage: 93,
    type: '',
  },
];
