import React from 'react';
import { Container, Box, Heading } from '@chakra-ui/react';
import { PropertyValueStack, CurrencyText } from '@salesdna/ui';

export const ForecastSummarySidebar = ({ data }) => {
  return data ? (
    <Container
      bgColor='forecastSummary.color'
      textColor='white'
      margin={0}
      py='72px'
      px={10}
      width={80}
    >
      <Box p={15}>
        <Heading size='md' fontWeight='semibold'>
          {data.title}
        </Heading>
        <Box my={13} mb='30px' fontSize={16} fontWeight='bold'>
          {<CurrencyText numberType='short' value={data.amount} />}
          {data.days && ` + ${data.days} days`}
        </Box>
        {data.data.map((element, index) => {
          const { key, value } = element;
          return key === 'predicted' && value ? (
            <PropertyValueStack
              border='none'
              bShadow='none'
              key={index}
              title='Predicted'
              percentage={value}
              value={value}
            />
          ) : (
            <PropertyValueStack
              border='none'
              key={index}
              bShadow='none'
              title={element.key}
              value={element.value}
            />
          );
        })}
      </Box>
    </Container>
  ) : null;
};
