import React from 'react';
import { Table } from '@chakra-ui/react';
import { TableHead } from './TableHead';

export default {
  title: 'Components/TableHead',
  component: TableHead,
};

export const TableExample = () => {
  return (
    <Table>
      <TableHead />
    </Table>
  );
};
