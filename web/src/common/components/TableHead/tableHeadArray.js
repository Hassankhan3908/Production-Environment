export const tableHeadArray = [
  {
    argu: ['accountName'],
    text: 'Account Name',
    type: 'string',
  },
  {
    argu: ['accountOwner'],
    text: 'Account Owner',
    type: 'string',
  },
  {
    argu: ['tasks', true],
    text: 'Tasks',
    type: 'number',
  },

  {
    argu: ['projectedAmount'],
    text: 'Projected Amt',
    type: 'number',
  },
  {
    argu: ['systemAmount'],
    text: 'System Amt',
    type: 'number',
  },
  {
    argu: ['projectedCloseDate'],
    text: 'Projected Close by Date',
    type: 'string',
  },
  {
    argu: ['systemCloseDate'],
    text: 'System Close by Date',
    type: 'string',
  },
  {
    argu: ['projectOutCome'],
    text: 'Project Outcome',
    type: 'string',
  },
  {
    argu: ['predictedScore'],
    text: 'Predicted Score',
    type: 'number',
  },
];
