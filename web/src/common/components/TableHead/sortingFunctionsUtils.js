export const stringSorting = (data, key) => {
  return [...data].sort((a, b) =>
    a[key].toLowerCase() < b[key].toLowerCase()
      ? -1
      : a[key].toLowerCase() > b[key].toLowerCase()
      ? 1
      : 0
  );
};
export const numberSorting = (data, key, nested = false) => {
  if (!nested) {
    return [...data].sort((a, b) =>
      a[key] > b[key] ? -1 : a[key] < b[key] ? 1 : 0
    );
  } else {
    return [...data].sort((a, b) =>
      a[key]['completed'] > b[key]['completed']
        ? -1
        : a[key]['completed'] < b[key]['completed']
        ? 1
        : 0
    );
  }
};
