import React from 'react';
import { Thead, Tr, Th, Button, Icon } from '@chakra-ui/react';
import { DropIcon } from 'src/assets/icons';
import { tableHeadArray } from './tableHeadArray';
import { stringSorting, numberSorting } from './sortingFunctionsUtils';
export const TableHead = ({ setState = () => {}, data = [] }) => {
  return (
    <Thead>
      <Tr>
        {tableHeadArray.map(({ argu, text, type }, index) => {
          return (
            <Th key={index} pl={4} pr={0} fontSize={10} color='general.gray'>
              <Button
                fontSize={13}
                variant='unstyled'
                onClick={() =>
                  type === 'number'
                    ? setState(numberSorting(data, ...argu))
                    : setState(stringSorting(data, ...argu))
                }
              >
                {text}
                <Icon position='relative' top={2} left={2} fontSize={23}>
                  <DropIcon />
                </Icon>
              </Button>
            </Th>
          );
        })}
      </Tr>
    </Thead>
  );
};
