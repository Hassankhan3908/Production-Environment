import React from 'react';
import { useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Box, LinkBox } from '@chakra-ui/react';
import { TitleWithIcon } from '../../design-components/title-with-icon/TitleWithIcon';

/**
 * MenuItem is used to return an Item consists of Icon and Title as a Clickable Link
 *
 * Inputs: id, isActive, addStyling, text, iconType, url
 */
export const activeStyling = {
  border: '2px',
  borderColor: 'PersonIcons.border',
  borderRadius: 'md',
  bgColor: 'menuItem.bgColor',
};
export const defaultStyling = {
  borderColor: 'badge.textColor',
  bgColor: 'badge.textColor',
  borderLeftColor: 'badge.textColor',
  color: 'gray.500',
};

export const MenuItem = ({ id, text, icon, url, DisplayAs }) => {
  const location = useLocation().pathname;
  const isActive = location.includes(url);
  return (
    <LinkBox
      key={id}
      px={3}
      borderLeftColor={isActive ? 'general.blue' : 'badge.textColor'}
      borderLeftWidth={4}
    >
      <DisplayAs to={url}>
        <Box {...(isActive ? activeStyling : defaultStyling)} pl={4}>
          <TitleWithIcon
            marginRight={3}
            title={text}
            fontSizeIcon={17}
            fontSizeBox={12}
            fontWeight={isActive ? 'bold' : 'medium'}
            iconColor={isActive ? 'general.blue' : 'general.gray'}
          >
            {icon}
          </TitleWithIcon>
        </Box>
      </DisplayAs>
    </LinkBox>
  );
};

MenuItem.propTypes = {
  /**
   * Id is used to identify items uniquely
   */
  id: PropTypes.number,
  /**
   * IsActive is bool
   */
  isActive: PropTypes.bool,
  /**
   * AddStyling is object to be used as style
   */
  addStyling: PropTypes.func,
  /**
   * Text
   */
  text: PropTypes.string,
  /**
   * IconType
   */
  icon: PropTypes.object,
  /**
   * URL
   */
  url: PropTypes.string,
};
