import React from 'react';
import { RightSidebar } from './RightSidebar';
import { SampleData } from './SampleData';
export default {
  title: 'layout/RightSidebar',
  component: RightSidebar,
};

export const RightSidebarExample = () => {
  return <RightSidebar menuConfig={SampleData} />;
};
