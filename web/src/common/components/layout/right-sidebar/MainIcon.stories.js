import React from 'react';
import { MainIconComponent } from './MainIconComponent';

export default {
  title: 'layout/MainIconComponent',
  component: MainIconComponent,
};

export const MainIconExample = () => {
  return <MainIconComponent />;
};
