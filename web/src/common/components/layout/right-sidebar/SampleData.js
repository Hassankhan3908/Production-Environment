import { IconType } from './IconType';
import { Box } from '@chakra-ui/react';
export const SampleData = [
  {
    id: 1,
    isActive: true,
    text: 'Your DNA',
    url: '/',
    DisplayAs: Box,
    iconType: IconType.DNA,
  },
  {
    id: 2,
    isActive: false,
    text: 'Forecast',
    url: '/forecast',
    DisplayAs: Box,
    iconType: IconType.FORECAST,
  },
  {
    id: 3,
    isActive: false,
    text: 'Opportunities',
    url: '/opportunities',
    DisplayAs: Box,
    iconType: IconType.OPPURTUNITY,
  },
];
