import React from 'react';
import { MenuItem } from './MenuItem';
/**
 * The MenuItem component for the right sidebar
 */

export const MenuItemContainer = ({ data }) => {
  return (
    <>
      {data.map(({ isActive, id, text, icon, url, DisplayAs }) => (
        <MenuItem
          key={id}
          url={url}
          isActive={isActive}
          id={id}
          text={text}
          icon={icon}
          DisplayAs={DisplayAs}
        />
      ))}
    </>
  );
};
