import React from 'react';
import { HStack, Stack } from '@chakra-ui/react';

import { MainIconComponent } from './MainIconComponent';
import { MenuItemContainer } from './MenuItemContainer';
/**
 * The RightSidebar component for salesDNA Dashboard
 */
export const RightSidebar = ({ menuConfig }) => {
  return (
    <Stack
      minW='206'
      border='1px'
      minHeight='100vh'
      width={52}
      bgColor='badge.textColor'
      borderColor='borderGreyOpac'
    >
      <HStack justify='center' py={5}>
        <MainIconComponent />
      </HStack>
      <MenuItemContainer data={menuConfig} />
    </Stack>
  );
};
