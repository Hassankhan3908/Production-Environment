import React from 'react'
import { Icon, Box } from '@chakra-ui/react'
import { MainIcon } from 'src/assets/icons'
/**
 * MainIconComponent is used to render MainIcon
 */
export const MainIconComponent = () => {
  return (
    <Box fontSize={20}>
      <Icon fontSize="xx-large" pr="2">
        <MainIcon />
      </Icon>
      sales
      <Box display="inline-block" fontWeight="bold">
        DNA
      </Box>
    </Box>
  )
}
