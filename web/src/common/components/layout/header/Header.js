import React from 'react'
import { Flex, HStack } from '@chakra-ui/react'
import { Search } from '../../search/Search'
import { UserItem } from '../../user-item/UserItem'
import { Avatars } from './Avatars'
import { LinkBoxComponent } from '../../design-components/linkbox-component/LinkBoxComponent'
import { revoke } from '@salesdna/services'
/**
 * The header component for salesDNA Dashboard
 */

export const Header = ({ id, name, logout, userId }) => {
  return (
    <Flex
      justify="space-between"
      bgColor="badge.textColor"
      width="100%"
      py={3.5}
      px={8}
    >
      <Flex alignItems="baseline">
        <Search />
        <LinkBoxComponent text="What can I search ?" />
      </Flex>
      <HStack justifyContent="space-between">
        <UserItem
          imageUrl={Avatars[id]}
          fullName={name}
          logout={logout}
          userId={userId}
          revokeToken={revoke}
        />
      </HStack>
    </Flex>
  )
}
