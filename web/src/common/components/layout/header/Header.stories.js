import React from 'react';
import { Header } from './Header';

export default {
  title: 'layout/Header',
  component: Header,
};

export const HeaderExample = () => {
  return <Header />;
};
