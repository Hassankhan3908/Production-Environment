import React from 'react';
import {
  Input,
  Icon,
  InputGroup,
  InputLeftElement,
  Box,
} from '@chakra-ui/react';
import { SearchIcon } from 'src/assets/icons';
import { LinkBoxComponent } from '../design-components/linkbox-component/LinkBoxComponent';
/**
 * The search component for salesDNA Dashboard
 */
export const Search = () => {
  return (
    <Box width={96} pr={2} fontSize='small'>
      <InputGroup size='sm'>
        <InputLeftElement
          children={
            <Icon fontSize={25} position='relative' left={1} top={1.5}>
              <SearchIcon />
            </Icon>
          }
        />
        <Input
          bgColor='searchBgColor'
          fontSize='smaller'
          pl={8}
          borderRadius='sm'
          placeholder='Search ...'
        />
      </InputGroup>

      <LinkBoxComponent />
    </Box>
  );
};
