import React from 'react';
import { TitleIconChart } from './TitleIconChart';

export default {
  title: 'Dashboard/TitleIconChart',
  component: TitleIconChart,
};

export const TitleIconChartPageExample = () => {
  return <TitleIconChart />;
};
