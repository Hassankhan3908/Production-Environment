import React from 'react';
import {
  VStack,
  HStack,
  Icon,
  Heading,
  Flex,
  Box,
  Divider,
  UnorderedList,
  ListItem,
  Text,
  Center,
} from '@chakra-ui/react';
import { Line } from 'react-chartjs-2';
import { PerformanceStatus } from '@salesdna/ui';

export const TitleIconChart = ({ title, icon, dataSet, options }) => {
  return (
    <VStack alignItems='flex-start' my={8} width='100%' px={10}>
      <HStack>
        <Icon fontSize='x-large' color='general.blue'>
          {icon}
        </Icon>
        <Heading size='sm'>{title}</Heading>
      </HStack>
      <Flex bgColor='white' w='100%' px={7} py={10}>
        <VStack alignItems='flex-start' width='300px'>
          <Heading size='sm'>What does this mean ?</Heading>
          <Divider />
          <PerformanceStatus
            text={
              performance === 1
                ? 'Performing Strong! 🎉'
                : performance === 2
                ? 'Doing Okay 👍'
                : 'Needs Work 🤔'
            }
          />
          <Text fontSize='sm' fontWeight='normal' color='general.gray'>
            Consistently above-average opportunity amounts compared to peer
            group and company benchmark.
          </Text>
          <UnorderedList pl={5}>
            <ListItem color='general.blue'>
              <Text color='black'>Your Average</Text>
            </ListItem>
            <ListItem color='container.purple'>
              <Text color='black'>Company Average</Text>
            </ListItem>
            <ListItem color='container.green'>
              <Text color='black'>Company Benchmark</Text>
            </ListItem>
          </UnorderedList>
        </VStack>
        <Center height='200' px={4}>
          <Divider borderLeftWidth={2} orientation='vertical' />
        </Center>
        <Box flexGrow='1'>
          <Line data={dataSet} options={options} height={100} />
        </Box>
      </Flex>
    </VStack>
  );
};
