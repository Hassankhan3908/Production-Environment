import src1 from './1.PNG';
import src2 from './2.PNG';
import src3 from './3.PNG';
import src4 from './4.PNG';
import src5 from './5.PNG';
import src6 from './6.PNG';
import src7 from './7.PNG';
import src8 from './8.PNG';
export const Pngs = {
  1: src1,
  2: src2,
  3: src3,
  4: src4,
  5: src5,
  6: src6,
  7: src7,
  8: src8,
};
