import React from 'react';
import { Paginator } from './Paginator';

export default {
  title: 'Components/Paginator',
  component: Paginator,
};

export const PaginatorExampleWith50Records = () => {
  return <Paginator total={50} />;
};
export const PaginatorExampleWith30Records = () => {
  return <Paginator total={30} />;
};
export const PaginatorExample10Records = () => {
  return <Paginator total={10} />;
};
