import React from 'react'
import { PropTypes } from 'prop-types'

import { Button, HStack } from '@chakra-ui/react'
/**
 * Pagination component for list of records.
 * Input: maxPages, currentPage, onPageChange, isDisabled
 */

export const Paginator = ({ page, totalPages = 5, size = 10, setPage }) => {
  let pageCount = []

  pageCount = [...Array(totalPages).keys()]
  return totalPages > 1 ? (
    <HStack>
      <Button
        isDisabled={page === 0 ? true : false}
        onClick={() => {
          if (page > 0) {
            setPage(page - 1)
          }
        }}
      >
        {'<  Prev'}
      </Button>
      {totalPages <= 10 &&
        pageCount.map((index) => {
          return (
            <Button
              {...(page === index
                ? { bgColor: 'paginator.bgColor', textColor: 'paginator.color' }
                : '')}
              key={index}
              onClick={() => setPage(index)}
            >
              {index + 1}
            </Button>
          )
        })}
      {page >= 0 && page < 4 && totalPages > 10 && (
        <>
          {pageCount.slice(0, 5).map((el, index) => (
            <Button
              {...(page === index
                ? { bgColor: 'paginator.bgColor', textColor: 'paginator.color' }
                : '')}
              key={index}
              onClick={() => setPage(index)}
            >
              {index + 1}
            </Button>
          ))}
          ...
          {
            <Button
              {...(page === pageCount.length - 1
                ? { bgColor: 'paginator.bgColor', textColor: 'paginator.color' }
                : '')}
              key={pageCount.length - 1}
              onClick={() => setPage(pageCount.length - 1)}
            >
              {pageCount.length - 1 + 1}
            </Button>
          }
        </>
      )}
      {page >= 4 && page <= pageCount.length - 6 && totalPages > 10 && (
        <>
          {
            <Button
              {...(page === pageCount[0]
                ? { bgColor: 'paginator.bgColor', textColor: 'paginator.color' }
                : '')}
              key={0}
              onClick={() => setPage(0)}
            >
              {1}
            </Button>
          }
          ...
          {pageCount.slice(page - 2, page + 3).map((index) => (
            <Button
              {...(page === index
                ? { bgColor: 'paginator.bgColor', textColor: 'paginator.color' }
                : '')}
              key={index}
              onClick={() => setPage(index)}
            >
              {index + 1}
            </Button>
          ))}
          ...
          {
            <Button
              {...(page === pageCount.length - 1
                ? { bgColor: 'paginator.bgColor', textColor: 'paginator.color' }
                : '')}
              key={pageCount.length - 1}
              onClick={() => setPage(pageCount.length - 1)}
            >
              {pageCount.length - 1 + 1}
            </Button>
          }
        </>
      )}
      {page < pageCount.length &&
        page > pageCount.length - 6 &&
        totalPages > 10 && (
          <>
            {
              <Button
                {...(page === pageCount[0]
                  ? {
                      bgColor: 'paginator.bgColor',
                      textColor: 'paginator.color',
                    }
                  : '')}
                key={0}
                onClick={() => setPage(0)}
              >
                {1}
              </Button>
            }
            ...
            {pageCount
              .slice(pageCount.length - 6, pageCount.length)
              .map((index) => (
                <Button
                  {...(page === index
                    ? {
                        bgColor: 'paginator.bgColor',
                        textColor: 'paginator.color',
                      }
                    : '')}
                  key={index}
                  onClick={() => setPage(index)}
                >
                  {index + 1}
                </Button>
              ))}
          </>
        )}
      <Button
        isDisabled={page === totalPages - 1 ? true : false}
        onClick={() => setPage(page + 1)}
      >
        {`Next >`}
      </Button>
    </HStack>
  ) : (
    ''
  )
}

Paginator.propTypes = {
  /**
   * Currently selected page.
   */
  page: PropTypes.number,
  /**
   * Total number of pages.
   */
  total: PropTypes.number,
  /**
   * The component will emit an event when the page will be changed.
   */
  setPage: PropTypes.func.isRequired,
  /**
   * size of the page
   */
  size: PropTypes.number,
}

Paginator.defaultProps = {
  page: 1,
  setPage: () => {},
  size: 10,
}
