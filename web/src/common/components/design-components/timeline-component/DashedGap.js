import React from 'react';
import { Stack, Divider } from '@chakra-ui/react';

/**
 * DashedGap Component is used to render dashed vertical line Gap
 */
export const DashedGap = ({ height = [50], marginL = '4' }) => {
  return (
    <Stack h={height}>
      <Divider
        ml={marginL}
        orientation='vertical'
        borderLeftWidth={2.5}
        variant='dashed'
        borderLeftColor='dashedGrayOpac'
      />
    </Stack>
  );
};
