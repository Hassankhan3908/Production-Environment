import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Box } from '@chakra-ui/react'
import {
  AccountActivated,
  OppurtunityWon,
  OppurtunityIcon,
} from 'src/assets/icons'
import { ActivityTypes } from './ActivityTypes'

/**
 * ActivityIconComponent is used to return <Icon /> of either type(OppurtunityWon, OppurtunityIcon, AccountActivated) based on iconType Prop
 *
 * Input: iconType
 */

export const ActivityIconComponent = ({ iconType }) => {
  return (
    <Box
      borderRadius="sm"
      display="inline-block"
      bgColor={`activityColors.${iconType}`}
    >
      <Icon
        viewBox={iconType === `won` ? '-2 -2 14 24' : '1 -2 14 24'}
        fontSize="x-large"
        m={1.5}
        color={iconType === ActivityTypes.CREATED && 'white'}
      >
        {iconType === ActivityTypes.WON ? (
          <OppurtunityWon />
        ) : iconType === ActivityTypes.CREATED ? (
          <OppurtunityIcon />
        ) : iconType === ActivityTypes.ACTIVATED ? (
          <AccountActivated />
        ) : (
          ''
        )}
      </Icon>
    </Box>
  )
}
ActivityIconComponent.propTypes = {
  /**
   * IconType is the string which differentiate the return type of Icon
   */
  iconType: PropTypes.string,
}
