import React from 'react'
import { Icon, Box } from '@chakra-ui/react'
import { DotIcon } from 'src/assets/icons'

/**
 * NoActivityIcon returns a Dot Icon if required value is absent in data
 */
export const NoActivityIcon = () => {
  return (
    <Box minH={9}>
      <Icon ml={3} position="relative" top={1.5}>
        <DotIcon />
      </Icon>
    </Box>
  )
}
