export const ActivityTypes = {
  ACTIVATED: 'activated',
  CREATED: 'created',
  WON: 'won',
};
