import React from 'react'
import { Activity } from './Activity'
import { ActivityIconComponent } from './ActivityIconComponent'
import { RenderSubActivities } from './RenderSubActivities'
import { DashedGap } from './DashedGap'

export const RenderActivities = ({ data }) => {
  return (
    <React.Fragment>
      <Activity text={'Account Activated'}>
        <ActivityIconComponent iconType={'activated'} />
      </Activity>
      <DashedGap height={30} />
      <RenderSubActivities
        data={data
          .slice(0, 3)
          .sort((a, b) => (a.order > b.order ? 1 : a.order < b.order ? -1 : 1))}
      />
      <Activity text={'Opportunity Created'}>
        <ActivityIconComponent iconType={'created'} />
      </Activity>
      <DashedGap height={30} />
      <RenderSubActivities
        data={data
          .slice(3)
          .sort((a, b) => (a.order > b.order ? 1 : a.order < b.order ? -1 : 1))}
      />
      <Activity text={'Opportunity Won!'}>
        <ActivityIconComponent iconType={'won'} />
      </Activity>
    </React.Fragment>
  )
}
