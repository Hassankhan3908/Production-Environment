import React from 'react'
import PropTypes from 'prop-types'
import { SubActivity } from './SubActivity'
import { SubActivityIconComponent } from './SubActivityIconComponent'
import { DashedGap } from './DashedGap'
import { NoActivityIcon } from './NoActivityIcon'

/**
 * RenderSubActivies is used to render a list of component based on array as prop
 */

export const RenderSubActivities = ({ data }) => {
  return data.map(({ type = 'person', metric_value, display_name }, index) =>
    type && metric_value !== 'undefined' && name !== 'undefined' ? (
      <React.Fragment key={index}>
        <SubActivity
          key={Math.random().toString()}
          value={metric_value}
          text={display_name}
        >
          <SubActivityIconComponent iconType={type} />
        </SubActivity>
        <DashedGap height={30} />
      </React.Fragment>
    ) : (
      <React.Fragment key={index}>
        <NoActivityIcon />
        <DashedGap height={30} />
      </React.Fragment>
    )
  )
}
RenderSubActivities.propTypes = {
  /**
   * Data
   */
  data: PropTypes.array,
}
