import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Box } from '@chakra-ui/react'
import {
  PersonIcon,
  PersonReportIcon,
  PersonScreenIcon,
} from 'src/assets/icons'
import { SubActivityTypes } from './SubActivityTypes'

/**
 * SubActivityIconComponent is used to return either type Icon(PersonIcon, PersonReportIcon, PersonScreenIcon)
 *
 * Inputs: iconTypes
 */
export const SubActivityIconComponent = ({ iconType }) => {
  return (
    <Box
      border="2px"
      borderRadius="sm"
      borderColor="general.blueBorderColor"
      bgColor="badge.textColor"
      display="inline-block"
    >
      <Icon color="general.blue" viewBox="0 -4 14 24" fontSize="x-large" m={1}>
        {iconType === SubActivityTypes.PERSON ? (
          <PersonIcon />
        ) : iconType === SubActivityTypes.REPORT ? (
          <PersonReportIcon />
        ) : iconType === SubActivityTypes.SCREEN ? (
          <PersonScreenIcon />
        ) : (
          ''
        )}
      </Icon>
    </Box>
  )
}
SubActivityIconComponent.propTypes = {
  /**
   * IconType is the string which differentiate the return type of Icon
   */
  iconType: PropTypes.string,
}
