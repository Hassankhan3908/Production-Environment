import React from 'react'
import PropTypes from 'prop-types'
import { Box } from '@chakra-ui/react'
import { RenderActivities } from './RenderActivities'
/**
 * TimeLine Component is used to render a sequence of Activities and their subActivites based on data prop
 *
 * Input: data
 */

export const TimelineComponent = ({ data }) => {
  return (
    <Box py={30}>
      <RenderActivities
        data={data
          .slice()
          .sort((a, b) => (a.phase > b.phase ? 1 : a.phase < b.phase ? -1 : 1))}
      />
    </Box>
  )
}

TimelineComponent.propTypes = {
  /**
   * Data
   */
  data: PropTypes.array,
}
