import React from 'react';
import { Container } from '@chakra-ui/layout';
import {
  timelineActivites,
  timelineActivitesBad,
  timelineActivitesOutlier,
} from './SampleData';
import { TimelineComponent } from './TimelineComponent';
export default {
  title: 'DesignComponents/TimelineComponent',
  component: TimelineComponent,
};

export const TaskStatusGood = () => {
  return (
    <Container>
      <TimelineComponent data={timelineActivites} />;
    </Container>
  );
};
export const TaskStatusBad = () => {
  return (
    <Container>
      <TimelineComponent data={timelineActivitesBad} />;
    </Container>
  );
};
export const TaskStatusOutlier = () => {
  return (
    <Container>
      <TimelineComponent data={timelineActivitesOutlier} />;
    </Container>
  );
};
