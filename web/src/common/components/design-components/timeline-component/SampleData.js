import { ActivityTypes } from './ActivityTypes';
const dataG = [
  { type: 'person', value: '0.4', text: 'Marketing Touches Before' },
  { type: 'report', value: '5.6', text: 'Sales Touches Before' },
  { type: 'person', value: '13-day', text: ' Marketing Nurture' },
];
const dataG2 = [
  { type: 'screen', value: '18-day', text: 'Proof of Concept' },
  { type: 'person', value: '2.3', text: 'Marketing Touches After' },
  { type: 'person', value: '2.6', text: 'Engaged Contacts' },
  { type: 'report', value: '2.2', text: 'Sales Touches After' },
  { type: 'report', value: '191-day', text: 'Sales Nurture' },
];
const dataB = [
  {},
  { type: 'report', value: '11.7', text: 'Sales Touches Before' },
  { type: 'person', value: '128-day', text: ' Marketing Nurture' },
];
const dataB2 = [
  { type: 'screen', value: '18-day', text: 'Proof of Concept' },
  {},
  { type: 'report', value: '1.3', text: 'Engaged Contacts' },
  { type: 'report', value: '2.8', text: 'Sales Touches After' },
  { type: 'report', value: '235-day', text: 'Sales Nurture' },
];
const dataO = [{}, {}, {}];
const dataO2 = [
  { type: 'screen', value: '18-day', text: 'Proof of Concept' },
  {},
  { type: 'report', value: '1.3', text: 'Engaged Contacts' },
  { type: 'report', value: '2.8', text: 'Sales Touches After' },
  { type: 'report', value: '235-day', text: 'Sales Nurture' },
];

export const timelineActivites = [
  {
    type: ActivityTypes.ACTIVATED,
    text: 'Account Activated',
    subActivities: [...dataG],
  },
  {
    type: ActivityTypes.CREATED,
    text: 'Oppurtunity Created',
    subActivities: [...dataG2],
  },
  { type: ActivityTypes.WON, text: 'Oppurtunity Won!', subActivities: [] },
];
export const timelineActivitesBad = [
  {
    type: ActivityTypes.ACTIVATED,
    text: 'Account Activated',
    subActivities: [...dataB],
  },
  {
    type: ActivityTypes.CREATED,
    text: 'Oppurtunity Created',
    subActivities: [...dataB2],
  },
  { type: ActivityTypes.WON, text: 'Oppurtunity Won!', subActivities: [] },
];
export const timelineActivitesOutlier = [
  {
    type: ActivityTypes.ACTIVATED,
    text: 'Account Activated',
    subActivities: [...dataO],
  },
  {
    type: ActivityTypes.CREATED,
    text: 'Oppurtunity Created',
    subActivities: [...dataO2],
  },
  { type: ActivityTypes.WON, text: 'Oppurtunity Won!', subActivities: [] },
];
