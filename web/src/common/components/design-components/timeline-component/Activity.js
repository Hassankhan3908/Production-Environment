import React from 'react';
import PropTypes from 'prop-types';
import { Box, Text } from '@chakra-ui/react';

/**
 * Activity Component is used render major Activities, renders Icon side by side of Text based on text prop and children(<icon />)
 *
 * Inputs: text
 */
export const Activity = ({ text, children }) => {
  return (
    <Box>
      {children}
      <Text
        pl='4'
        position='relative'
        top={1}
        fontSize='medium'
        fontWeight='bold'
        display='inline-block'
      >
        {text}
      </Text>
    </Box>
  );
};

Activity.propTypes = {
  /**
   * Text
   */
  text: PropTypes.string,
};
