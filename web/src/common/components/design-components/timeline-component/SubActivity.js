import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@chakra-ui/react';

/**
 * SubActivity is used to render a sub activity a <icon /> related value and text based on provided props
 *
 * Inputs: value,text,children
 */
export const SubActivity = ({ value, text, children }) => {
  return (
    <Box color='general.gray' fontSize='sm'>
      {children}
      <Box pl='4' fontWeight='semibold' pr={2} display='inline-block'>
        {value}
      </Box>
      {text}
    </Box>
  );
};
SubActivity.propTypes = {
  /**
   * Text name of activity and it's purpose
   */
  text: PropTypes.string,
  /**
   * Value represents value and/or day
   */
  value: PropTypes.string,
};
