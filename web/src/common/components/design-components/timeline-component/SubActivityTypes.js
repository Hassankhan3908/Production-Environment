export const SubActivityTypes = {
  PERSON: 'person',
  REPORT: 'report',
  SCREEN: 'screen',
};
