import React from 'react';
import { WinLossFlag } from './WinLossFlag';

export default {
  title: 'DesignComponents/WinLossFlag',
  component: WinLossFlag,
};

export const WinFlagExample = () => {
  return <WinLossFlag type='win' />;
};

export const LossFlagExample = () => {
  return <WinLossFlag type='loss' />;
};
