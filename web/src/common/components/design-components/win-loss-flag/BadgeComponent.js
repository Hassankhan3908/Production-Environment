import React from 'react';
import PropTypes from 'prop-types';
import { Badge, Icon } from '@chakra-ui/react';
import { LossFlagIcon, DollarIcon } from 'src/assets/icons';

/**
 * BadgeComponent returns the badge based on required type prop either loss badge, or dollar badge
 * Inputs:type
 */
export const BadgeComponent = ({ type }) => {
  return (
    <Badge
      bg={type === 'win' ? 'container.green' : 'container.red'}
      textTransform='none'
      width={12}
      color='badge.textColor'
      pr={16}
      pl={0}
      fontSize={13}
      lineHeight={6}
      fontWeight='medium'
      height={7}
    >
      <Icon
        fontSize={28}
        position='relative'
        top={type === 'win' ? 1.5 : 2.5}
        left={2.5}
        width='auto'
      >
        {type === 'win' ? <DollarIcon /> : <LossFlagIcon />}
      </Icon>
      Loss
    </Badge>
  );
};
BadgeComponent.propTypes = {
  /**
   * Type
   */
  type: PropTypes.string,
};
