import React from 'react';
import { PropTypes } from 'prop-types';
import { BadgeComponent } from './BadgeComponent';
/**
 * WinLossFlag is the component which returns a BadgeComponent
 */
export const WinLossFlag = ({ type }) => {
  return type ? <BadgeComponent type={type} /> : null;
};

WinLossFlag.propTypes = {
  /**
   * type is the string value
   */
  type: PropTypes.string,
};
