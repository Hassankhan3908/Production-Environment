import React from 'react';
import { PropTypes } from 'prop-types';
import { Box, Icon, Text } from '@chakra-ui/react';

/**
 * TitleWithIcon Component is used to render a Icon with relevant Text(Heading) based on two values props (title,children) 
   title is the text to be rendered as heading while is children props will be used as Icon
 * Inputs:title, children
 */
export const TitleWithIcon = ({
  title,
  children,
    subtitle,
  fontSizeIcon,
  fontSizeText,
  fontSizeBox = 32,
  fontWeight = 'bold',
  iconColor,
  marginRight,
  paddingY,
  boxColor,
}) => {
  return (
    <Box
      color={boxColor || ''}
      width='100%'
      py={paddingY || '15px'}
      px='0px'
    >
      <Icon
        mr={marginRight}
        fontSize={fontSizeIcon || fontSizeBox}
        display='inline-block'
        color={iconColor || 'general.blue'}
      >
        {children}
      </Icon>
      <Text fontSize={fontSizeText || fontSizeBox} fontWeight={fontWeight} display='inline-block'>
        {title}
      </Text>{subtitle}
    </Box>
  );
};

TitleWithIcon.propTypes = {
  /**
   * title is the string to be rendered as bold Heading
   */
  title: PropTypes.string,
};
