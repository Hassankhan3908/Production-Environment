import React from 'react'
import { TitleWithIcon } from './TitleWithIcon'
import { YourDNA } from 'src/assets/icons'
export default {
  title: 'DesignComponents/TitleWithIcon',
  component: TitleWithIcon,
}

export const TitleWithIconExample = () => {
  return (
    <TitleWithIcon title="Your DNA">
      <YourDNA />
    </TitleWithIcon>
  )
}
