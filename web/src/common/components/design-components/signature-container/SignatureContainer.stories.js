import React from 'react';
import { Container, Divider, HStack } from '@chakra-ui/react';
import { SignatureContainer } from './SignatureContainer';
import { LinkBoxComponent } from '../linkbox-component/LinkBoxComponent';
import { AccordionList } from '../accordion-list/AccordionList';
import { PropertyValueStack } from '../property-value-stack/PropertyValueStack';
import { SignatureContainerTypes } from './SignatureContainerValueType';
import { FORECASTTYPES } from '../property-value-stack/forecastTypes';
import { DATA } from '../accordion-list/ListData';
import {
  propertyValues,
  propertyValuesOutlier,
  propertyValuesBad,
} from './SampleData';
import { GOOD_SIGNATURE } from './SingatureData';
export default {
  title: 'DesignComponents/SignatureContainer',
  component: SignatureContainer,
};

export const Good = () => {
  return (
    <Container>
      <SignatureContainer
        type={GOOD_SIGNATURE.type}
        amount={GOOD_SIGNATURE.amount}
        days={GOOD_SIGNATURE.days}
        pipeline={GOOD_SIGNATURE.pipeline}
        revenue={GOOD_SIGNATURE.revenue}
        rate={GOOD_SIGNATURE.rate}
      />
    </Container>
  );
};
export const BadWithoutDaysProp = () => {
  return (
    <Container spacing={3}>
      <SignatureContainer
        amount={123}
        rate={25}
        revenue={11897}
        type={SignatureContainerTypes.BAD}
      />
    </Container>
  );
};
export const ContainerComponentsSideBySide = () => {
  return (
    <HStack alignItems='flex-start' spacing={5} justifyContent='center'>
      <SignatureContainer
        type={SignatureContainerTypes.GOOD}
        revenue={123123132}
        amount={189908}
        days={786}
        rate={25}
        pipeline={789879}
      >
        <Divider />
        <LinkBoxComponent />
        <AccordionList list={DATA} />
        <AccordionList list={DATA} />
        <AccordionList list={DATA} />
      </SignatureContainer>
      <SignatureContainer
        type={SignatureContainerTypes.BAD}
        revenue={123123132}
        amount={189908}
        days={786}
        rate={25}
        pipeline={789879}
      >
        <Divider />
        <LinkBoxComponent />
        <AccordionList list={DATA} />
      </SignatureContainer>
      <SignatureContainer
        type={SignatureContainerTypes.OUTLIER}
        revenue={123123132}
        amount={189908}
        days={786}
        rate={25}
        pipeline={789879}
      >
        <Divider />
        <LinkBoxComponent />
        <AccordionList list={DATA} />
        <AccordionList list={DATA} />
      </SignatureContainer>
    </HStack>
  );
};
export const OutlierWithRevenue = () => {
  return (
    <Container>
      <SignatureContainer
        revenue={123123123}
        type={SignatureContainerTypes.OUTLIER}
      />
    </Container>
  );
};
export const WithoutValues = () => {
  return (
    <Container>
      <SignatureContainer type={SignatureContainerTypes.OUTLIER} />;
    </Container>
  );
};

export const GoodWithChildrenProps = () => {
  return (
    <Container>
      <SignatureContainer
        type={SignatureContainerTypes.GOOD}
        revenue={123123132}
        amount={189908}
        days={786}
        rate={25}
        pipeline={789879}
      >
        <Divider />
        <LinkBoxComponent />
        <AccordionList list={DATA} />
        <AccordionList list={DATA} />
        <AccordionList list={DATA} />
      </SignatureContainer>
    </Container>
  );
};

export const ForecastFeatureComponentGood = () => {
  return (
    <Container>
      <SignatureContainer
        type={SignatureContainerTypes.GOOD}
        amount={189908}
        days={786}
      >
        {propertyValues.map((element, index) => {
          const { percentage, type } = element;

          return percentage && type ? (
            <PropertyValueStack
              key={index}
              title={FORECASTTYPES.predicted}
              percentage={percentage}
              type={type}
              value={41987905}
            />
          ) : (
            <PropertyValueStack
              key={index}
              title={element.title}
              value={element.value}
            />
          );
        })}
      </SignatureContainer>
    </Container>
  );
};
export const ForecastFeatureComponentBad = () => {
  return (
    <Container>
      <SignatureContainer
        type={SignatureContainerTypes.BAD}
        amount={189908}
        days={786}
      >
        {propertyValuesBad.map((element, index) => {
          const { percentage, type } = element;
          return percentage && type ? (
            <PropertyValueStack
              key={index}
              title={FORECASTTYPES.predicted}
              percentage={percentage}
              type={type}
              value={41987905}
            />
          ) : (
            <PropertyValueStack
              key={index}
              title={element.title}
              value={element.value}
            />
          );
        })}
      </SignatureContainer>
    </Container>
  );
};
export const ForecastFeatureComponentOutlier = () => {
  return (
    <Container>
      <SignatureContainer
        type={SignatureContainerTypes.OUTLIER}
        amount={189908}
        days={786}
      >
        {propertyValuesOutlier.map((element, index) => {
          const { percentage, type } = element;
          return percentage && type ? (
            <PropertyValueStack
              key={index}
              title={FORECASTTYPES.predicted}
              percentage={percentage}
              type={type}
              value={41987905}
            />
          ) : (
            <PropertyValueStack
              key={index}
              title={element.title}
              value={element.value}
            />
          );
        })}
      </SignatureContainer>
    </Container>
  );
};
