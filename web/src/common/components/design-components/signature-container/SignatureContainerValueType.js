export const SignatureContainerTypes = {
  GOOD: 'Good',
  BAD: 'Bad',
  OUTLIER: 'Outlier',
};
