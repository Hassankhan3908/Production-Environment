import React from 'react'
import { PropTypes } from 'prop-types'
import { Box, Flex, Icon, Heading, Text } from '@chakra-ui/react'
import { CurrencyText } from '../../general/number-text/CurrencyText'
import { SignatureContainerTypes } from './SignatureContainerValueType'
import { NUMBERTYPE } from '../../general/number-text/numberType'
import {
  PiplineIcon,
  RevenueIcon,
  RateIcon,
  SignatureLike,
  SigntureDislike,
  SignatureFlash,
} from 'src/assets/icons'
import { HeaderContent } from './HeaderContent'
/**
 * SignatureContainer Component will render data and adsjust itself dynamically on the basis of props it recieves, divided by the <hr />(divider) on above it will render data based on below mentiond inputs and below the divider it renders child components

* Inputs : amount, days, revenue, pipeline, rate, type
 */

export const SignatureContainer = (props) => {
  const {
    amount,
    days,
    pipeline,
    revenue,
    rate,
    type,
    title,
    children,
    totalPipeline,
    totalRevenue,
  } = props
  const containerBorderColor =
    type === SignatureContainerTypes.GOOD
      ? 'container.green'
      : type === SignatureContainerTypes.BAD
      ? 'container.red'
      : 'container.purple'
  return (
    <Box
      position="relative"
      borderTopWidth="5px"
      borderTopColor={containerBorderColor}
      bgColor="badge.textColor"
      boxShadow="md"
      rounded="sm"
      overflowY="auto"
      p={8}
      width="100%"
      maxW="400px"
    >
      <Flex direction="column">
        {(rate && (
          <HeaderContent
            type={type}
            containerBorderColor={containerBorderColor}
            rate={rate}
          />
        )) || (
          <>
            <Flex>
              {title && <Heading size="md"> {title}</Heading>}
              <Icon width={10} height={10} viewBox="0 0 35 35">
                {type === SignatureContainerTypes.GOOD ? (
                  <SignatureLike />
                ) : type === SignatureContainerTypes.BAD ? (
                  <SigntureDislike />
                ) : type === SignatureContainerTypes.OUTLIER ? (
                  <SignatureFlash />
                ) : (
                  ''
                )}
              </Icon>
            </Flex>
            {amount && (
              <Box
                my={2.5}
                fontSize={16}
                color={containerBorderColor}
                fontWeight="bold"
              >
                {<CurrencyText numberType={NUMBERTYPE.short} value={amount} />}
                {days && ` + ${days} days`}
              </Box>
            )}
          </>
        )}
        {pipeline && (
          <Box my={2.5} color="general.gray" fontSize={14} fontWeight="normal">
            <Icon fontSize={22} position="relative" top={1}>
              <PiplineIcon />
            </Icon>
            {<CurrencyText numberType={NUMBERTYPE.short} value={pipeline} />}
            {` Pipeline`}
            <Text ml={1} display="inline" fontWeight="semibold">
              ({totalPipeline}% of total)
            </Text>
          </Box>
        )}
        {revenue && (
          <Box my={2.5} color="general.gray" fontSize={14} fontWeight="normal">
            <Icon fontSize={22} position="relative" top={1}>
              <RevenueIcon />
            </Icon>
            {<CurrencyText numberType={NUMBERTYPE.short} value={revenue} />}
            {` Revenue`}
            <Text ml={1} display="inline" fontWeight="semibold">
              ({totalRevenue}% of total)
            </Text>
          </Box>
        )}
        {rate && (
          <Box my={2.5} color="general.gray" fontSize={14} fontWeight="normal">
            <Icon fontSize={22} position="relative" top={1}>
              <RateIcon />
            </Icon>
            <CurrencyText value={amount} numberType={NUMBERTYPE.short} />
            {` + ${days} days`}
          </Box>
        )}
      </Flex>
      <Box bgColor="white !important">{children}</Box>
    </Box>
  )
}
SignatureContainer.propTypes = {
  /**
   * Type is string change the component appearance accordingly
   */
  type: PropTypes.string.isRequired,
  /**
   * Actual amount
   */
  amount: PropTypes.number,
  /**
   * Days
   */
  days: PropTypes.number,
  /**
   * Pipeline
   */
  pipeline: PropTypes.number,
  /**
   * Generated revenue
   */
  revenue: PropTypes.number,
  /**
   * Rate
   */
  rate: PropTypes.number,
}
