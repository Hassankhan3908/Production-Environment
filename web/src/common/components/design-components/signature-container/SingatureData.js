export const GOOD_SIGNATURE = {
    type: 'Good',
    revenue: 1324135,
    amount: 189908,
    days: 123,
    rate: 25,
    pipeline: 25236,
    topValues: [{
        title: 'Top Industries',
        values: [{ industry: 'Financial', pScore: '3.5' }, { industry: 'Retail', pScore: '3.0' }, {
            industry: 'Technology',
            pScore: '2.2'
        }, { industry: 'Entertainment', pScore: '1.4' }, { industry: 'Transportation', pScore: '0.6' }]
    }, { title: 'Top Competitors', values: [{ industry: 'New Relic', pScore: '1.8' }] }, {
        title: 'Top Incumbents',
        values: [{ industry: 'CA', pScore: '4' }]
    }, { title: 'Top Opportunity Source', values: [{ industry: 'Channel', pScore: '2.3x' }] }],
    timeline: [{
        type: 'activated',
        text: 'Account Activated',
        subActivities: [{}, { type: 'report', value: '11.7', text: 'Sales Touches Before' }, {
            type: 'person',
            value: '128-day',
            text: ' Marketing Nurture'
        }]
    }, {
        type: 'created',
        text: 'Opportunity Created',
        subActivities: [{ type: 'screen', value: '18-day', text: 'Proof of Concept' }, {}, {
            type: 'report',
            value: '1.3',
            text: 'Engaged Contacts'
        }, { type: 'report', value: '2.8', text: 'Sales Touches After' }, {
            type: 'report',
            value: '235-day',
            text: 'Sales Nurture'
        }]
    }, { type: 'won', text: 'Opportunity Won!', subActivities: [] }]
};
