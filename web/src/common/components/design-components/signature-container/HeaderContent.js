import React from 'react'
import { Heading, HStack, VStack, Icon, Flex } from '@chakra-ui/react'
import { SignatureContainerTypes } from './SignatureContainerValueType'
import {
  SignatureLike,
  SigntureDislike,
  SignatureFlash,
} from 'src/assets/icons'
export const HeaderContent = ({ title, type, rate, containerBorderColor }) => {
  return (
    <HStack justifyContent="space-between">
      <VStack alignItems="flex-start" w="100%">
        {(type && (
          <Flex w="100%" justifyContent="space-between">
            <Heading size="md" mb={4}>
              The {type} Signature
            </Heading>
            <Icon width={10} height={10} viewBox="0 0 35 35">
              {type === SignatureContainerTypes.GOOD ? (
                <SignatureLike />
              ) : type === SignatureContainerTypes.BAD ? (
                <SigntureDislike />
              ) : type === SignatureContainerTypes.OUTLIER ? (
                <SignatureFlash />
              ) : (
                ''
              )}
            </Icon>
          </Flex>
        )) || <Heading size="md">{title}</Heading>}

        {(type && (
          <Heading
            mb={4}
            color={containerBorderColor}
            size="sm"
            fontWeight="bold"
          >
            {rate}% Win Rate
          </Heading>
        )) ||
          null}
      </VStack>
    </HStack>
  )
}
