import React from 'react';
import { Container } from '@chakra-ui/react';
import { PropertyValueStack } from './PropertyValueStack';
import { FORECASTTYPES } from './forecastTypes';
export default {
  title: 'DesignComponents/PropertyValueStack',
  component: PropertyValueStack,
};

export const PropertyValueStackPipline = () => {
  return (
    <Container bgColor='badge.textColor' px={0}>
      <PropertyValueStack
        type='Good'
        percentage={12}
        title={FORECASTTYPES.predicted}
        value={1231231232}
      ></PropertyValueStack>
    </Container>
  );
};

export const PropertyValueStackCommit = () => {
  return (
    <Container bgColor='badge.textColor' px={0}>
      <PropertyValueStack
        percentage={78}
        type='Bad'
        title={FORECASTTYPES.predicted}
        value={6767632342}
      ></PropertyValueStack>
    </Container>
  );
};

export const PropertyValueStackUpside = () => {
  return (
    <Container bgColor='badge.textColor' px={0}>
      <PropertyValueStack
        type='Outlier'
        title={FORECASTTYPES.predicted}
        value={1231231232}
        percentage={82}
      ></PropertyValueStack>
    </Container>
  );
};
export const PropertyValueStackBestCase = () => {
  return (
    <Container bgColor='badge.textColor' px={0}>
      <PropertyValueStack
        title={FORECASTTYPES.bestCase}
        value={68678678}
      ></PropertyValueStack>
    </Container>
  );
};

export const PropertyValueStackTarget = () => {
  return (
    <Container bgColor='badge.textColor' px={0}>
      <PropertyValueStack
        title={FORECASTTYPES.target}
        value={34534578}
      ></PropertyValueStack>
    </Container>
  );
};
