export const FORECASTTYPES = {
  pipeline: 'Pipeline',
  commit: 'Commit',
  upside: 'Upside',
  bestCase: 'Best Case',
  target: 'Target',
  predicted: 'Predicted',
};
