import React from 'react';
import { PropTypes } from 'prop-types';
import { VStack, Text, Divider, Box } from '@chakra-ui/react';
import { CurrencyText } from '../../general/number-text/CurrencyText';
import { NUMBERTYPE } from '../../general/number-text/numberType';

/**
 * PropertyValueStack component renders data with a heading (title) and a value (text)
   if we provide two additional props(type, percentage) it will return a predicted Component with amount and probability
 *
 * Inputs: title,value,type,percentage
 */
export const PropertyValueStack = ({
  title,
  value,
  type,
  border,
  bShadow,
  bColor,
}) => {
  const containerBgColor =
    ((type === 'Good' || type === 'Bad' || type === 'Outlier') &&
      title === 'Predicted' &&
      type &&
      `container.l${type}`) ||
    '';
  let text = title;
  let result = text.replace(/([A-Z])/g, ' $1');
  let finalResult = result.charAt(0).toUpperCase() + result.slice(1);
  return (
    <VStack
      boxShadow={bShadow || 'base'}
      px={2.5}
      py={1}
      my={4}
      border={border || '1px'}
      borderColor='borderGrey'
      alignItems='flex-start'
      fontSize={13}
      fontWeight='medium'
      bgColor={containerBgColor || bColor}
    >
      <Text color='general.gray'>{finalResult}</Text>
      <Divider borderBottomWidth={2} borderBottomColor='gray.300' />
      <Box>
        {(!!containerBgColor && (
          <CurrencyText value={value} numberType={NUMBERTYPE.long} />
        )) || <CurrencyText value={value} numberType={NUMBERTYPE.long} />}
      </Box>
    </VStack>
  );
};
PropertyValueStack.propTypes = {
  /**
   * value is the number (amount)
   */
  value: PropTypes.number | PropTypes.string,
  /**
   * Title is the string to be displayed as text
   */
  title: PropTypes.string,
  /**
   * Percentage is the probability
   */
  percentage: PropTypes.number,
  /**
   * Type is string which defines whether the component is the predicted component or generic one
   */
  type: PropTypes.string,
};
