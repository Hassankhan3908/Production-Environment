import React from 'react';
import { TaskStatus } from './TaskStatus';

export default {
  title: 'DesignComponents/TaskStatus',
  component: TaskStatus,
};

export const TaskStatusExample = () => {
  return <TaskStatus tasksCompleted={7}></TaskStatus>;
};
export const TaskStatusWithTotalOf20Tasks = () => {
  return <TaskStatus tasksCompleted={12} totalTasks={20}></TaskStatus>;
};
export const TaskStatusWithTotalOf72Tasks = () => {
  return <TaskStatus tasksCompleted={72} totalTasks={72}></TaskStatus>;
};
