import React from 'react';
import { PropTypes } from 'prop-types';
import { Box, Icon, Text } from '@chakra-ui/react';
import { TaskStatusIcon } from 'src/assets/icons';

/**
 * TaskStatus is the component which accepts tasksCompleted and totalTasks as a props and render it as completed tasks out of
   totalTasks
 * Inputs: value,totalTasks
 */
export const TaskStatus = ({ tasksCompleted, totalTasks = 10 }) => {
  return (
    <Box>
      <Icon fontSize={32} position='relative' top={1.5}>
        <TaskStatusIcon />
      </Icon>
      <Text display='inline-block' color='general.blue'>
        {tasksCompleted}/{totalTasks}
      </Text>
    </Box>
  );
};

TaskStatus.propTypes = {
  /**
   * TasksCompleted is the number of task completed
   */
  tasksCompleted: PropTypes.number,
  /**
   * totalTask is the total amount of task to be completed
   */
  totalTasks: PropTypes.number,
};
