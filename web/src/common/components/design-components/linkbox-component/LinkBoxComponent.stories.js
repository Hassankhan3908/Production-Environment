import React from 'react';
import { LinkBoxComponent } from './LinkBoxComponent';

export default {
  title: 'DesignComponents/LinkBoxComponent',
  component: LinkBoxComponent,
};

export const LinkBoxComponentExample = () => {
  return <LinkBoxComponent text='What are Power Scores ?' />;
};
