import React from 'react';
import { PropTypes } from 'prop-types';
import { LinkBox, LinkOverlay, Icon } from '@chakra-ui/react';
import { LinkBoxIcon } from 'src/assets/icons';

/**
 * LinkBoxComponent renders a link, it changes It's text based on provided prop(text) and a url prop which goes to the
   mentioned location address
 *
 * Inputs: text, url
 */

export const LinkBoxComponent = ({ text, url = '#' }) => {
  return text ? (
    <LinkBox fontWeight='medium' fontSize='small'>
      <LinkOverlay color='general.blue' href={url}>
        {text}
        <Icon fontSize={20} mt={2} ml='1'>
          <LinkBoxIcon />
        </Icon>
      </LinkOverlay>
    </LinkBox>
  ) : null;
};

LinkBoxComponent.propTypes = {
  /**
   * Text is the inner content of link component
   */
  text: PropTypes.string,
  /**
   * URL props refers to the mentioned location address
   */
  url: PropTypes.string,
};
