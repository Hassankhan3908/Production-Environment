import React from 'react';
import { Container } from '@chakra-ui/react';
import { AccordionList } from './AccordionList';
import { DATA } from './ListData';

export default {
  title: 'DesignComponents/AccordionListComponent',
  component: AccordionList,
};

export const AccordionListComponent = () => {
  return (
    <Container bgColor='badge.textColor' px={0}>
      <AccordionList list={DATA} />
    </Container>
  );
};
