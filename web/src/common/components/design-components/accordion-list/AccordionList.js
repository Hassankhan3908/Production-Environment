import React, { useState } from 'react';
import { PropTypes } from 'prop-types';
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Divider,
  ListItem,
  OrderedList,
  Spacer,
  Text,
} from '@chakra-ui/react';
/**
 * AccordionList Component is used to render data in a ordered list format, It accepts array and renders a list
   with dropdown and toggle functionality

 * Inputs: list
 */
export const AccordionList = ({ list: { values, type } }) => {
  const [hide, setHide] = useState(false);
  return values.length > 0 ? (
    <Accordion
      boxShadow='md'
      allowToggle
      border='1px'
      borderColor='borderGrey'
      borderRadius='sm'
      fontWeight='medium'
      my={3}
      mt={0}
      bgColor='white'
    >
      <AccordionItem border='none'>
        <h2>
          <AccordionButton
            py={0}
            onClick={() => setHide(!hide)}
            my={1}
            fontSize='sm'
          >
            <Box
              flex='1'
              textAlign='left'
              color='general.gray'
              fontWeight='medium'
            >
              Top {type}
            </Box>
            <Box fontWeight='medium' color='general.blue'>
              View all
              <AccordionIcon />
            </Box>
          </AccordionButton>
          {!hide && (
            <OrderedList>
              <Divider />
              <ListItem my={1} display='flex' pr={3} fontSize='sm'>
                1. {values[0].name}
                <Spacer />
                <Box whiteSpace='nowrap'>
                  <Text
                    display='inline-block'
                    color='general.gray'
                    fontWeight='normal'
                  >
                    Power Score:
                  </Text>
                  <Text
                    display='inline-block'
                    fontSize='xs'
                    minW='38px'
                    ml='2px'
                  >
                    {` ${values[0].score}x`}
                  </Text>
                </Box>
              </ListItem>
            </OrderedList>
          )}
        </h2>

        <AccordionPanel pb={4} p='0'>
          {hide && (
            <OrderedList>
              <Divider />
              {values.map(({ name, score }, index) => (
                <ListItem
                  my={1}
                  key={index}
                  display='flex'
                  pr={3}
                  fontSize='sm'
                >
                  {index + 1}. {name} <Spacer />
                  <Box minW='120px'>
                    <Text
                      color='general.gray'
                      fontWeight='normal'
                      display='inline-block'
                    >
                      Power Score:
                    </Text>
                    <Text
                      display='inline-block'
                      fontSize='xs'
                      minW='38px'
                      ml='2px'
                    >
                      {` ${score}x`}
                    </Text>
                  </Box>
                </ListItem>
              ))}
            </OrderedList>
          )}
        </AccordionPanel>
      </AccordionItem>
    </Accordion>
  ) : null;
};

AccordionList.propTypes = {
  /**
   * List represents sorted records
   */
  list: PropTypes.object,
};
