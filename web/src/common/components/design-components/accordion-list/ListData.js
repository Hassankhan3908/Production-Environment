export const DATA = { title: 'Top Industries',
    values: [{
        industry: 'Financial',
        pScore: '3.5',
    },
        {
            industry: 'Retail',
            pScore: '3.0',
        },
        {
            industry: 'Technology',
            pScore: '2.2',
        },
        {
            industry: 'Entertainment',
            pScore: '1.4',
        },
        {
            industry: 'Transportation',
            pScore: '0.6',
        }]
};
