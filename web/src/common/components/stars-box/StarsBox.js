import React from 'react'
import { Flex, Icon } from '@chakra-ui/react'
import { StarIcon } from 'src/assets/icons'
export const StarsBox = ({ value }) => {
  return (
    <Flex>
      {value === 3 ? (
        <>
          <Icon viewBox="0 0 20 20" ml={1}>
            <StarIcon />
          </Icon>
          <Icon viewBox="0 0 20 20" ml={1}>
            <StarIcon />
          </Icon>
          <Icon viewBox="0 0 20 20" ml={1}>
            <StarIcon />
          </Icon>
        </>
      ) : value === 2 ? (
        <>
          <Icon viewBox="0 0 20 20" ml={1}>
            <StarIcon />
          </Icon>
          <Icon viewBox="0 0 20 20" ml={1}>
            <StarIcon />
          </Icon>
        </>
      ) : (
        <Icon viewBox="0 0 20 20" ml={1}>
          <StarIcon />
        </Icon>
      )}
    </Flex>
  )
}
