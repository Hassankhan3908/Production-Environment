import React from 'react'

import { StarsBox } from './StarsBox'
export default {
  title: 'Components/StarBox',
  component: StarsBox,
}

export const StarsBoxExample = () => {
  return <StarsBox value={1} />
}
