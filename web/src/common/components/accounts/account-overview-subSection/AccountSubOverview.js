import React from 'react';
import { HStack, Text } from '@chakra-ui/react';

export const AccountSubOverview = ({ title, value }) => {
  return (
    <HStack justifyContent='space-between'>
      <Text fontSize='sm' color='general.gray'>
        {title}
      </Text>
      <Text fontWeight='semibold'>{value}</Text>
    </HStack>
  );
};
