import React from 'react';
import { Text } from '@chakra-ui/react';
export const PerformanceStatus = ({ text }) => {
  return (
    <Text
      bgColor='general.blue1'
      color='general.blue'
      fontWeight='bold'
      fontSize='sm'
      px={2}
      py={2}
      borderRadius='md'
    >
      {text}
    </Text>
  );
};
