import React from 'react';
import { VStack, HStack, Text, Flex, Icon } from '@chakra-ui/react';
import { StarIcon, StarEmpty } from 'src/assets/icons';
export const EngagementPerformance = ({ text, count, performance }) => {
  performance = Number(performance);
  return (
    <VStack alignItems='flex-start' px={2}>
      <HStack alignSelf='stretch' justifyContent='space-between'>
        <Text fontSize='small' fontWeight='semibold' color='general.gray'>
          {text}
        </Text>
        <Text fontWeight='bold'>{count}</Text>
      </HStack>

      <Flex
        px={1}
        py={2}
        bgColor='gray.50'
        borderRadius='md'
        width={20}
        justifyContent='space-around'
      >
        {performance === 1 ? (
          <>
            <Icon viewBox='0 0 14 13'>
              <StarIcon />
            </Icon>
            <Icon viewBox='0 0 14 13'>
              <StarIcon />
            </Icon>

            <Icon viewBox='0 0 14 13'>
              <StarIcon />
            </Icon>
          </>
        ) : performance === 2 ? (
          <>
            <Icon viewBox='0 0 14 13'>
              <StarIcon />
            </Icon>
            <Icon viewBox='0 0 14 13'>
              <StarIcon />
            </Icon>

            <Icon viewBox='0 0 14 13'>
              <StarEmpty />
            </Icon>
          </>
        ) : performance === 3 ? (
          <>
            <Icon viewBox='0 0 14 13'>
              <StarIcon />
            </Icon>

            <Icon viewBox='0 0 14 13'>
              <StarEmpty />
            </Icon>
            <Icon viewBox='0 0 14 13'>
              <StarEmpty />
            </Icon>
          </>
        ) : (
          <>
            <Icon viewBox='0 0 14 13'>
              <StarEmpty />
            </Icon>
            <Icon viewBox='0 0 14 13'>
              <StarEmpty />
            </Icon>
            <Icon viewBox='0 0 14 13'>
              <StarEmpty />
            </Icon>
          </>
        )}
      </Flex>
    </VStack>
  );
};
