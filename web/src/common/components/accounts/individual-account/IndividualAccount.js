import React from 'react';
import { Flex, HStack, VStack, Icon, Text, Divider } from '@chakra-ui/react';
import { ArrowRightIcon } from 'src/assets/icons';
import { EngagementPerformance } from './EngagementPerformance';
import { GetIcon } from '@salesdna/ui';
export const IndividualAccount = ({ props }) => {
  return (
    <VStack bgColor='white' p={5} borderRadius='md' shadow='lg'>
      <HStack alignSelf='stretch' justifyContent='space-between'>
        <Flex>
          <HStack alignSelf='stretch'>
            <Text bgColor='gray.300' py={2} px={4} borderRadius='md'>
              {props.name[0]}
            </Text>
          </HStack>
          <VStack ml={2} alignItems='flex-start'>
            <Text fontWeight='bold'>{props.name}</Text>
            <Text
              m='0px !important'
              fontSize='small'
              fontWeight='semibold'
              color='general.blue'
            >
              View Details
              <Icon ml={1} width={3} height={3} viewBox='0 0 11 11'>
                <ArrowRightIcon />
              </Icon>
            </Text>
          </VStack>
        </Flex>
        <Flex>
          <Text
            mr={2}
            fontSize='sm'
            fontWeight='semibold'
            color='general.gray'
            display='inline-block'
          >
            {props.heat_score === 3
              ? 'High Heat 🔥🔥🔥'
              : props.heat_score === 2
              ? 'Med Heat 🔥🔥'
              : 'Low Heat 🔥'}
          </Text>
        </Flex>
      </HStack>
      <Divider />
      <HStack alignSelf='stretch' justifyContent='space-between'>
        <EngagementPerformance
          text='Num of engaged contacts'
          count={props.account_attributes.number_of_engaged_contacts}
          performance={Number(
            props.account_attributes.engaged_contacts_performance
          )}
        />
        <EngagementPerformance
          text='Num of open Opportunities'
          count={props.account_attributes.number_of_open_opportunities}
          performance={Number(
            props.account_attributes.open_opportunities_performance
          )}
        />
      </HStack>
    </VStack>
  );
};
