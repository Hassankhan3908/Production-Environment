import React from 'react';
import { IndividualAccount } from './IndividualAccount';

export default {
  title: 'Account/IndividualAccount',
  component: IndividualAccount,
};

export const IndividualAccountPageExample = () => {
  return <IndividualAccount />;
};
