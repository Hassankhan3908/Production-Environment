import React from 'react';
import { PropTypes } from 'prop-types';
import { useCurrencyHook } from './useCurrencyHook';
/**
 * CurrencyText is Component which will return the string in a specific formats(long,short) base on the prop
 *
 * Inputs:value, numberType
 */
export const CurrencyText = ({ value, numberType }) => {
  const [currency] = useCurrencyHook(value, numberType);
  return <>{value === 0 ? '-' : currency}</>;
};

CurrencyText.propTypes = {
  /**
   * Value  will be convered in string value
   */
  value: PropTypes.number,
  /**
   * NumberType: will decide the required format
   */
  numberType: PropTypes.string,
};
