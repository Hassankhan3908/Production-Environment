import { useState, useEffect } from 'react';
import { NUMBERTYPE } from './numberType';
function amountWithUnit(number) {
  const Billion = 1000000000;
  const million = 1000000;
  const thousand = 1000;
  return number >= Billion
    ? number % thousand
      ? `$${(number / Billion).toFixed(2)} B`
      : `$${number / Billion} B`
    : number >= million
    ? number % thousand
      ? `$${(number / million).toFixed(2)} M`
      : `$${number / million} M`
    : number >= thousand
    ? number % thousand
      ? `$${(number / thousand).toFixed(2)}K`
      : `$${number / thousand}K`
    : `$${number}`;
}
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 0,
});

export const useCurrencyHook = (number, numberType) => {
  const [currency, setCurrency] = useState();
  if (numberType === NUMBERTYPE.short) {
    useEffect(() => setCurrency(amountWithUnit(number)), [number]);
  } else if (numberType === NUMBERTYPE.long) {
    useEffect(() => setCurrency(formatter.format(number)), [number]);
  }
  return [currency];
};
