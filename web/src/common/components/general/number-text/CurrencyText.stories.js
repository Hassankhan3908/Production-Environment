import React from 'react';
import { CurrencyText } from './CurrencyText';
import { NUMBERTYPE } from './numberType';
import { Heading } from '@chakra-ui/react';

export default {
  title: 'General/CurrencyText',
  component: CurrencyText,
};

export const CurrencyTextExampleShortFormat12312314 = () => {
  return (
    <Heading>
      <CurrencyText value={12313122} numberType={NUMBERTYPE.short} />
    </Heading>
  );
};
export const CurrencyTextExampleShortFormat1231 = () => {
  return (
    <Heading>
      <CurrencyText value={1231} numberType={NUMBERTYPE.short} />
    </Heading>
  );
};
export const CurrencyTextExampleShortFormat3533 = () => {
  return (
    <Heading>
      <CurrencyText value={3533} numberType={NUMBERTYPE.short} />
    </Heading>
  );
};
export const CurrencyTextExampleShortFormat35330 = () => {
  return (
    <Heading>
      <CurrencyText value={35330} numberType={NUMBERTYPE.short} />
    </Heading>
  );
};
export const CurrencyTextExampleShortFormat895330 = () => {
  return (
    <Heading>
      <CurrencyText value={895330} numberType={NUMBERTYPE.short} />
    </Heading>
  );
};
export const CurrencyTextExampleShortFormat8953300 = () => {
  return (
    <Heading>
      <CurrencyText value={8953300} numberType={NUMBERTYPE.short} />
    </Heading>
  );
};

export const CurrencyTextExampleLongFormat12313122 = () => {
  return (
    <Heading>
      <CurrencyText value={12313122} numberType={NUMBERTYPE.long} />
    </Heading>
  );
};
export const CurrencyTextExampleLongFormat123 = () => {
  return (
    <Heading>
      <CurrencyText value={123} numberType={NUMBERTYPE.long} />
    </Heading>
  );
};
export const CurrencyTextExampleLongFormat789667 = () => {
  return (
    <Heading>
      <CurrencyText value={789667} numberType={NUMBERTYPE.long} />
    </Heading>
  );
};
