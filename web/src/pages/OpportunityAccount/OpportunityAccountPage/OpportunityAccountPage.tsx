import OpportunityAccountCell from 'src/components/OpportunityAccount/OpportunityAccountCell'

type OpportunityAccountPageProps = {
  id: String
}

const OpportunityAccountPage = ({ id }: OpportunityAccountPageProps) => {
  return <OpportunityAccountCell id={id} />
}

export default OpportunityAccountPage
