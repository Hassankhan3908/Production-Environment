import OpportunityAccountsCell from 'src/components/OpportunityAccount/OpportunityAccountsCell'
import { useAuth } from '@redwoodjs/auth'

const OpportunityAccountsPage = () => {
  const { userMetadata } = useAuth()
  return (
    <OpportunityAccountsCell
      id={userMetadata?.sub?.replace('salesforce|', '')}
    />
  )
}

export default OpportunityAccountsPage
