import { useAuth } from '@redwoodjs/auth'

const LoginPage = () => {
  const { isAuthenticated, logIn, logOut } = useAuth()

  return (
    <button
      onClick={async () => {
        if (isAuthenticated) {
          await logOut({ returnTo: process.env.AUTH0_REDIRECT_URI })
        } else {
          const searchParams = new URLSearchParams(window.location.search)
          await logIn({
            appState: { targetUrl: searchParams.get('redirectTo') },
          })
        }
      }}
    >
      {isAuthenticated ? 'Log out' : 'Log in'}
    </button>
  )
}

export default LoginPage
