import OpportunitiesCell from 'src/components/Opportunity/OpportunitiesCell'
import { useAuth } from '@redwoodjs/auth'

const OpportunitiesPage = () => {
  const { userMetadata } = useAuth()
  return (
    userMetadata && (
      <OpportunitiesCell id={userMetadata?.sub?.replace('salesforce|', '')} />
    )
  )
}

export default OpportunitiesPage
