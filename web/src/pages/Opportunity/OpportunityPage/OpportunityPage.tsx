import OpportunityCell from 'src/components/Opportunity/OpportunityCell'

type OpportunityPageProps = {
  id: String
}

const OpportunityPage = ({ id }: OpportunityPageProps) => {
  return <OpportunityCell id={id} />
}

export default OpportunityPage
