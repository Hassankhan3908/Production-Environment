export const styles = {
  global: {
    body: {
      bg: '#F5F7FB',
      color: '#314158',
    },
    a: {
      color: 'teal.500',
      _hover: {},
    },
    button: {
      boxShadow: 'none !important',
    },
  },
}
