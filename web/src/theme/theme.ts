import { extendTheme, theme } from '@chakra-ui/react'

import { styles } from './styles'
import Input from './components/Input'
const fonts = { mono: "'Roboto', monospace" }

const customTheme = {
  styles,

  space: {
    1: '0.25rem',
    574: '574px',
    35: '35px',
    zeroImportant: '0px !mportant',
  },

  colors: {
    body: {
      color: '#F5F7FB',
    },

    loginScreen: {
      bgColor: '#F5F7FB' + '60',
    },

    forecastSummary: {
      color: '#314158',
    },
    container: {
      green: '#4DD474',
      lGood: '#4DD474' + '20',

      red: '#EA3A50',
      lBad: '#EA3A50' + '20',

      purple: '#7359EC',
      lOutlier: '#7359EC' + '20',
    },
    badge: {
      textColor: 'white',
      TextHigh: '#EB5757',
      BgHigh: '#EB5757' + '50',
      TextMed: '#F2994A',
      BgMed: '#F2994A' + '50',
      TextLow: '#4DD474',
      BgLow: '#4DD474' + '50',
    },
    general: {
      blue: '#2766F6',
      blue1: '#2766F6' + '10',
      blueBorderColor: '#2766F6' + '40',
      gray: '#8181A5',
    },
    button: {
      default: '#4E4EAE',
      active: '#2D2675',
      disabled: '#E3E3E5',
    },
    PersonIcons: {
      border: '#2766F6' + '40',
    },
    menuItem: {
      bgColor: '#2766F6' + '15',
    },
    borderGrey: '#ECECF2',
    borderGreyOpac: '#ECECF2' + '40',
    dashedGrayOpac: '#A9B4CD' + '70',
    searchBgColor: '#FBFCFE',
    created: {
      450: '#ECB63C',
    },
    activated: {
      450: '#7359EC',
    },
    miniHeader: {
      bgColor: '#FBFCFE',
      ButtonColor: '#F6F7F9',
    },
    activityColors: {
      created: '#ECB63C',
      activated: '#7359EC',
      won: '#4DD474',
    },
    paginator: {
      bgColor: '#EFF2FE',
      color: '#5E81F4',
    },
  },
  fonts,
  components: {
    Button: {
      baseStyle: {
        borderRadius: 'md',
      },
    },
    Input,
  },
}

export const salesDNATheme = extendTheme(customTheme, theme)
