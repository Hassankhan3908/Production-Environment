// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

import { Set, Router, Route, Private } from '@redwoodjs/router'
import OpportunityAccountsLayout from 'src/layouts/OpportunityAccountsLayout'
import OpportunitiesLayout from 'src/layouts/OpportunitiesLayout'
import DnaSignaturesLayout from 'src/layouts/DnaSignaturesLayout'
import LoginPage from './pages/LoginPage/LoginPage'

const Routes = () => {
  return (
    <Router>
      <Private unauthenticated="login">
        <Set wrap={OpportunityAccountsLayout}>
          <Route path="/accounts/{id}" page={OpportunityAccountOpportunityAccountPage} name="opportunityAccount" />
          <Route path="/accounts" page={OpportunityAccountOpportunityAccountsPage} name="opportunityAccounts" />
        </Set>
        <Set wrap={OpportunitiesLayout}>
          <Route path="/opportunities/{id}" page={OpportunityOpportunityPage} name="opportunity" />
          <Route path="/opportunities" page={OpportunityOpportunitiesPage} name="opportunities" />
        </Set>

        <Set wrap={DnaSignaturesLayout}>
          <Route path="/dna" page={DnaSignatureDnaSignaturesPage} name="dnaSignatures" />
        </Set>

        <Route path="/" page={DnaSignatureDnaSignaturesPage} name="dna" />
      </Private>
      <Route path="/login" page={LoginPage} name="login" />
      <Route notfound page={NotFoundPage} />
    </Router>
  )
}

export default Routes
