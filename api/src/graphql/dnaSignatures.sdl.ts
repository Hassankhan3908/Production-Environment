export const schema = gql`
  type DnaSignature {
    id: String!
    dna_id_fk: String!
    total_pipeline: Float
    total_revenue: Float
    pct_win_rate: Float
    avg_deal_size: Float
    avg_nrtr_days: Float
    display_name: String
    effective_date: DateTime
    exp_date: DateTime
    ui_order: Int!
    ui_color: String!
    classification_label: Int
    ui_dna: Dna!
    ui_dna_signature_forecast: [DnaSignatureForecast]!
    ui_dna_signature_stage: [DnaSignatureStage]!
    ui_opportunity: [Opportunity]!
    ui_power_score: [PowerScore]!
  }

  type Query {
    dnaSignatures: [DnaSignature!]! @requireAuth
    dnaSignature(id: String!): DnaSignature @requireAuth
  }

  input CreateDnaSignatureInput {
    dna_id_fk: String!
    total_pipeline: Float
    total_revenue: Float
    pct_win_rate: Float
    avg_deal_size: Float
    avg_nrtr_days: Float
    display_name: String
    effective_date: DateTime
    exp_date: DateTime
    ui_order: Int!
    ui_color: String!
    classification_label: Int
  }

  input UpdateDnaSignatureInput {
    dna_id_fk: String
    total_pipeline: Float
    total_revenue: Float
    pct_win_rate: Float
    avg_deal_size: Float
    avg_nrtr_days: Float
    display_name: String
    effective_date: DateTime
    exp_date: DateTime
    ui_order: Int
    ui_color: String
    classification_label: Int
  }

  type Mutation {
    createDnaSignature(input: CreateDnaSignatureInput!): DnaSignature!
      @requireAuth
    updateDnaSignature(
      id: String!
      input: UpdateDnaSignatureInput!
    ): DnaSignature! @requireAuth
    deleteDnaSignature(id: String!): DnaSignature! @requireAuth
  }
`
