export const schema = gql`
  type AccountPowerscore {
    id: String!
    type: String!
    name: String!
    description: String
    dna_id_fk: String!
    score: Float!
    effective_date: DateTime
    exp_date: DateTime
    ui_dna: Dna!
  }

  type Query {
    accountPowerscores: [AccountPowerscore!]! @requireAuth
    accountPowerscore(id: String!): AccountPowerscore @requireAuth
  }

  input CreateAccountPowerscoreInput {
    type: String!
    name: String!
    description: String
    dna_id_fk: String!
    score: Float!
    effective_date: DateTime
    exp_date: DateTime
  }

  input UpdateAccountPowerscoreInput {
    type: String
    name: String
    description: String
    dna_id_fk: String
    score: Float
    effective_date: DateTime
    exp_date: DateTime
  }

  type Mutation {
    createAccountPowerscore(
      input: CreateAccountPowerscoreInput!
    ): AccountPowerscore! @requireAuth
    updateAccountPowerscore(
      id: String!
      input: UpdateAccountPowerscoreInput!
    ): AccountPowerscore! @requireAuth
    deleteAccountPowerscore(id: String!): AccountPowerscore! @requireAuth
  }
`
