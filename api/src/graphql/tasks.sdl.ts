export const schema = gql`
  type Task {
    id: String!
    opportunity_id_fk: String!
    display_name: String!
    sla: Int
    due: DateTime
    due_in_days: Int
    display_status: String!
    notes: String!
    current_value: Float
    min_target_value: Float
    max_target_value: Float
    priority: Int
    ui_opportunity: Opportunity!
  }

  type Query {
    tasks: [Task!]! @requireAuth
  }

  input CreateTaskInput {
    opportunity_id_fk: String!
    display_name: String!
    sla: Int
    due: DateTime
    due_in_days: Int
    display_status: String!
    notes: String!
    current_value: Float
    min_target_value: Float
    max_target_value: Float
    priority: Int
  }

  input UpdateTaskInput {
    opportunity_id_fk: String
    display_name: String
    sla: Int
    due: DateTime
    due_in_days: Int
    display_status: String
    notes: String
    current_value: Float
    min_target_value: Float
    max_target_value: Float
    priority: Int
  }
`
