export const schema = gql`
  type OpportunityRecommendations {
    id: String!
    recommendation: String
    opportunity_id_fk: String!
    ui_opportunity: Opportunity!
  }

  type Query {
    oppRecommendations: [OpportunityRecommendations!]! @requireAuth
  }

  input CreateOpportunityRecommendationsInput {
    recommendation: String
    opportunity_id_fk: String!
  }

  input UpdateOpportunityRecommendationsInput {
    recommendation: String
    opportunity_id_fk: String
  }
`
