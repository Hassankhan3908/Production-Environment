export const schema = gql`
  type Dna {
    id: String!
    account_id_fk: String!
    effective_date: DateTime
    exp_date: DateTime
    cut_id: Int
    ui_account: Account!
    ui_account_powerscore: [AccountPowerscore]!
    ui_dna_signature: [DnaSignature]!
  }

  type Query {
    dnas: [Dna!]! @requireAuth
    dna(id: String!): Dna @requireAuth
  }

  input CreateDnaInput {
    account_id_fk: String!
    effective_date: DateTime
    exp_date: DateTime
    cut_id: Int
  }

  input UpdateDnaInput {
    account_id_fk: String
    effective_date: DateTime
    exp_date: DateTime
    cut_id: Int
  }

  type Mutation {
    createDna(input: CreateDnaInput!): Dna! @requireAuth
    updateDna(id: String!, input: UpdateDnaInput!): Dna! @requireAuth
    deleteDna(id: String!): Dna! @requireAuth
  }
`
