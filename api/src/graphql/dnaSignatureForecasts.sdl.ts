export const schema = gql`
  type DnaSignatureForecast {
    id: String!
    signature_id_fk: String!
    effective_date: DateTime
    exp_date: DateTime
    rep_id_fk: String!
    rep_mgr_id_fk: String!
    avg_deal_size: Float
    avg_nrtr_days: Float
    pipeline: Float
    pipeline_commit: Float
    upside: Float
    best_case: Float
    pipeline_target: Float
    predicted: Float
    predicted_probability: Float
    year: Int
    month: Int
    day: Int
    quarter: Int
    date_dim_id_fk: Int
    ui_dna_signature: DnaSignature!
  }

  type Query {
    dnaSignatureForecasts: [DnaSignatureForecast!]! @requireAuth
    dnaSignatureForecast(id: String!): DnaSignatureForecast @requireAuth
  }

  input CreateDnaSignatureForecastInput {
    signature_id_fk: String!
    effective_date: DateTime
    exp_date: DateTime
    rep_id_fk: String!
    rep_mgr_id_fk: String!
    avg_deal_size: Float
    avg_nrtr_days: Float
    pipeline: Float
    pipeline_commit: Float
    upside: Float
    best_case: Float
    pipeline_target: Float
    predicted: Float
    predicted_probability: Float
    year: Int
    month: Int
    day: Int
    quarter: Int
    date_dim_id_fk: Int
  }

  input UpdateDnaSignatureForecastInput {
    signature_id_fk: String
    effective_date: DateTime
    exp_date: DateTime
    rep_id_fk: String
    rep_mgr_id_fk: String
    avg_deal_size: Float
    avg_nrtr_days: Float
    pipeline: Float
    pipeline_commit: Float
    upside: Float
    best_case: Float
    pipeline_target: Float
    predicted: Float
    predicted_probability: Float
    year: Int
    month: Int
    day: Int
    quarter: Int
    date_dim_id_fk: Int
  }

  type Mutation {
    createDnaSignatureForecast(
      input: CreateDnaSignatureForecastInput!
    ): DnaSignatureForecast! @requireAuth
    updateDnaSignatureForecast(
      id: String!
      input: UpdateDnaSignatureForecastInput!
    ): DnaSignatureForecast! @requireAuth
    deleteDnaSignatureForecast(id: String!): DnaSignatureForecast! @requireAuth
  }
`
