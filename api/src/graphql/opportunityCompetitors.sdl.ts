export const schema = gql`
  type OpportunityCompetitor {
    id: String!
    opportunity_id_fk: String!
    display_name: String!
    ui_opportunity: Opportunity!
  }

  type Query {
    opportunityCompetitors: [OpportunityCompetitor!]! @requireAuth
  }

  input CreateOpportunityCompetitorInput {
    opportunity_id_fk: String!
    display_name: String!
  }

  input UpdateOpportunityCompetitorInput {
    opportunity_id_fk: String
    display_name: String
  }
`
