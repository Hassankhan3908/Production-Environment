export const schema = gql`
  type OpportunityIncumbent {
    id: String!
    opportunity_id_fk: String!
    display_name: String!
    ui_opportunity: Opportunity!
  }

  type Query {
    opportunityIncumbents: [OpportunityIncumbent!]! @requireAuth
  }

  input CreateOpportunityIncumbentInput {
    opportunity_id_fk: String!
    display_name: String!
  }

  input UpdateOpportunityIncumbentInput {
    opportunity_id_fk: String
    display_name: String
  }
`
