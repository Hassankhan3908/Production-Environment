export const schema = gql`
  type InterestingActivityHub {
    id: String!
    task_id_fk: String
    event_id_fk: String
    moment_id_fk: String
    opportunity_id_fk: String!
    ui_opportunity: Opportunity!
  }

  type Query {
    interestingActivityHubs: [InterestingActivityHub!]! @requireAuth
  }

  input CreateInterestingActivityHubInput {
    task_id_fk: String
    event_id_fk: String
    moment_id_fk: String
    opportunity_id_fk: String!
  }

  input UpdateInterestingActivityHubInput {
    task_id_fk: String
    event_id_fk: String
    moment_id_fk: String
    opportunity_id_fk: String
  }
`
