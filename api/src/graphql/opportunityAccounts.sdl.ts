export const schema = gql`
  type OpportunityAccountsPaginated {
    opportunityAccounts: [OpportunityAccount!]!
    count: Int!
  }
  type OpportunityAccount {
    id: String!
    display_name: String
    effective_date: DateTime
    exp_date: DateTime
    heat_score: Int
    sf_id: String
    ui_account_attribs: [AccountAttributes]!
    ui_account_recommendations: [AccountRecommendations]!
    ui_opportunity: [Opportunity]!
  }

  type Query {
    opportunityAccounts: [OpportunityAccount!]! @requireAuth
    opportunityAccount(id: String!): OpportunityAccount @requireAuth
    filterOpportunityAccounts(
      page: Int
      sfId: String
    ): OpportunityAccountsPaginated! @requireAuth
  }

  input CreateOpportunityAccountInput {
    display_name: String
    effective_date: DateTime
    exp_date: DateTime
    heat_score: Int
    sf_id: String
  }

  input UpdateOpportunityAccountInput {
    display_name: String
    effective_date: DateTime
    exp_date: DateTime
    heat_score: Int
    sf_id: String
  }

  type Mutation {
    createOpportunityAccount(
      input: CreateOpportunityAccountInput!
    ): OpportunityAccount! @requireAuth
    updateOpportunityAccount(
      id: String!
      input: UpdateOpportunityAccountInput!
    ): OpportunityAccount! @requireAuth
    deleteOpportunityAccount(id: String!): OpportunityAccount! @requireAuth
  }
`
