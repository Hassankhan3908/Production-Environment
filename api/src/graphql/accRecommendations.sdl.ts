export const schema = gql`
  type AccountRecommendations {
    id: String!
    recommendation: String
    account_id_fk: String!
    ui_opportunity_account: OpportunityAccount!
  }

  type Query {
    accRecommendations: [AccountRecommendations!]! @requireAuth
  }

  input CreateAccountRecommendationsInput {
    recommendation: String
    account_id_fk: String!
  }

  input UpdateAccountRecommendationsInput {
    recommendation: String
    account_id_fk: String
  }
`
