export const schema = gql`
  type AccountAttributes {
    id: String!
    attrib_name: String
    attrib_value: String
    account_id_fk: String
    ui_opportunity_account: OpportunityAccount
  }

  type AccountAttributeValue {
    attrib_value: String
  }
  type Query {
    oppAccountAttributes: [AccountAttributes!]! @requireAuth
    accountTheaters: [AccountAttributeValue!]! @requireAuth
    accountAttributes(id: String!): AccountAttributes @requireAuth
  }

  input CreateAccountAttributesInput {
    attrib_name: String
    attrib_value: String
    account_id_fk: String
  }

  input UpdateAccountAttributesInput {
    attrib_name: String
    attrib_value: String
    account_id_fk: String
  }

  type Mutation {
    createAccountAttributes(
      input: CreateAccountAttributesInput!
    ): AccountAttributes! @requireAuth
    updateAccountAttributes(
      id: String!
      input: UpdateAccountAttributesInput!
    ): AccountAttributes! @requireAuth
    deleteAccountAttributes(id: String!): AccountAttributes! @requireAuth
  }
`
