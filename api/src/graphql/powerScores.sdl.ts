export const schema = gql`
  type PowerScore {
    id: String!
    type: String!
    name: String!
    description: String
    signature_id_fk: String!
    score: Float!
    effective_date: DateTime
    exp_date: DateTime
    ui_dna_signature: DnaSignature!
  }

  type Query {
    powerScores: [PowerScore!]! @requireAuth
    powerScore(id: String!): PowerScore @requireAuth
  }

  input CreatePowerScoreInput {
    type: String!
    name: String!
    description: String
    signature_id_fk: String!
    score: Float!
    effective_date: DateTime
    exp_date: DateTime
  }

  input UpdatePowerScoreInput {
    type: String
    name: String
    description: String
    signature_id_fk: String
    score: Float
    effective_date: DateTime
    exp_date: DateTime
  }

  type Mutation {
    createPowerScore(input: CreatePowerScoreInput!): PowerScore! @requireAuth
    updatePowerScore(id: String!, input: UpdatePowerScoreInput!): PowerScore!
      @requireAuth
    deletePowerScore(id: String!): PowerScore! @requireAuth
  }
`
