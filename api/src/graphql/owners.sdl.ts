export const schema = gql`
  type Owner {
    id: String!
    manager_id: String
    first_given_name: String!
    second_given_name: String
    family_name: String!
    sf_id: String
    manager_sf_id: String
    org_tree: JSON
    ui_owner: Owner
    ui_opportunity: [Opportunity]!
    other_ui_owner: [Owner]!
  }

  type Query {
    owners: [Owner!]! @requireAuth
    owner(id: String!): Owner @requireAuth
    ownerCount: Int! @requireAuth
  }

  input CreateOwnerInput {
    manager_id: String
    first_given_name: String!
    second_given_name: String
    family_name: String!
    sf_id: String
    manager_sf_id: String
    org_tree: JSON
  }

  input UpdateOwnerInput {
    manager_id: String
    first_given_name: String
    second_given_name: String
    family_name: String
    sf_id: String
    manager_sf_id: String
    org_tree: JSON
  }

  type Mutation {
    createOwner(input: CreateOwnerInput!): Owner! @requireAuth
    updateOwner(id: String!, input: UpdateOwnerInput!): Owner! @requireAuth
    deleteOwner(id: String!): Owner! @requireAuth
  }
`
