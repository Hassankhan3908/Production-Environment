export const schema = gql`
  type DnaSignatureStage {
    id: String!
    effective_date: DateTime
    exp_date: DateTime
    phase: String!
    display_name: String
    seq_order: Int!
    metric_value: String
    font_icon_desc: String
    signature_id_fk: String!
    ui_color: String
    ui_dna_signature: DnaSignature!
  }

  type Query {
    dnaSignatureStages: [DnaSignatureStage!]! @requireAuth
    dnaSignatureStage(id: String!): DnaSignatureStage @requireAuth
  }

  input CreateDnaSignatureStageInput {
    effective_date: DateTime
    exp_date: DateTime
    phase: String!
    display_name: String
    seq_order: Int!
    metric_value: String
    font_icon_desc: String
    signature_id_fk: String!
    ui_color: String
  }

  input UpdateDnaSignatureStageInput {
    effective_date: DateTime
    exp_date: DateTime
    phase: String
    display_name: String
    seq_order: Int
    metric_value: String
    font_icon_desc: String
    signature_id_fk: String
    ui_color: String
  }

  type Mutation {
    createDnaSignatureStage(
      input: CreateDnaSignatureStageInput!
    ): DnaSignatureStage! @requireAuth
    updateDnaSignatureStage(
      id: String!
      input: UpdateDnaSignatureStageInput!
    ): DnaSignatureStage! @requireAuth
    deleteDnaSignatureStage(id: String!): DnaSignatureStage! @requireAuth
  }
`
