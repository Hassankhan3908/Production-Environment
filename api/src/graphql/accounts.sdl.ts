export const schema = gql`
  type Account {
    id: String!
    display_name: String!
    effective_date: DateTime
    exp_date: DateTime
    #ui_authuser: [AuthUser]!
    ui_dna: [Dna]!
  }

  type Query {
    accounts: [Account!]! @requireAuth
    account(id: String!): Account @requireAuth
  }

  input CreateAccountInput {
    display_name: String!
    effective_date: DateTime
    exp_date: DateTime
  }

  input UpdateAccountInput {
    display_name: String
    effective_date: DateTime
    exp_date: DateTime
  }

  type Mutation {
    createAccount(input: CreateAccountInput!): Account! @requireAuth
    updateAccount(id: String!, input: UpdateAccountInput!): Account!
      @requireAuth
    deleteAccount(id: String!): Account! @requireAuth
  }
`
