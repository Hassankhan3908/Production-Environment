import {
  opportunityAccounts,
  opportunityAccount,
  createOpportunityAccount,
  updateOpportunityAccount,
  deleteOpportunityAccount,
} from './opportunityAccounts'
import type { StandardScenario } from './opportunityAccounts.scenarios'

describe('opportunityAccounts', () => {
  scenario(
    'returns all opportunityAccounts',
    async (scenario: StandardScenario) => {
      const result = await opportunityAccounts()

      expect(result.length).toEqual(
        Object.keys(scenario.opportunityAccount).length
      )
    }
  )

  scenario(
    'returns a single opportunityAccount',
    async (scenario: StandardScenario) => {
      const result = await opportunityAccount({
        id: scenario.opportunityAccount.one.id,
      })

      expect(result).toEqual(scenario.opportunityAccount.one)
    }
  )

  scenario(
    'deletes a opportunityAccount',
    async (scenario: StandardScenario) => {
      const original = await deleteOpportunityAccount({
        id: scenario.opportunityAccount.one.id,
      })
      const result = await opportunityAccount({ id: original.id })

      expect(result).toEqual(null)
    }
  )
})
