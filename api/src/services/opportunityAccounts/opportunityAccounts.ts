import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

const RECS_PER_PAGE = 8
export const opportunityAccounts = () => {
  return db.opportunityAccount.findMany({
    take: 10,
  })
}
export const filterOpportunityAccounts = ({
  page,
  sfId,
}: {
  page: number
  sfId?: string
}) => {
  const offset = (page - 1) * RECS_PER_PAGE
  const whereClause = sfId
    ? {
        sf_id: sfId,
      }
    : undefined
  return {
    opportunityAccounts: db.opportunityAccount.findMany({
      take: RECS_PER_PAGE,
      skip: offset,
      where: whereClause,
    }),
    count: db.opportunityAccount.count({
      where: whereClause,
    }),
  }
}

export const opportunityAccount = ({
  id,
}: Prisma.OpportunityAccountWhereUniqueInput) => {
  return db.opportunityAccount.findUnique({
    where: { id },
  })
}

interface CreateOpportunityAccountArgs {
  input: Prisma.OpportunityAccountCreateInput
}

export const createOpportunityAccount = ({
  input,
}: CreateOpportunityAccountArgs) => {
  return db.opportunityAccount.create({
    data: input,
  })
}

interface UpdateOpportunityAccountArgs
  extends Prisma.OpportunityAccountWhereUniqueInput {
  input: Prisma.OpportunityAccountUpdateInput
}

export const updateOpportunityAccount = ({
  id,
  input,
}: UpdateOpportunityAccountArgs) => {
  return db.opportunityAccount.update({
    data: input,
    where: { id },
  })
}

export const deleteOpportunityAccount = ({
  id,
}: Prisma.OpportunityAccountWhereUniqueInput) => {
  return db.opportunityAccount.delete({
    where: { id },
  })
}

export const OpportunityAccount = {
  ui_account_attribs: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof opportunityAccount>>
  ) =>
    db.opportunityAccount
      .findUnique({ where: { id: root.id } })
      .ui_account_attribs(),
  ui_account_recommendations: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof opportunityAccount>>
  ) =>
    db.opportunityAccount
      .findUnique({ where: { id: root.id } })
      .ui_account_recommendations(),
  ui_opportunity: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof opportunityAccount>>
  ) =>
    db.opportunityAccount
      .findUnique({ where: { id: root.id } })
      .ui_opportunity(),
}
