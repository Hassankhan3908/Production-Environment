import { db } from 'src/lib/db'

export const opportunityCompetitors = () => {
  return db.opportunityCompetitor.findMany()
}
