import { opportunityCompetitors } from './opportunityCompetitors'
import type { StandardScenario } from './opportunityCompetitors.scenarios'

describe('opportunityCompetitors', () => {
  scenario(
    'returns all opportunityCompetitors',
    async (scenario: StandardScenario) => {
      const result = await opportunityCompetitors()

      expect(result.length).toEqual(
        Object.keys(scenario.opportunityCompetitor).length
      )
    }
  )
})
