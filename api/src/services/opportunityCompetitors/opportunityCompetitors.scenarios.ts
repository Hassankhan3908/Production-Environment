import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.OpportunityCompetitorCreateArgs>({
  opportunityCompetitor: {
    one: {
      data: {
        display_name: 'String',
        ui_opportunity: {
          create: {
            ui_opportunity_account: { create: {} },
            ui_owner: {
              create: { first_given_name: 'String', family_name: 'String' },
            },
            ui_dna_signature: {
              create: {
                ui_order: 7591177,
                ui_color: 'String',
                ui_dna: {
                  create: {
                    ui_account: { create: { display_name: 'String1225361' } },
                  },
                },
              },
            },
          },
        },
      },
    },
    two: {
      data: {
        display_name: 'String',
        ui_opportunity: {
          create: {
            ui_opportunity_account: { create: {} },
            ui_owner: {
              create: { first_given_name: 'String', family_name: 'String' },
            },
            ui_dna_signature: {
              create: {
                ui_order: 5033169,
                ui_color: 'String',
                ui_dna: {
                  create: {
                    ui_account: { create: { display_name: 'String928053' } },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
})

export type StandardScenario = typeof standard
