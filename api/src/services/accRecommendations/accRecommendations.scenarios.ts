import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.AccountRecommendationsCreateArgs>(
  {
    accountRecommendations: {
      one: { data: { ui_opportunity_account: { create: {} } } },
      two: { data: { ui_opportunity_account: { create: {} } } },
    },
  }
)

export type StandardScenario = typeof standard
