import { db } from 'src/lib/db'

export const accRecommendations = () => {
  return db.accountRecommendations.findMany()
}
