import { accRecommendations } from './accRecommendations'
import type { StandardScenario } from './accRecommendations.scenarios'

describe('accRecommendations', () => {
  scenario(
    'returns all accRecommendations',
    async (scenario: StandardScenario) => {
      const result = await accRecommendations()

      expect(result.length).toEqual(
        Object.keys(scenario.accountRecommendations).length
      )
    }
  )
})
