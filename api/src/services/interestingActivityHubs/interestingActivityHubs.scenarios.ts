import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.InterestingActivityHubCreateArgs>(
  {
    interestingActivityHub: {
      one: {
        data: {
          ui_opportunity: {
            create: {
              ui_opportunity_account: { create: {} },
              ui_owner: {
                create: { first_given_name: 'String', family_name: 'String' },
              },
              ui_dna_signature: {
                create: {
                  ui_order: 726145,
                  ui_color: 'String',
                  ui_dna: {
                    create: {
                      ui_account: { create: { display_name: 'String3810512' } },
                    },
                  },
                },
              },
            },
          },
        },
      },
      two: {
        data: {
          ui_opportunity: {
            create: {
              ui_opportunity_account: { create: {} },
              ui_owner: {
                create: { first_given_name: 'String', family_name: 'String' },
              },
              ui_dna_signature: {
                create: {
                  ui_order: 1269499,
                  ui_color: 'String',
                  ui_dna: {
                    create: {
                      ui_account: { create: { display_name: 'String5658816' } },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  }
)

export type StandardScenario = typeof standard
