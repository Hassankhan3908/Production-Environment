import { db } from 'src/lib/db'

export const interestingActivityHubs = () => {
  return db.interestingActivityHub.findMany()
}
