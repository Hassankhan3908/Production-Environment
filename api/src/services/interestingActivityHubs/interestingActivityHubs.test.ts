import { interestingActivityHubs } from './interestingActivityHubs'
import type { StandardScenario } from './interestingActivityHubs.scenarios'

describe('interestingActivityHubs', () => {
  scenario(
    'returns all interestingActivityHubs',
    async (scenario: StandardScenario) => {
      const result = await interestingActivityHubs()

      expect(result.length).toEqual(
        Object.keys(scenario.interestingActivityHub).length
      )
    }
  )
})
