import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

export const powerScores = () => {
  return db.powerScore.findMany()
}

export const powerScore = ({ id }: Prisma.PowerScoreWhereUniqueInput) => {
  return db.powerScore.findUnique({
    where: { id },
  })
}

interface CreatePowerScoreArgs {
  input: Prisma.PowerScoreCreateInput
}

export const createPowerScore = ({ input }: CreatePowerScoreArgs) => {
  return db.powerScore.create({
    data: input,
  })
}

interface UpdatePowerScoreArgs extends Prisma.PowerScoreWhereUniqueInput {
  input: Prisma.PowerScoreUpdateInput
}

export const updatePowerScore = ({ id, input }: UpdatePowerScoreArgs) => {
  return db.powerScore.update({
    data: input,
    where: { id },
  })
}

export const deletePowerScore = ({ id }: Prisma.PowerScoreWhereUniqueInput) => {
  return db.powerScore.delete({
    where: { id },
  })
}

export const PowerScore = {
  ui_dna_signature: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof powerScore>>
  ) => db.powerScore.findUnique({ where: { id: root.id } }).ui_dna_signature(),
}
