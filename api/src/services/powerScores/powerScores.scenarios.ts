import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.PowerScoreCreateArgs>({
  powerScore: {
    one: {
      data: {
        type: 'String',
        name: 'String',
        score: 4010638.827288877,
        ui_dna_signature: {
          create: {
            ui_order: 9608389,
            ui_color: 'String',
            ui_dna: {
              create: {
                ui_account: { create: { display_name: 'String8957860' } },
              },
            },
          },
        },
      },
    },
    two: {
      data: {
        type: 'String',
        name: 'String',
        score: 809371.6355374192,
        ui_dna_signature: {
          create: {
            ui_order: 1918179,
            ui_color: 'String',
            ui_dna: {
              create: {
                ui_account: { create: { display_name: 'String6923811' } },
              },
            },
          },
        },
      },
    },
  },
})

export type StandardScenario = typeof standard
