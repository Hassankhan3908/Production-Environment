import {
  powerScores,
  powerScore,
  createPowerScore,
  updatePowerScore,
  deletePowerScore,
} from './powerScores'
import type { StandardScenario } from './powerScores.scenarios'

describe('powerScores', () => {
  scenario('returns all powerScores', async (scenario: StandardScenario) => {
    const result = await powerScores()

    expect(result.length).toEqual(Object.keys(scenario.powerScore).length)
  })

  scenario(
    'returns a single powerScore',
    async (scenario: StandardScenario) => {
      const result = await powerScore({ id: scenario.powerScore.one.id })

      expect(result).toEqual(scenario.powerScore.one)
    }
  )

  scenario('creates a powerScore', async (scenario: StandardScenario) => {
    const result = await createPowerScore({
      input: {
        type: 'String',
        name: 'String',
        signature_id_fk: scenario.powerScore.two.signature_id_fk,
        score: 2879365.1502471706,
      },
    })

    expect(result.type).toEqual('String')
    expect(result.name).toEqual('String')
    expect(result.signature_id_fk).toEqual(
      scenario.powerScore.two.signature_id_fk
    )
    expect(result.score).toEqual(2879365.1502471706)
  })

  scenario('updates a powerScore', async (scenario: StandardScenario) => {
    const original = await powerScore({ id: scenario.powerScore.one.id })
    const result = await updatePowerScore({
      id: original.id,
      input: { type: 'String2' },
    })

    expect(result.type).toEqual('String2')
  })

  scenario('deletes a powerScore', async (scenario: StandardScenario) => {
    const original = await deletePowerScore({ id: scenario.powerScore.one.id })
    const result = await powerScore({ id: original.id })

    expect(result).toEqual(null)
  })
})
