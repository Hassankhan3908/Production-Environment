import {
  dnaSignatureStages,
  dnaSignatureStage,
  createDnaSignatureStage,
  updateDnaSignatureStage,
  deleteDnaSignatureStage,
} from './dnaSignatureStages'
import type { StandardScenario } from './dnaSignatureStages.scenarios'

describe('dnaSignatureStages', () => {
  scenario(
    'returns all dnaSignatureStages',
    async (scenario: StandardScenario) => {
      const result = await dnaSignatureStages()

      expect(result.length).toEqual(
        Object.keys(scenario.dnaSignatureStage).length
      )
    }
  )

  scenario(
    'returns a single dnaSignatureStage',
    async (scenario: StandardScenario) => {
      const result = await dnaSignatureStage({
        id: scenario.dnaSignatureStage.one.id,
      })

      expect(result).toEqual(scenario.dnaSignatureStage.one)
    }
  )

  scenario(
    'creates a dnaSignatureStage',
    async (scenario: StandardScenario) => {
      const result = await createDnaSignatureStage({
        input: {
          phase: 'String',
          seq_order: 4189456,
          signature_id_fk: scenario.dnaSignatureStage.two.signature_id_fk,
        },
      })

      expect(result.phase).toEqual('String')
      expect(result.seq_order).toEqual(4189456)
      expect(result.signature_id_fk).toEqual(
        scenario.dnaSignatureStage.two.signature_id_fk
      )
    }
  )

  scenario(
    'updates a dnaSignatureStage',
    async (scenario: StandardScenario) => {
      const original = await dnaSignatureStage({
        id: scenario.dnaSignatureStage.one.id,
      })
      const result = await updateDnaSignatureStage({
        id: original.id,
        input: { phase: 'String2' },
      })

      expect(result.phase).toEqual('String2')
    }
  )

  scenario(
    'deletes a dnaSignatureStage',
    async (scenario: StandardScenario) => {
      const original = await deleteDnaSignatureStage({
        id: scenario.dnaSignatureStage.one.id,
      })
      const result = await dnaSignatureStage({ id: original.id })

      expect(result).toEqual(null)
    }
  )
})
