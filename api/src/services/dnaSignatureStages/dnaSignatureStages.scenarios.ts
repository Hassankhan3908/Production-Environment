import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.DnaSignatureStageCreateArgs>({
  dnaSignatureStage: {
    one: {
      data: {
        phase: 'String',
        seq_order: 8807016,
        ui_dna_signature: {
          create: {
            ui_order: 5937856,
            ui_color: 'String',
            ui_dna: {
              create: {
                ui_account: { create: { display_name: 'String5343662' } },
              },
            },
          },
        },
      },
    },
    two: {
      data: {
        phase: 'String',
        seq_order: 5870089,
        ui_dna_signature: {
          create: {
            ui_order: 3519514,
            ui_color: 'String',
            ui_dna: {
              create: {
                ui_account: { create: { display_name: 'String7414312' } },
              },
            },
          },
        },
      },
    },
  },
})

export type StandardScenario = typeof standard
