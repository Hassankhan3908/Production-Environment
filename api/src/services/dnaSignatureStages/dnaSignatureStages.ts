import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

export const dnaSignatureStages = () => {
  return db.dnaSignatureStage.findMany()
}

export const dnaSignatureStage = ({
  id,
}: Prisma.DnaSignatureStageWhereUniqueInput) => {
  return db.dnaSignatureStage.findUnique({
    where: { id },
  })
}

interface CreateDnaSignatureStageArgs {
  input: Prisma.DnaSignatureStageCreateInput
}

export const createDnaSignatureStage = ({
  input,
}: CreateDnaSignatureStageArgs) => {
  return db.dnaSignatureStage.create({
    data: input,
  })
}

interface UpdateDnaSignatureStageArgs
  extends Prisma.DnaSignatureStageWhereUniqueInput {
  input: Prisma.DnaSignatureStageUpdateInput
}

export const updateDnaSignatureStage = ({
  id,
  input,
}: UpdateDnaSignatureStageArgs) => {
  return db.dnaSignatureStage.update({
    data: input,
    where: { id },
  })
}

export const deleteDnaSignatureStage = ({
  id,
}: Prisma.DnaSignatureStageWhereUniqueInput) => {
  return db.dnaSignatureStage.delete({
    where: { id },
  })
}

export const DnaSignatureStage = {
  ui_dna_signature: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof dnaSignatureStage>>
  ) =>
    db.dnaSignatureStage
      .findUnique({ where: { id: root.id } })
      .ui_dna_signature(),
}
