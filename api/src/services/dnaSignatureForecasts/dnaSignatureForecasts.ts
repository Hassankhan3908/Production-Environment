import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

export const dnaSignatureForecasts = () => {
  return db.dnaSignatureForecast.findMany()
}

export const dnaSignatureForecast = ({
  id,
}: Prisma.DnaSignatureForecastWhereUniqueInput) => {
  return db.dnaSignatureForecast.findUnique({
    where: { id },
  })
}

interface CreateDnaSignatureForecastArgs {
  input: Prisma.DnaSignatureForecastCreateInput
}

export const createDnaSignatureForecast = ({
  input,
}: CreateDnaSignatureForecastArgs) => {
  return db.dnaSignatureForecast.create({
    data: input,
  })
}

interface UpdateDnaSignatureForecastArgs
  extends Prisma.DnaSignatureForecastWhereUniqueInput {
  input: Prisma.DnaSignatureForecastUpdateInput
}

export const updateDnaSignatureForecast = ({
  id,
  input,
}: UpdateDnaSignatureForecastArgs) => {
  return db.dnaSignatureForecast.update({
    data: input,
    where: { id },
  })
}

export const deleteDnaSignatureForecast = ({
  id,
}: Prisma.DnaSignatureForecastWhereUniqueInput) => {
  return db.dnaSignatureForecast.delete({
    where: { id },
  })
}

export const DnaSignatureForecast = {
  ui_dna_signature: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof dnaSignatureForecast>>
  ) =>
    db.dnaSignatureForecast
      .findUnique({ where: { id: root.id } })
      .ui_dna_signature(),
}
