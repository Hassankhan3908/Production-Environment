import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.DnaSignatureForecastCreateArgs>({
  dnaSignatureForecast: {
    one: {
      data: {
        rep_id_fk: 'String',
        rep_mgr_id_fk: 'String',
        ui_dna_signature: {
          create: {
            ui_order: 3042204,
            ui_color: 'String',
            ui_dna: {
              create: {
                ui_account: { create: { display_name: 'String7541780' } },
              },
            },
          },
        },
      },
    },
    two: {
      data: {
        rep_id_fk: 'String',
        rep_mgr_id_fk: 'String',
        ui_dna_signature: {
          create: {
            ui_order: 8424594,
            ui_color: 'String',
            ui_dna: {
              create: {
                ui_account: { create: { display_name: 'String2659075' } },
              },
            },
          },
        },
      },
    },
  },
})

export type StandardScenario = typeof standard
