import {
  dnaSignatureForecasts,
  dnaSignatureForecast,
  createDnaSignatureForecast,
  updateDnaSignatureForecast,
  deleteDnaSignatureForecast,
} from './dnaSignatureForecasts'
import type { StandardScenario } from './dnaSignatureForecasts.scenarios'

describe('dnaSignatureForecasts', () => {
  scenario(
    'returns all dnaSignatureForecasts',
    async (scenario: StandardScenario) => {
      const result = await dnaSignatureForecasts()

      expect(result.length).toEqual(
        Object.keys(scenario.dnaSignatureForecast).length
      )
    }
  )

  scenario(
    'returns a single dnaSignatureForecast',
    async (scenario: StandardScenario) => {
      const result = await dnaSignatureForecast({
        id: scenario.dnaSignatureForecast.one.id,
      })

      expect(result).toEqual(scenario.dnaSignatureForecast.one)
    }
  )

  scenario(
    'creates a dnaSignatureForecast',
    async (scenario: StandardScenario) => {
      const result = await createDnaSignatureForecast({
        input: {
          signature_id_fk: scenario.dnaSignatureForecast.two.signature_id_fk,
          rep_id_fk: 'String',
          rep_mgr_id_fk: 'String',
        },
      })

      expect(result.signature_id_fk).toEqual(
        scenario.dnaSignatureForecast.two.signature_id_fk
      )
      expect(result.rep_id_fk).toEqual('String')
      expect(result.rep_mgr_id_fk).toEqual('String')
    }
  )

  scenario(
    'updates a dnaSignatureForecast',
    async (scenario: StandardScenario) => {
      const original = await dnaSignatureForecast({
        id: scenario.dnaSignatureForecast.one.id,
      })
      const result = await updateDnaSignatureForecast({
        id: original.id,
        input: { rep_id_fk: 'String2' },
      })

      expect(result.rep_id_fk).toEqual('String2')
    }
  )

  scenario(
    'deletes a dnaSignatureForecast',
    async (scenario: StandardScenario) => {
      const original = await deleteDnaSignatureForecast({
        id: scenario.dnaSignatureForecast.one.id,
      })
      const result = await dnaSignatureForecast({ id: original.id })

      expect(result).toEqual(null)
    }
  )
})
