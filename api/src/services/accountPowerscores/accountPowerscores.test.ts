import {
  accountPowerscores,
  accountPowerscore,
  createAccountPowerscore,
  updateAccountPowerscore,
  deleteAccountPowerscore,
} from './accountPowerscores'
import type { StandardScenario } from './accountPowerscores.scenarios'

describe('accountPowerscores', () => {
  scenario(
    'returns all accountPowerscores',
    async (scenario: StandardScenario) => {
      const result = await accountPowerscores()

      expect(result.length).toEqual(
        Object.keys(scenario.accountPowerscore).length
      )
    }
  )

  scenario(
    'returns a single accountPowerscore',
    async (scenario: StandardScenario) => {
      const result = await accountPowerscore({
        id: scenario.accountPowerscore.one.id,
      })

      expect(result).toEqual(scenario.accountPowerscore.one)
    }
  )

  scenario(
    'creates a accountPowerscore',
    async (scenario: StandardScenario) => {
      const result = await createAccountPowerscore({
        input: {
          type: 'String',
          name: 'String',
          dna_id_fk: scenario.accountPowerscore.two.dna_id_fk,
          score: 2003756.8082522373,
        },
      })

      expect(result.type).toEqual('String')
      expect(result.name).toEqual('String')
      expect(result.dna_id_fk).toEqual(scenario.accountPowerscore.two.dna_id_fk)
      expect(result.score).toEqual(2003756.8082522373)
    }
  )

  scenario(
    'updates a accountPowerscore',
    async (scenario: StandardScenario) => {
      const original = await accountPowerscore({
        id: scenario.accountPowerscore.one.id,
      })
      const result = await updateAccountPowerscore({
        id: original.id,
        input: { type: 'String2' },
      })

      expect(result.type).toEqual('String2')
    }
  )

  scenario(
    'deletes a accountPowerscore',
    async (scenario: StandardScenario) => {
      const original = await deleteAccountPowerscore({
        id: scenario.accountPowerscore.one.id,
      })
      const result = await accountPowerscore({ id: original.id })

      expect(result).toEqual(null)
    }
  )
})
