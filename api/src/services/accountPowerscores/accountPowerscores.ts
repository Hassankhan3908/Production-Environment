import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

export const accountPowerscores = () => {
  return db.accountPowerscore.findMany()
}

export const accountPowerscore = ({
  id,
}: Prisma.AccountPowerscoreWhereUniqueInput) => {
  return db.accountPowerscore.findUnique({
    where: { id },
  })
}

interface CreateAccountPowerscoreArgs {
  input: Prisma.AccountPowerscoreCreateInput
}

export const createAccountPowerscore = ({
  input,
}: CreateAccountPowerscoreArgs) => {
  return db.accountPowerscore.create({
    data: input,
  })
}

interface UpdateAccountPowerscoreArgs
  extends Prisma.AccountPowerscoreWhereUniqueInput {
  input: Prisma.AccountPowerscoreUpdateInput
}

export const updateAccountPowerscore = ({
  id,
  input,
}: UpdateAccountPowerscoreArgs) => {
  return db.accountPowerscore.update({
    data: input,
    where: { id },
  })
}

export const deleteAccountPowerscore = ({
  id,
}: Prisma.AccountPowerscoreWhereUniqueInput) => {
  return db.accountPowerscore.delete({
    where: { id },
  })
}

export const AccountPowerscore = {
  ui_dna: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof accountPowerscore>>
  ) => db.accountPowerscore.findUnique({ where: { id: root.id } }).ui_dna(),
}
