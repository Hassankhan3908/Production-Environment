import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.AccountPowerscoreCreateArgs>({
  accountPowerscore: {
    one: {
      data: {
        type: 'String',
        name: 'String',
        score: 2076744.9717641217,
        ui_dna: {
          create: { ui_account: { create: { display_name: 'String4169221' } } },
        },
      },
    },
    two: {
      data: {
        type: 'String',
        name: 'String',
        score: 3330142.441380781,
        ui_dna: {
          create: { ui_account: { create: { display_name: 'String2439037' } } },
        },
      },
    },
  },
})

export type StandardScenario = typeof standard
