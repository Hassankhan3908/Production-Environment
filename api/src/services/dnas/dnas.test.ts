import { dnas, dna, createDna, updateDna, deleteDna } from './dnas'
import type { StandardScenario } from './dnas.scenarios'

describe('dnas', () => {
  scenario('returns all dnas', async (scenario: StandardScenario) => {
    const result = await dnas()

    expect(result.length).toEqual(Object.keys(scenario.dna).length)
  })

  scenario('returns a single dna', async (scenario: StandardScenario) => {
    const result = await dna({ id: scenario.dna.one.id })

    expect(result).toEqual(scenario.dna.one)
  })

  scenario('creates a dna', async (scenario: StandardScenario) => {
    const result = await createDna({
      input: { account_id_fk: scenario.dna.two.account_id_fk },
    })

    expect(result.account_id_fk).toEqual(scenario.dna.two.account_id_fk)
  })

  scenario('updates a dna', async (scenario: StandardScenario) => {
    const original = await dna({ id: scenario.dna.one.id })
    const result = await updateDna({
      id: original.id,
      input: { account_id_fk: scenario.dna.two.account_id_fk },
    })

    expect(result.account_id_fk).toEqual(scenario.dna.two.account_id_fk)
  })

  scenario('deletes a dna', async (scenario: StandardScenario) => {
    const original = await deleteDna({ id: scenario.dna.one.id })
    const result = await dna({ id: original.id })

    expect(result).toEqual(null)
  })
})
