import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.DnaCreateArgs>({
  dna: {
    one: {
      data: { ui_account: { create: { display_name: 'String3578276' } } },
    },
    two: {
      data: { ui_account: { create: { display_name: 'String5450455' } } },
    },
  },
})

export type StandardScenario = typeof standard
