import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

export const dnas = () => {
  return db.dna.findMany()
}

export const dna = ({ id }: Prisma.DnaWhereUniqueInput) => {
  return db.dna.findUnique({
    where: { id },
  })
}

interface CreateDnaArgs {
  input: Prisma.DnaCreateInput
}

export const createDna = ({ input }: CreateDnaArgs) => {
  return db.dna.create({
    data: input,
  })
}

interface UpdateDnaArgs extends Prisma.DnaWhereUniqueInput {
  input: Prisma.DnaUpdateInput
}

export const updateDna = ({ id, input }: UpdateDnaArgs) => {
  return db.dna.update({
    data: input,
    where: { id },
  })
}

export const deleteDna = ({ id }: Prisma.DnaWhereUniqueInput) => {
  return db.dna.delete({
    where: { id },
  })
}

export const Dna = {
  ui_account: (_obj, { root }: ResolverArgs<ReturnType<typeof dna>>) =>
    db.dna.findUnique({ where: { id: root.id } }).ui_account(),
  ui_account_powerscore: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof dna>>
  ) => db.dna.findUnique({ where: { id: root.id } }).ui_account_powerscore(),
  ui_dna_signature: (_obj, { root }: ResolverArgs<ReturnType<typeof dna>>) =>
    db.dna.findUnique({ where: { id: root.id } }).ui_dna_signature(),
}
