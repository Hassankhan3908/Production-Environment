import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.OwnerCreateArgs>({
  owner: {
    one: { data: { first_given_name: 'String', family_name: 'String' } },
    two: { data: { first_given_name: 'String', family_name: 'String' } },
  },
})

export type StandardScenario = typeof standard
