import { owners, owner, createOwner, updateOwner, deleteOwner } from './owners'
import type { StandardScenario } from './owners.scenarios'

describe('owners', () => {
  scenario('returns all owners', async (scenario: StandardScenario) => {
    const result = await owners()

    expect(result.length).toEqual(Object.keys(scenario.owner).length)
  })

  scenario('returns a single owner', async (scenario: StandardScenario) => {
    const result = await owner({ id: scenario.owner.one.id })

    expect(result).toEqual(scenario.owner.one)
  })

  scenario('creates a owner', async () => {
    const result = await createOwner({
      input: { first_given_name: 'String', family_name: 'String' },
    })

    expect(result.first_given_name).toEqual('String')
    expect(result.family_name).toEqual('String')
  })

  scenario('updates a owner', async (scenario: StandardScenario) => {
    const original = await owner({ id: scenario.owner.one.id })
    const result = await updateOwner({
      id: original.id,
      input: { first_given_name: 'String2' },
    })

    expect(result.first_given_name).toEqual('String2')
  })

  scenario('deletes a owner', async (scenario: StandardScenario) => {
    const original = await deleteOwner({ id: scenario.owner.one.id })
    const result = await owner({ id: original.id })

    expect(result).toEqual(null)
  })
})
