import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

export const owners = () => {
  return db.owner.findMany({
    where: {
      ui_opportunity: {
        some: {
          NOT: {
            id: undefined,
          },
        },
      },
    },
  })
}

export const owner = ({ id }: Prisma.OwnerWhereUniqueInput) => {
  return db.owner.findFirst({
    where: { sf_id: id },
  })
}

export const ownerCount = () => {
  return db.owner.count({
    where: {
      ui_opportunity: {
        some: {
          NOT: {
            id: undefined,
          },
        },
      },
    },
  })
}

interface CreateOwnerArgs {
  input: Prisma.OwnerCreateInput
}

export const createOwner = ({ input }: CreateOwnerArgs) => {
  return db.owner.create({
    data: input,
  })
}

interface UpdateOwnerArgs extends Prisma.OwnerWhereUniqueInput {
  input: Prisma.OwnerUpdateInput
}

export const updateOwner = ({ id, input }: UpdateOwnerArgs) => {
  return db.owner.update({
    data: input,
    where: { id },
  })
}

export const deleteOwner = ({ id }: Prisma.OwnerWhereUniqueInput) => {
  return db.owner.delete({
    where: { id },
  })
}

export const Owner = {
  ui_owner: (_obj, { root }: ResolverArgs<ReturnType<typeof owner>>) =>
    db.owner.findUnique({ where: { id: root.id } }).ui_owner(),
  ui_opportunity: (_obj, { root }: ResolverArgs<ReturnType<typeof owner>>) =>
    db.owner.findUnique({ where: { id: root.id } }).ui_opportunity(),
  other_ui_owner: (_obj, { root }: ResolverArgs<ReturnType<typeof owner>>) =>
    db.owner.findUnique({ where: { id: root.id } }).other_ui_owner(),
}
