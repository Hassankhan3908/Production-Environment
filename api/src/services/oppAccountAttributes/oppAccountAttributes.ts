import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

export const oppAccountAttributes = () => {
  return db.accountAttributes.findMany()
}

export const accountTheaters = () => {
  return db.accountAttributes.findMany({
    where: {
      attrib_name: 'theater',
      NOT: {
        attrib_value: null,
      },
    },
    distinct: ['attrib_value'],
    select: {
      attrib_value: true,
    },
  })
}

export const accountAttributes = ({
  id,
}: Prisma.AccountAttributesWhereUniqueInput) => {
  return db.accountAttributes.findUnique({
    where: { id },
  })
}

interface CreateAccountAttributesArgs {
  input: Prisma.AccountAttributesCreateInput
}

export const createAccountAttributes = ({
  input,
}: CreateAccountAttributesArgs) => {
  return db.accountAttributes.create({
    data: input,
  })
}

interface UpdateAccountAttributesArgs
  extends Prisma.AccountAttributesWhereUniqueInput {
  input: Prisma.AccountAttributesUpdateInput
}

export const updateAccountAttributes = ({
  id,
  input,
}: UpdateAccountAttributesArgs) => {
  return db.accountAttributes.update({
    data: input,
    where: { id },
  })
}

export const deleteAccountAttributes = ({
  id,
}: Prisma.AccountAttributesWhereUniqueInput) => {
  return db.accountAttributes.delete({
    where: { id },
  })
}

export const AccountAttributes = {
  ui_opportunity_account: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof accountAttributes>>
  ) =>
    db.accountAttributes
      .findUnique({ where: { id: root.id } })
      .ui_opportunity_account(),
}
