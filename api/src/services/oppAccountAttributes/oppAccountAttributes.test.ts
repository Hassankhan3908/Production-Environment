import {
  oppAccountAttributes,
  accountAttributes,
  createAccountAttributes,
  updateAccountAttributes,
  deleteAccountAttributes,
} from './oppAccountAttributes'
import type { StandardScenario } from './oppAccountAttributes.scenarios'

describe('oppAccountAttributes', () => {
  scenario(
    'returns all oppAccountAttributes',
    async (scenario: StandardScenario) => {
      const result = await oppAccountAttributes()

      expect(result.length).toEqual(
        Object.keys(scenario.accountAttributes).length
      )
    }
  )

  scenario(
    'returns a single accountAttributes',
    async (scenario: StandardScenario) => {
      const result = await accountAttributes({
        id: scenario.accountAttributes.one.id,
      })

      expect(result).toEqual(scenario.accountAttributes.one)
    }
  )

  scenario(
    'deletes a accountAttributes',
    async (scenario: StandardScenario) => {
      const original = await deleteAccountAttributes({
        id: scenario.accountAttributes.one.id,
      })
      const result = await accountAttributes({ id: original.id })

      expect(result).toEqual(null)
    }
  )
})
