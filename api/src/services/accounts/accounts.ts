import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

export const accounts = () => {
  return db.account.findMany()
}

export const account = ({ id }: Prisma.AccountWhereUniqueInput) => {
  return db.account.findUnique({
    where: { id },
  })
}

interface CreateAccountArgs {
  input: Prisma.AccountCreateInput
}

export const createAccount = ({ input }: CreateAccountArgs) => {
  return db.account.create({
    data: input,
  })
}

interface UpdateAccountArgs extends Prisma.AccountWhereUniqueInput {
  input: Prisma.AccountUpdateInput
}

export const updateAccount = ({ id, input }: UpdateAccountArgs) => {
  return db.account.update({
    data: input,
    where: { id },
  })
}

export const deleteAccount = ({ id }: Prisma.AccountWhereUniqueInput) => {
  return db.account.delete({
    where: { id },
  })
}

export const Account = {
  ui_authuser: (_obj, { root }: ResolverArgs<ReturnType<typeof account>>) =>
    db.account.findUnique({ where: { id: root.id } }).ui_authuser(),
  ui_dna: (_obj, { root }: ResolverArgs<ReturnType<typeof account>>) =>
    db.account.findUnique({ where: { id: root.id } }).ui_dna(),
}
