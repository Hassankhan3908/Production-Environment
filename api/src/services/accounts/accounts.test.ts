import {
  accounts,
  account,
  createAccount,
  updateAccount,
  deleteAccount,
} from './accounts'
import type { StandardScenario } from './accounts.scenarios'

describe('accounts', () => {
  scenario('returns all accounts', async (scenario: StandardScenario) => {
    const result = await accounts()

    expect(result.length).toEqual(Object.keys(scenario.account).length)
  })

  scenario('returns a single account', async (scenario: StandardScenario) => {
    const result = await account({ id: scenario.account.one.id })

    expect(result).toEqual(scenario.account.one)
  })

  scenario('creates a account', async () => {
    const result = await createAccount({
      input: { display_name: 'String3242183' },
    })

    expect(result.display_name).toEqual('String3242183')
  })

  scenario('updates a account', async (scenario: StandardScenario) => {
    const original = await account({ id: scenario.account.one.id })
    const result = await updateAccount({
      id: original.id,
      input: { display_name: 'String63361532' },
    })

    expect(result.display_name).toEqual('String63361532')
  })

  scenario('deletes a account', async (scenario: StandardScenario) => {
    const original = await deleteAccount({ id: scenario.account.one.id })
    const result = await account({ id: original.id })

    expect(result).toEqual(null)
  })
})
