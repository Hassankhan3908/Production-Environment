import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.AccountCreateArgs>({
  account: {
    one: { data: { display_name: 'String6013023' } },
    two: { data: { display_name: 'String5332875' } },
  },
})

export type StandardScenario = typeof standard
