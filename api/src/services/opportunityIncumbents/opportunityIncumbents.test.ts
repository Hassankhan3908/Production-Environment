import { opportunityIncumbents } from './opportunityIncumbents'
import type { StandardScenario } from './opportunityIncumbents.scenarios'

describe('opportunityIncumbents', () => {
  scenario(
    'returns all opportunityIncumbents',
    async (scenario: StandardScenario) => {
      const result = await opportunityIncumbents()

      expect(result.length).toEqual(
        Object.keys(scenario.opportunityIncumbent).length
      )
    }
  )
})
