import { db } from 'src/lib/db'

export const opportunityIncumbents = () => {
  return db.opportunityIncumbent.findMany()
}
