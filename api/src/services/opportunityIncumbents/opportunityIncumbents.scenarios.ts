import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.OpportunityIncumbentCreateArgs>({
  opportunityIncumbent: {
    one: {
      data: {
        display_name: 'String',
        ui_opportunity: {
          create: {
            ui_opportunity_account: { create: {} },
            ui_owner: {
              create: { first_given_name: 'String', family_name: 'String' },
            },
            ui_dna_signature: {
              create: {
                ui_order: 5168305,
                ui_color: 'String',
                ui_dna: {
                  create: {
                    ui_account: { create: { display_name: 'String3529286' } },
                  },
                },
              },
            },
          },
        },
      },
    },
    two: {
      data: {
        display_name: 'String',
        ui_opportunity: {
          create: {
            ui_opportunity_account: { create: {} },
            ui_owner: {
              create: { first_given_name: 'String', family_name: 'String' },
            },
            ui_dna_signature: {
              create: {
                ui_order: 25355,
                ui_color: 'String',
                ui_dna: {
                  create: {
                    ui_account: { create: { display_name: 'String3289790' } },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
})

export type StandardScenario = typeof standard
