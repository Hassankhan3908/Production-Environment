import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

const RECS_PER_PAGE = 8

export const opportunities = () => {
  return db.opportunity.findMany({
    take: RECS_PER_PAGE,
  })
}

export const filterOpportunities = ({
  page,
  sfId,
  theater,
}: {
  page: number
  sfId?: string
  theater?: string
}) => {
  const offset = (page - 1) * RECS_PER_PAGE
  const whereClause = {
    ui_opportunity_account: theater
      ? {
          ui_account_attribs: {
            some: {
              attrib_name: 'theater',
              attrib_value: theater,
            },
          },
        }
      : undefined,
    ui_owner: sfId
      ? {
          sf_id: sfId,
        }
      : undefined,
  }
  return {
    opportunities: db.opportunity.findMany({
      take: RECS_PER_PAGE,
      skip: offset,
      where: whereClause,
    }),
    count: db.opportunity.count({
      where: whereClause,
    }),
  }
}

export const opportunity = ({ id }: Prisma.OpportunityWhereUniqueInput) => {
  return db.opportunity.findUnique({
    where: { id },
  })
}

interface CreateOpportunityArgs {
  input: Prisma.OpportunityCreateInput
}

export const createOpportunity = ({ input }: CreateOpportunityArgs) => {
  return db.opportunity.create({
    data: input,
  })
}

interface UpdateOpportunityArgs extends Prisma.OpportunityWhereUniqueInput {
  input: Prisma.OpportunityUpdateInput
}

export const updateOpportunity = ({ id, input }: UpdateOpportunityArgs) => {
  return db.opportunity.update({
    data: input,
    where: { id },
  })
}

export const deleteOpportunity = ({
  id,
}: Prisma.OpportunityWhereUniqueInput) => {
  return db.opportunity.delete({
    where: { id },
  })
}

export const Opportunity = {
  ui_opportunity_account: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof opportunity>>
  ) =>
    db.opportunity
      .findUnique({ where: { id: root.id } })
      .ui_opportunity_account(),
  ui_owner: (_obj, { root }: ResolverArgs<ReturnType<typeof opportunity>>) =>
    db.opportunity.findUnique({ where: { id: root.id } }).ui_owner(),
  ui_dna_signature: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof opportunity>>
  ) => db.opportunity.findUnique({ where: { id: root.id } }).ui_dna_signature(),
  ui_contact: (_obj, { root }: ResolverArgs<ReturnType<typeof opportunity>>) =>
    db.opportunity.findUnique({ where: { id: root.id } }).ui_contact(),
  ui_event: (_obj, { root }: ResolverArgs<ReturnType<typeof opportunity>>) =>
    db.opportunity.findUnique({ where: { id: root.id } }).ui_event(),
  ui_interesting_activity_hub: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof opportunity>>
  ) =>
    db.opportunity
      .findUnique({ where: { id: root.id } })
      .ui_interesting_activity_hub(),
  ui_moment: (_obj, { root }: ResolverArgs<ReturnType<typeof opportunity>>) =>
    db.opportunity.findUnique({ where: { id: root.id } }).ui_moment(),
  ui_opportunity_competitor: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof opportunity>>
  ) =>
    db.opportunity
      .findUnique({ where: { id: root.id } })
      .ui_opportunity_competitor(),
  ui_opportunity_incumbent: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof opportunity>>
  ) =>
    db.opportunity
      .findUnique({ where: { id: root.id } })
      .ui_opportunity_incumbent(),
  ui_opportunity_recommendations: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof opportunity>>
  ) =>
    db.opportunity
      .findUnique({ where: { id: root.id } })
      .ui_opportunity_recommendations(),
  ui_task: (_obj, { root }: ResolverArgs<ReturnType<typeof opportunity>>) =>
    db.opportunity.findUnique({ where: { id: root.id } }).ui_task(),
}
