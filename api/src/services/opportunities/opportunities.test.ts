import {
  opportunities,
  opportunity,
  createOpportunity,
  updateOpportunity,
  deleteOpportunity,
} from './opportunities'
import type { StandardScenario } from './opportunities.scenarios'

describe('opportunities', () => {
  scenario('returns all opportunities', async (scenario: StandardScenario) => {
    const result = await opportunities()

    expect(result.length).toEqual(Object.keys(scenario.opportunity).length)
  })

  scenario(
    'returns a single opportunity',
    async (scenario: StandardScenario) => {
      const result = await opportunity({ id: scenario.opportunity.one.id })

      expect(result).toEqual(scenario.opportunity.one)
    }
  )

  scenario('creates a opportunity', async (scenario: StandardScenario) => {
    const result = await createOpportunity({
      input: {
        account_id_fk: scenario.opportunity.two.account_id_fk,
        owner_id_fk: scenario.opportunity.two.owner_id_fk,
        signature_id_fk: scenario.opportunity.two.signature_id_fk,
      },
    })

    expect(result.account_id_fk).toEqual(scenario.opportunity.two.account_id_fk)
    expect(result.owner_id_fk).toEqual(scenario.opportunity.two.owner_id_fk)
    expect(result.signature_id_fk).toEqual(
      scenario.opportunity.two.signature_id_fk
    )
  })

  scenario('updates a opportunity', async (scenario: StandardScenario) => {
    const original = await opportunity({ id: scenario.opportunity.one.id })
    const result = await updateOpportunity({
      id: original.id,
      input: { account_id_fk: scenario.opportunity.two.account_id_fk },
    })

    expect(result.account_id_fk).toEqual(scenario.opportunity.two.account_id_fk)
  })

  scenario('deletes a opportunity', async (scenario: StandardScenario) => {
    const original = await deleteOpportunity({
      id: scenario.opportunity.one.id,
    })
    const result = await opportunity({ id: original.id })

    expect(result).toEqual(null)
  })
})
