import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.OpportunityCreateArgs>({
  opportunity: {
    one: {
      data: {
        ui_opportunity_account: { create: {} },
        ui_owner: {
          create: { first_given_name: 'String', family_name: 'String' },
        },
        ui_dna_signature: {
          create: {
            ui_order: 2397279,
            ui_color: 'String',
            ui_dna: {
              create: {
                ui_account: { create: { display_name: 'String7257562' } },
              },
            },
          },
        },
      },
    },
    two: {
      data: {
        ui_opportunity_account: { create: {} },
        ui_owner: {
          create: { first_given_name: 'String', family_name: 'String' },
        },
        ui_dna_signature: {
          create: {
            ui_order: 2931627,
            ui_color: 'String',
            ui_dna: {
              create: {
                ui_account: { create: { display_name: 'String6950101' } },
              },
            },
          },
        },
      },
    },
  },
})

export type StandardScenario = typeof standard
