import type { Prisma } from '@prisma/client'

export const standard =
  defineScenario<Prisma.OpportunityRecommendationsCreateArgs>({
    opportunityRecommendations: {
      one: {
        data: {
          ui_opportunity: {
            create: {
              ui_opportunity_account: { create: {} },
              ui_owner: {
                create: { first_given_name: 'String', family_name: 'String' },
              },
              ui_dna_signature: {
                create: {
                  ui_order: 6443009,
                  ui_color: 'String',
                  ui_dna: {
                    create: {
                      ui_account: { create: { display_name: 'String8420752' } },
                    },
                  },
                },
              },
            },
          },
        },
      },
      two: {
        data: {
          ui_opportunity: {
            create: {
              ui_opportunity_account: { create: {} },
              ui_owner: {
                create: { first_given_name: 'String', family_name: 'String' },
              },
              ui_dna_signature: {
                create: {
                  ui_order: 208491,
                  ui_color: 'String',
                  ui_dna: {
                    create: {
                      ui_account: { create: { display_name: 'String880821' } },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  })

export type StandardScenario = typeof standard
