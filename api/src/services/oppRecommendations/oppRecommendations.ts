import { db } from 'src/lib/db'

export const oppRecommendations = () => {
  return db.opportunityRecommendations.findMany()
}
