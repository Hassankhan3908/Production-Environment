import { oppRecommendations } from './oppRecommendations'
import type { StandardScenario } from './oppRecommendations.scenarios'

describe('oppRecommendations', () => {
  scenario(
    'returns all oppRecommendations',
    async (scenario: StandardScenario) => {
      const result = await oppRecommendations()

      expect(result.length).toEqual(
        Object.keys(scenario.opportunityRecommendations).length
      )
    }
  )
})
