import {
  dnaSignatures,
  dnaSignature,
  createDnaSignature,
  updateDnaSignature,
  deleteDnaSignature,
} from './dnaSignatures'
import type { StandardScenario } from './dnaSignatures.scenarios'

describe('dnaSignatures', () => {
  scenario('returns all dnaSignatures', async (scenario: StandardScenario) => {
    const result = await dnaSignatures()

    expect(result.length).toEqual(Object.keys(scenario.dnaSignature).length)
  })

  scenario(
    'returns a single dnaSignature',
    async (scenario: StandardScenario) => {
      const result = await dnaSignature({ id: scenario.dnaSignature.one.id })

      expect(result).toEqual(scenario.dnaSignature.one)
    }
  )

  scenario('creates a dnaSignature', async (scenario: StandardScenario) => {
    const result = await createDnaSignature({
      input: {
        dna_id_fk: scenario.dnaSignature.two.dna_id_fk,
        ui_order: 2506016,
        ui_color: 'String',
      },
    })

    expect(result.dna_id_fk).toEqual(scenario.dnaSignature.two.dna_id_fk)
    expect(result.ui_order).toEqual(2506016)
    expect(result.ui_color).toEqual('String')
  })

  scenario('updates a dnaSignature', async (scenario: StandardScenario) => {
    const original = await dnaSignature({ id: scenario.dnaSignature.one.id })
    const result = await updateDnaSignature({
      id: original.id,
      input: { ui_order: 9273793 },
    })

    expect(result.ui_order).toEqual(9273793)
  })

  scenario('deletes a dnaSignature', async (scenario: StandardScenario) => {
    const original = await deleteDnaSignature({
      id: scenario.dnaSignature.one.id,
    })
    const result = await dnaSignature({ id: original.id })

    expect(result).toEqual(null)
  })
})
