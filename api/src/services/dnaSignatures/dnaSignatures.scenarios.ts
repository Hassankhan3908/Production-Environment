import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.DnaSignatureCreateArgs>({
  dnaSignature: {
    one: {
      data: {
        ui_order: 8278541,
        ui_color: 'String',
        ui_dna: {
          create: { ui_account: { create: { display_name: 'String8495044' } } },
        },
      },
    },
    two: {
      data: {
        ui_order: 4115501,
        ui_color: 'String',
        ui_dna: {
          create: { ui_account: { create: { display_name: 'String2452143' } } },
        },
      },
    },
  },
})

export type StandardScenario = typeof standard
