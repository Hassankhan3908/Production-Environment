import type { Prisma } from '@prisma/client'
import type { ResolverArgs } from '@redwoodjs/graphql-server'

import { db } from 'src/lib/db'

export const dnaSignatures = () => {
  return db.dnaSignature.findMany()
}

export const dnaSignature = ({ id }: Prisma.DnaSignatureWhereUniqueInput) => {
  return db.dnaSignature.findUnique({
    where: { id },
  })
}

interface CreateDnaSignatureArgs {
  input: Prisma.DnaSignatureCreateInput
}

export const createDnaSignature = ({ input }: CreateDnaSignatureArgs) => {
  return db.dnaSignature.create({
    data: input,
  })
}

interface UpdateDnaSignatureArgs extends Prisma.DnaSignatureWhereUniqueInput {
  input: Prisma.DnaSignatureUpdateInput
}

export const updateDnaSignature = ({ id, input }: UpdateDnaSignatureArgs) => {
  return db.dnaSignature.update({
    data: input,
    where: { id },
  })
}

export const deleteDnaSignature = ({
  id,
}: Prisma.DnaSignatureWhereUniqueInput) => {
  return db.dnaSignature.delete({
    where: { id },
  })
}

export const DnaSignature = {
  ui_dna: (_obj, { root }: ResolverArgs<ReturnType<typeof dnaSignature>>) =>
    db.dnaSignature.findUnique({ where: { id: root.id } }).ui_dna(),
  ui_dna_signature_forecast: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof dnaSignature>>
  ) =>
    db.dnaSignature
      .findUnique({ where: { id: root.id } })
      .ui_dna_signature_forecast(),
  ui_dna_signature_stage: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof dnaSignature>>
  ) =>
    db.dnaSignature
      .findUnique({ where: { id: root.id } })
      .ui_dna_signature_stage(),
  ui_opportunity: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof dnaSignature>>
  ) => db.dnaSignature.findUnique({ where: { id: root.id } }).ui_opportunity(),
  ui_power_score: (
    _obj,
    { root }: ResolverArgs<ReturnType<typeof dnaSignature>>
  ) => db.dnaSignature.findUnique({ where: { id: root.id } }).ui_power_score(),
}
