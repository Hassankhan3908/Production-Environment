import { db } from 'src/lib/db'

export const tasks = () => {
  return db.task.findMany()
}
