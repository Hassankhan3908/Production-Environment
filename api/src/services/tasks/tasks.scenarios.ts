import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.TaskCreateArgs>({
  task: {
    one: {
      data: {
        display_name: 'String',
        display_status: 'String',
        notes: 'String',
        ui_opportunity: {
          create: {
            ui_opportunity_account: { create: {} },
            ui_owner: {
              create: { first_given_name: 'String', family_name: 'String' },
            },
            ui_dna_signature: {
              create: {
                ui_order: 6898625,
                ui_color: 'String',
                ui_dna: {
                  create: {
                    ui_account: { create: { display_name: 'String8437173' } },
                  },
                },
              },
            },
          },
        },
      },
    },
    two: {
      data: {
        display_name: 'String',
        display_status: 'String',
        notes: 'String',
        ui_opportunity: {
          create: {
            ui_opportunity_account: { create: {} },
            ui_owner: {
              create: { first_given_name: 'String', family_name: 'String' },
            },
            ui_dna_signature: {
              create: {
                ui_order: 4623848,
                ui_color: 'String',
                ui_dna: {
                  create: {
                    ui_account: { create: { display_name: 'String6771129' } },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
})

export type StandardScenario = typeof standard
