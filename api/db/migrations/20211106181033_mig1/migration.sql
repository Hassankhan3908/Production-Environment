-- CreateEnum
CREATE TYPE "finegrain_industry" AS ENUM ('Communications', 'Electronics', 'Services', 'Technology', 'Telecommunications', 'Biotechnology', 'Healthcare', 'Healthcare Providers', 'Insurance', 'Banking', 'Finance', 'Financial', 'Engineering', 'Machinery', 'Manufacturing', 'Utilities', 'Apparel', 'Retail', 'Communications & Media', 'Entertainment', 'Education', 'Education - Higher Ed', 'Education - K-12', 'Federal Government', 'Government', 'Local Government', 'State Government', 'Agriculture', 'Commercial', 'Construction', 'Consulting', 'Energy', 'Hospitality', 'Natural Resources', 'Not For Profit', 'Other', 'Recreation', 'Special District', 'Transportation', 'Unclassified', 'Wholesale Trade');

-- CreateEnum
CREATE TYPE "industry" AS ENUM ('Tech', 'Healthcare', 'Insurance', 'Financial Services', 'Manufacturing', 'Utilities', 'Retail', 'Media & Entertainment', 'Education', 'Public Sector', 'Other');

-- CreateEnum
CREATE TYPE "outcome" AS ENUM ('WIN', 'LOSS');

-- CreateTable
CREATE TABLE "UserExample" (
    "id" SERIAL NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT,

    CONSTRAINT "UserExample_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "date_time" (
    "date_dim_id" INTEGER NOT NULL,
    "date_actual" DATE NOT NULL,
    "epoch" BIGINT NOT NULL,
    "day_suffix" VARCHAR(4) NOT NULL,
    "day_name" VARCHAR(9) NOT NULL,
    "day_of_week" INTEGER NOT NULL,
    "day_of_month" INTEGER NOT NULL,
    "day_of_quarter" INTEGER NOT NULL,
    "day_of_year" INTEGER NOT NULL,
    "week_of_month" INTEGER NOT NULL,
    "week_of_year" INTEGER NOT NULL,
    "week_of_year_iso" CHAR(10) NOT NULL,
    "month_actual" INTEGER NOT NULL,
    "month_name" VARCHAR(9) NOT NULL,
    "month_name_abbreviated" CHAR(3) NOT NULL,
    "quarter_actual" INTEGER NOT NULL,
    "quarter_name" VARCHAR(9) NOT NULL,
    "year_actual" INTEGER NOT NULL,
    "first_day_of_week" DATE NOT NULL,
    "last_day_of_week" DATE NOT NULL,
    "first_day_of_month" DATE NOT NULL,
    "last_day_of_month" DATE NOT NULL,
    "first_day_of_quarter" DATE NOT NULL,
    "last_day_of_quarter" DATE NOT NULL,
    "first_day_of_year" DATE NOT NULL,
    "last_day_of_year" DATE NOT NULL,
    "mmyyyy" CHAR(6) NOT NULL,
    "mmddyyyy" CHAR(10) NOT NULL,
    "weekend_indr" BOOLEAN NOT NULL,

    CONSTRAINT "date_time_pkey" PRIMARY KEY ("date_dim_id")
);

-- CreateTable
CREATE TABLE "ui_account" (
    "account_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "display_name" VARCHAR(40) NOT NULL,
    "effective_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "exp_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),

    CONSTRAINT "accountr_pkey" PRIMARY KEY ("account_id_pk")
);

-- CreateTable
CREATE TABLE "ui_account_attribs" (
    "attrib_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "attrib_name" VARCHAR,
    "attrib_value" VARCHAR,
    "account_id_fk" UUID,

    CONSTRAINT "ui_account_attribs_pk" PRIMARY KEY ("attrib_id_pk")
);

-- CreateTable
CREATE TABLE "ui_account_powerscore" (
    "power_score_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "type" VARCHAR(250) NOT NULL,
    "name" VARCHAR(250) NOT NULL,
    "description" VARCHAR(250),
    "dna_id_fk" UUID NOT NULL,
    "score" DOUBLE PRECISION NOT NULL,
    "effective_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "exp_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),

    CONSTRAINT "account_powerscore_pkey" PRIMARY KEY ("power_score_id_pk")
);

-- CreateTable
CREATE TABLE "ui_account_recommendations" (
    "recommendation_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "recommendation" VARCHAR(512),
    "account_id_fk" UUID NOT NULL,

    CONSTRAINT "ui_account_recommendations_pk" PRIMARY KEY ("recommendation_id_pk")
);

-- CreateTable
CREATE TABLE "ui_authuser" (
    "user_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "first" VARCHAR(25),
    "last" VARCHAR(25),
    "validation_token" UUID DEFAULT gen_random_uuid(),
    "token_issue_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "token_expiry_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),
    "email" VARCHAR(255) NOT NULL,
    "search" tsvector,
    "hashed_pw" VARCHAR(255) NOT NULL,
    "effective_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "expiry_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),
    "designation" VARCHAR(25),
    "account_id_fk" UUID NOT NULL,
    "salesforce_id" VARCHAR(255),
    "role_id_fk" BIGINT,

    CONSTRAINT "authuser_pkey" PRIMARY KEY ("user_id_pk")
);

-- CreateTable
CREATE TABLE "ui_contact" (
    "contact_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "opportunity_id_fk" UUID NOT NULL,
    "display_name" VARCHAR(250) NOT NULL,

    CONSTRAINT "contact_pkey" PRIMARY KEY ("contact_id_pk")
);

-- CreateTable
CREATE TABLE "ui_dna" (
    "dna_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "account_id_fk" UUID NOT NULL,
    "effective_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "exp_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),
    "cut_id" INTEGER,

    CONSTRAINT "dna_pkey" PRIMARY KEY ("dna_id_pk")
);

-- CreateTable
CREATE TABLE "ui_dna_signature" (
    "signature_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "dna_id_fk" UUID NOT NULL,
    "total_pipeline" DOUBLE PRECISION,
    "total_revenue" DOUBLE PRECISION,
    "pct_win_rate" DOUBLE PRECISION,
    "avg_deal_size" DOUBLE PRECISION,
    "avg_nrtr_days" DOUBLE PRECISION,
    "display_name" VARCHAR(100),
    "effective_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "exp_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),
    "ui_order" INTEGER NOT NULL,
    "ui_color" VARCHAR(7) NOT NULL,
    "classification_label" INTEGER,

    CONSTRAINT "dna_signature_pkey" PRIMARY KEY ("signature_id_pk")
);

-- CreateTable
CREATE TABLE "ui_dna_signature_forecast" (
    "dna_signature_forecast_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "signature_id_fk" UUID NOT NULL,
    "effective_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "exp_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),
    "rep_id_fk" UUID NOT NULL,
    "rep_mgr_id_fk" UUID NOT NULL,
    "avg_deal_size" DOUBLE PRECISION,
    "avg_nrtr_days" DOUBLE PRECISION,
    "pipeline" DOUBLE PRECISION,
    "pipeline_commit" DOUBLE PRECISION,
    "upside" DOUBLE PRECISION,
    "best_case" DOUBLE PRECISION,
    "pipeline_target" DOUBLE PRECISION,
    "predicted" DOUBLE PRECISION,
    "predicted_probability" DOUBLE PRECISION,
    "year" INTEGER,
    "month" INTEGER,
    "day" INTEGER,
    "quarter" INTEGER,
    "date_dim_id_fk" INTEGER,

    CONSTRAINT "dna_signature_forecast_pkey" PRIMARY KEY ("dna_signature_forecast_id_pk")
);

-- CreateTable
CREATE TABLE "ui_dna_signature_stage" (
    "dna_signature_stage_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "effective_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "exp_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),
    "phase" VARCHAR(250) NOT NULL,
    "display_name" VARCHAR(250),
    "seq_order" INTEGER NOT NULL,
    "metric_value" VARCHAR(250),
    "font_icon_desc" VARCHAR(250),
    "signature_id_fk" UUID NOT NULL,
    "ui_color" VARCHAR(7),

    CONSTRAINT "dna_signature_stage_pkey" PRIMARY KEY ("dna_signature_stage_id_pk")
);

-- CreateTable
CREATE TABLE "ui_event" (
    "event_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "opportunity_id_fk" UUID NOT NULL,
    "display_name" VARCHAR(250) NOT NULL,
    "event_date" DATE,
    "notes" VARCHAR(3000) NOT NULL,

    CONSTRAINT "event_pkey" PRIMARY KEY ("event_id_pk")
);

-- CreateTable
CREATE TABLE "ui_interesting_activity_hub" (
    "ia_hub_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "task_id_fk" UUID,
    "event_id_fk" UUID,
    "moment_id_fk" UUID,
    "opportunity_id_fk" UUID NOT NULL,

    CONSTRAINT "interesting_activity_hub_pkey" PRIMARY KEY ("ia_hub_id_pk")
);

-- CreateTable
CREATE TABLE "ui_moment" (
    "moment_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "opportunity_id_fk" UUID NOT NULL,
    "display_name" VARCHAR(250) NOT NULL,
    "event_date" DATE,
    "notes" VARCHAR(3000) NOT NULL,

    CONSTRAINT "moment_pkey" PRIMARY KEY ("moment_id_pk")
);

-- CreateTable
CREATE TABLE "ui_opportunity" (
    "opportunity_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "account_id_fk" UUID NOT NULL,
    "owner_id_fk" UUID NOT NULL,
    "projected_amt" DOUBLE PRECISION,
    "system_amt" DOUBLE PRECISION,
    "projected_close_by_date" DATE,
    "system_date" DATE,
    "predicted_outcome" "outcome",
    "system_outcome" "outcome",
    "prediction_score" DOUBLE PRECISION,
    "creation_date" DATE,
    "mkt_nrcr_days" INTEGER,
    "sls_nrcr_days" INTEGER,
    "age" INTEGER,
    "pct_actvty_complete" DOUBLE PRECISION,
    "num_actvty" INTEGER,
    "num_sls_actvty" INTEGER,
    "num_mkt_actvty" INTEGER,
    "poc_duration" INTEGER,
    "industry_type" VARCHAR(200),
    "fg_industry_type" VARCHAR(200),
    "tasks_total" INTEGER,
    "tasks_completed" INTEGER,
    "tasks_avg_weekly_rate" INTEGER,
    "tasks_avg_daily_rate" INTEGER,
    "tasks_trend_percentage" DOUBLE PRECISION,
    "engagement_score" INTEGER,
    "engagement_daily_streak" INTEGER,
    "engagement_weekly_streak" INTEGER,
    "salesforce_closing_date" DATE,
    "signature_id_fk" UUID NOT NULL,
    "opportunity_name" VARCHAR(100),
    "opportunity_source" VARCHAR(100),

    CONSTRAINT "opportunity_pkey" PRIMARY KEY ("opportunity_id_pk")
);

-- CreateTable
CREATE TABLE "ui_opportunity_account" (
    "opportunity_account_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "display_name" VARCHAR(100),
    "effective_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "exp_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),
    "heat_score" INTEGER,
    "sf_id" VARCHAR(250),

    CONSTRAINT "opportunity_account_pkey" PRIMARY KEY ("opportunity_account_id_pk")
);

-- CreateTable
CREATE TABLE "ui_opportunity_competitor" (
    "opportunity_competitor_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "opportunity_id_fk" UUID NOT NULL,
    "display_name" VARCHAR(250) NOT NULL,

    CONSTRAINT "opportunity_competitor_pkey" PRIMARY KEY ("opportunity_competitor_id_pk")
);

-- CreateTable
CREATE TABLE "ui_opportunity_incumbent" (
    "opportunity_incumbent_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "opportunity_id_fk" UUID NOT NULL,
    "display_name" VARCHAR(250) NOT NULL,

    CONSTRAINT "opportunity_incumbent_pkey" PRIMARY KEY ("opportunity_incumbent_id_pk")
);

-- CreateTable
CREATE TABLE "ui_opportunity_recommendations" (
    "recommendation_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "recommendation" VARCHAR(512),
    "opportunity_id_fk" UUID NOT NULL,

    CONSTRAINT "ui_opportunity_recommendations_pk" PRIMARY KEY ("recommendation_id_pk")
);

-- CreateTable
CREATE TABLE "ui_owner" (
    "owner_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "manager_id" UUID,
    "first_given_name" VARCHAR(250) NOT NULL,
    "second_given_name" VARCHAR(250),
    "family_name" VARCHAR(250) NOT NULL,
    "sf_id" VARCHAR(250),
    "manager_sf_id" VARCHAR(250),
    "org_tree" JSON,

    CONSTRAINT "ui_owner_pkey" PRIMARY KEY ("owner_id_pk")
);

-- CreateTable
CREATE TABLE "ui_power_score" (
    "power_score_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "type" VARCHAR(250) NOT NULL,
    "name" VARCHAR(250) NOT NULL,
    "description" VARCHAR(250),
    "signature_id_fk" UUID NOT NULL,
    "score" DOUBLE PRECISION NOT NULL,
    "effective_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "exp_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),

    CONSTRAINT "power_score_pkey" PRIMARY KEY ("power_score_id_pk")
);

-- CreateTable
CREATE TABLE "ui_role" (
    "role_id" BIGINT NOT NULL,
    "salesforce_permission" VARCHAR,
    "description" VARCHAR,

    CONSTRAINT "ui_role_pk" PRIMARY KEY ("role_id")
);

-- CreateTable
CREATE TABLE "ui_role_permissions" (
    "permission_id" UUID NOT NULL,
    "permission_name" VARCHAR(255),
    "role_id_fk" BIGINT,

    CONSTRAINT "ui_role_permissions_pk" PRIMARY KEY ("permission_id")
);

-- CreateTable
CREATE TABLE "ui_session" (
    "session_id" UUID NOT NULL DEFAULT gen_random_uuid(),
    "token" UUID NOT NULL DEFAULT gen_random_uuid(),
    "ip" INET,
    "user_id_fk" UUID NOT NULL,
    "effective_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, now()),
    "expiry_date" TIMESTAMP(6) DEFAULT timezone('utc'::text, 'infinity'::timestamp without time zone),
    "access_token" VARCHAR(255),

    CONSTRAINT "session_pkey" PRIMARY KEY ("session_id")
);

-- CreateTable
CREATE TABLE "ui_task" (
    "task_id_pk" UUID NOT NULL DEFAULT gen_random_uuid(),
    "opportunity_id_fk" UUID NOT NULL,
    "display_name" VARCHAR(250) NOT NULL,
    "sla" INTEGER,
    "due" DATE,
    "due_in_days" INTEGER,
    "display_status" VARCHAR(250) NOT NULL,
    "notes" VARCHAR(3000) NOT NULL,
    "current_value" DECIMAL,
    "min_target_value" DECIMAL,
    "max_target_value" DECIMAL,
    "priority" INTEGER,

    CONSTRAINT "task_pkey" PRIMARY KEY ("task_id_pk")
);

-- CreateIndex
CREATE UNIQUE INDEX "UserExample_email_key" ON "UserExample"("email");

-- CreateIndex
CREATE INDEX "date_time_date_actual_idx" ON "date_time"("date_actual");

-- CreateIndex
CREATE UNIQUE INDEX "account_name_key" ON "ui_account"("display_name");

-- CreateIndex
CREATE INDEX "ui_account_attribs_account_id_fk_idx" ON "ui_account_attribs"("account_id_fk", "attrib_value", "attrib_name");

-- CreateIndex
CREATE UNIQUE INDEX "ui_authuser_email_key" ON "ui_authuser"("email");

-- CreateIndex
CREATE INDEX "ui_opp_acct_sf_id_idx" ON "ui_opportunity_account"("sf_id");

-- CreateIndex
CREATE INDEX "ui_owner_mgrsf_id_idx" ON "ui_owner"("manager_sf_id");

-- CreateIndex
CREATE INDEX "ui_owner_sf_id_idx" ON "ui_owner"("sf_id");

-- CreateIndex
CREATE UNIQUE INDEX "ui_session_token_key" ON "ui_session"("token");

-- AddForeignKey
ALTER TABLE "ui_account_attribs" ADD CONSTRAINT "ui_account_attribs_fk" FOREIGN KEY ("account_id_fk") REFERENCES "ui_opportunity_account"("opportunity_account_id_pk") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "ui_account_powerscore" ADD CONSTRAINT "power_score__dna__fkey" FOREIGN KEY ("dna_id_fk") REFERENCES "ui_dna"("dna_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_account_recommendations" ADD CONSTRAINT "ui_account_recommendations_fk" FOREIGN KEY ("account_id_fk") REFERENCES "ui_opportunity_account"("opportunity_account_id_pk") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "ui_authuser" ADD CONSTRAINT "dna__account__fkey" FOREIGN KEY ("account_id_fk") REFERENCES "ui_account"("account_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_authuser" ADD CONSTRAINT "role_id__fkey" FOREIGN KEY ("role_id_fk") REFERENCES "ui_role"("role_id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_contact" ADD CONSTRAINT "contact__opportunity__fkey" FOREIGN KEY ("opportunity_id_fk") REFERENCES "ui_opportunity"("opportunity_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_dna" ADD CONSTRAINT "dna__account__fkey" FOREIGN KEY ("account_id_fk") REFERENCES "ui_account"("account_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_dna_signature" ADD CONSTRAINT "dna_signature__dna__fkey" FOREIGN KEY ("dna_id_fk") REFERENCES "ui_dna"("dna_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_dna_signature_forecast" ADD CONSTRAINT "dna_signature_forecast__dna_signature__fkey" FOREIGN KEY ("signature_id_fk") REFERENCES "ui_dna_signature"("signature_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_dna_signature_stage" ADD CONSTRAINT "dna_signature_stage__dna_signature__fkey" FOREIGN KEY ("signature_id_fk") REFERENCES "ui_dna_signature"("signature_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_event" ADD CONSTRAINT "event__opportunity__fkey" FOREIGN KEY ("opportunity_id_fk") REFERENCES "ui_opportunity"("opportunity_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_interesting_activity_hub" ADD CONSTRAINT "interesting_activity_hub__opportunity__fkey" FOREIGN KEY ("opportunity_id_fk") REFERENCES "ui_opportunity"("opportunity_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_moment" ADD CONSTRAINT "moment__opportunity__fkey" FOREIGN KEY ("opportunity_id_fk") REFERENCES "ui_opportunity"("opportunity_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_opportunity" ADD CONSTRAINT "opportunity__opportunity_account__fkey" FOREIGN KEY ("account_id_fk") REFERENCES "ui_opportunity_account"("opportunity_account_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_opportunity" ADD CONSTRAINT "opportunity__owner__fkey" FOREIGN KEY ("owner_id_fk") REFERENCES "ui_owner"("owner_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_opportunity" ADD CONSTRAINT "ui_opportunity_fk" FOREIGN KEY ("signature_id_fk") REFERENCES "ui_dna_signature"("signature_id_pk") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "ui_opportunity_competitor" ADD CONSTRAINT "opportunity_competitor__opportunity__fkey" FOREIGN KEY ("opportunity_id_fk") REFERENCES "ui_opportunity"("opportunity_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_opportunity_incumbent" ADD CONSTRAINT "opportunity_incumbent__opportunity__fkey" FOREIGN KEY ("opportunity_id_fk") REFERENCES "ui_opportunity"("opportunity_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_opportunity_recommendations" ADD CONSTRAINT "ui_opportunity_recommendations_fk" FOREIGN KEY ("opportunity_id_fk") REFERENCES "ui_opportunity"("opportunity_id_pk") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "ui_owner" ADD CONSTRAINT "ui_owner__manager__fkey" FOREIGN KEY ("manager_id") REFERENCES "ui_owner"("owner_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_power_score" ADD CONSTRAINT "power_score__dna_signature__fkey" FOREIGN KEY ("signature_id_fk") REFERENCES "ui_dna_signature"("signature_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ui_role_permissions" ADD CONSTRAINT "ui_role_permissions_fk" FOREIGN KEY ("role_id_fk") REFERENCES "ui_role"("role_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "ui_session" ADD CONSTRAINT "session__authuser__fkey" FOREIGN KEY ("user_id_fk") REFERENCES "ui_authuser"("user_id_pk") ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "ui_task" ADD CONSTRAINT "task__opportunity__fkey" FOREIGN KEY ("opportunity_id_fk") REFERENCES "ui_opportunity"("opportunity_id_pk") ON DELETE CASCADE ON UPDATE CASCADE;
