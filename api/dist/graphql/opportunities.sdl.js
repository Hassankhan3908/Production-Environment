var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  schema: () => schema
});
var import_graphql_tag = __toModule(require("graphql-tag"));
const schema = import_graphql_tag.default`
  type OpportunitiesPaginated {
    opportunities: [Opportunity!]!
    count: Int!
  }
  type Opportunity {
    id: String!
    account_id_fk: String!
    owner_id_fk: String!
    projected_amt: Float
    system_amt: Float
    projected_close_by_date: DateTime
    system_date: DateTime
    predicted_outcome: Outcome
    system_outcome: Outcome
    prediction_score: Float
    creation_date: DateTime
    mkt_nrcr_days: Int
    sls_nrcr_days: Int
    age: Int
    pct_actvty_complete: Float
    num_actvty: Int
    num_sls_actvty: Int
    num_mkt_actvty: Int
    poc_duration: Int
    industry_type: String
    fg_industry_type: String
    tasks_total: Int
    tasks_completed: Int
    tasks_avg_weekly_rate: Int
    tasks_avg_daily_rate: Int
    tasks_trend_percentage: Float
    engagement_score: Int
    engagement_daily_streak: Int
    engagement_weekly_streak: Int
    salesforce_closing_date: DateTime
    signature_id_fk: String!
    opportunity_name: String
    opportunity_source: String
    ui_opportunity_account: OpportunityAccount!
    ui_owner: Owner!
    ui_dna_signature: DnaSignature!
    #ui_contact: [Contact]!
    #ui_event: [Event]!
    ui_interesting_activity_hub: [InterestingActivityHub]!
    #ui_moment: [Moment]!
    ui_opportunity_competitor: [OpportunityCompetitor]!
    ui_opportunity_incumbent: [OpportunityIncumbent]!
    ui_opportunity_recommendations: [OpportunityRecommendations]!
    ui_task: [Task]!
  }

  enum Outcome {
    WIN
    LOSS
  }
  enum Outcome {
    WIN
    LOSS
  }

  type Query {
    opportunities: [Opportunity!]! @requireAuth
    opportunity(id: String!): Opportunity @requireAuth
    filterOpportunities(
      page: Int
      sfId: String
      theater: String
    ): OpportunitiesPaginated! @requireAuth
  }

  input FilterOpportunitiesInput {
    sfId: String
    theater: String
  }
  input CreateOpportunityInput {
    account_id_fk: String!
    owner_id_fk: String!
    projected_amt: Float
    system_amt: Float
    projected_close_by_date: DateTime
    system_date: DateTime
    predicted_outcome: Outcome
    system_outcome: Outcome
    prediction_score: Float
    creation_date: DateTime
    mkt_nrcr_days: Int
    sls_nrcr_days: Int
    age: Int
    pct_actvty_complete: Float
    num_actvty: Int
    num_sls_actvty: Int
    num_mkt_actvty: Int
    poc_duration: Int
    industry_type: String
    fg_industry_type: String
    tasks_total: Int
    tasks_completed: Int
    tasks_avg_weekly_rate: Int
    tasks_avg_daily_rate: Int
    tasks_trend_percentage: Float
    engagement_score: Int
    engagement_daily_streak: Int
    engagement_weekly_streak: Int
    salesforce_closing_date: DateTime
    signature_id_fk: String!
    opportunity_name: String
    opportunity_source: String
  }

  input UpdateOpportunityInput {
    account_id_fk: String
    owner_id_fk: String
    projected_amt: Float
    system_amt: Float
    projected_close_by_date: DateTime
    system_date: DateTime
    predicted_outcome: Outcome
    system_outcome: Outcome
    prediction_score: Float
    creation_date: DateTime
    mkt_nrcr_days: Int
    sls_nrcr_days: Int
    age: Int
    pct_actvty_complete: Float
    num_actvty: Int
    num_sls_actvty: Int
    num_mkt_actvty: Int
    poc_duration: Int
    industry_type: String
    fg_industry_type: String
    tasks_total: Int
    tasks_completed: Int
    tasks_avg_weekly_rate: Int
    tasks_avg_daily_rate: Int
    tasks_trend_percentage: Float
    engagement_score: Int
    engagement_daily_streak: Int
    engagement_weekly_streak: Int
    salesforce_closing_date: DateTime
    signature_id_fk: String
    opportunity_name: String
    opportunity_source: String
  }

  type Mutation {
    createOpportunity(input: CreateOpportunityInput!): Opportunity! @requireAuth
    updateOpportunity(
      id: String!
      input: UpdateOpportunityInput!
    ): Opportunity! @requireAuth
    deleteOpportunity(id: String!): Opportunity! @requireAuth
  }
`;
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  schema
});
//# sourceMappingURL=opportunities.sdl.js.map
