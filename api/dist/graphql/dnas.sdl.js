var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  schema: () => schema
});
var import_graphql_tag = __toModule(require("graphql-tag"));
const schema = import_graphql_tag.default`
  type Dna {
    id: String!
    account_id_fk: String!
    effective_date: DateTime
    exp_date: DateTime
    cut_id: Int
    ui_account: Account!
    ui_account_powerscore: [AccountPowerscore]!
    ui_dna_signature: [DnaSignature]!
  }

  type Query {
    dnas: [Dna!]! @requireAuth
    dna(id: String!): Dna @requireAuth
  }

  input CreateDnaInput {
    account_id_fk: String!
    effective_date: DateTime
    exp_date: DateTime
    cut_id: Int
  }

  input UpdateDnaInput {
    account_id_fk: String
    effective_date: DateTime
    exp_date: DateTime
    cut_id: Int
  }

  type Mutation {
    createDna(input: CreateDnaInput!): Dna! @requireAuth
    updateDna(id: String!, input: UpdateDnaInput!): Dna! @requireAuth
    deleteDna(id: String!): Dna! @requireAuth
  }
`;
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  schema
});
//# sourceMappingURL=dnas.sdl.js.map
