var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  schema: () => schema
});
var import_graphql_tag = __toModule(require("graphql-tag"));
const schema = import_graphql_tag.default`
  type DnaSignature {
    id: String!
    dna_id_fk: String!
    total_pipeline: Float
    total_revenue: Float
    pct_win_rate: Float
    avg_deal_size: Float
    avg_nrtr_days: Float
    display_name: String
    effective_date: DateTime
    exp_date: DateTime
    ui_order: Int!
    ui_color: String!
    classification_label: Int
    ui_dna: Dna!
    ui_dna_signature_forecast: [DnaSignatureForecast]!
    ui_dna_signature_stage: [DnaSignatureStage]!
    ui_opportunity: [Opportunity]!
    ui_power_score: [PowerScore]!
  }

  type Query {
    dnaSignatures: [DnaSignature!]! @requireAuth
    dnaSignature(id: String!): DnaSignature @requireAuth
  }

  input CreateDnaSignatureInput {
    dna_id_fk: String!
    total_pipeline: Float
    total_revenue: Float
    pct_win_rate: Float
    avg_deal_size: Float
    avg_nrtr_days: Float
    display_name: String
    effective_date: DateTime
    exp_date: DateTime
    ui_order: Int!
    ui_color: String!
    classification_label: Int
  }

  input UpdateDnaSignatureInput {
    dna_id_fk: String
    total_pipeline: Float
    total_revenue: Float
    pct_win_rate: Float
    avg_deal_size: Float
    avg_nrtr_days: Float
    display_name: String
    effective_date: DateTime
    exp_date: DateTime
    ui_order: Int
    ui_color: String
    classification_label: Int
  }

  type Mutation {
    createDnaSignature(input: CreateDnaSignatureInput!): DnaSignature!
      @requireAuth
    updateDnaSignature(
      id: String!
      input: UpdateDnaSignatureInput!
    ): DnaSignature! @requireAuth
    deleteDnaSignature(id: String!): DnaSignature! @requireAuth
  }
`;
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  schema
});
//# sourceMappingURL=dnaSignatures.sdl.js.map
