var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  schema: () => schema
});
var import_graphql_tag = __toModule(require("graphql-tag"));
const schema = import_graphql_tag.default`
  type DnaSignatureForecast {
    id: String!
    signature_id_fk: String!
    effective_date: DateTime
    exp_date: DateTime
    rep_id_fk: String!
    rep_mgr_id_fk: String!
    avg_deal_size: Float
    avg_nrtr_days: Float
    pipeline: Float
    pipeline_commit: Float
    upside: Float
    best_case: Float
    pipeline_target: Float
    predicted: Float
    predicted_probability: Float
    year: Int
    month: Int
    day: Int
    quarter: Int
    date_dim_id_fk: Int
    ui_dna_signature: DnaSignature!
  }

  type Query {
    dnaSignatureForecasts: [DnaSignatureForecast!]! @requireAuth
    dnaSignatureForecast(id: String!): DnaSignatureForecast @requireAuth
  }

  input CreateDnaSignatureForecastInput {
    signature_id_fk: String!
    effective_date: DateTime
    exp_date: DateTime
    rep_id_fk: String!
    rep_mgr_id_fk: String!
    avg_deal_size: Float
    avg_nrtr_days: Float
    pipeline: Float
    pipeline_commit: Float
    upside: Float
    best_case: Float
    pipeline_target: Float
    predicted: Float
    predicted_probability: Float
    year: Int
    month: Int
    day: Int
    quarter: Int
    date_dim_id_fk: Int
  }

  input UpdateDnaSignatureForecastInput {
    signature_id_fk: String
    effective_date: DateTime
    exp_date: DateTime
    rep_id_fk: String
    rep_mgr_id_fk: String
    avg_deal_size: Float
    avg_nrtr_days: Float
    pipeline: Float
    pipeline_commit: Float
    upside: Float
    best_case: Float
    pipeline_target: Float
    predicted: Float
    predicted_probability: Float
    year: Int
    month: Int
    day: Int
    quarter: Int
    date_dim_id_fk: Int
  }

  type Mutation {
    createDnaSignatureForecast(
      input: CreateDnaSignatureForecastInput!
    ): DnaSignatureForecast! @requireAuth
    updateDnaSignatureForecast(
      id: String!
      input: UpdateDnaSignatureForecastInput!
    ): DnaSignatureForecast! @requireAuth
    deleteDnaSignatureForecast(id: String!): DnaSignatureForecast! @requireAuth
  }
`;
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  schema
});
//# sourceMappingURL=dnaSignatureForecasts.sdl.js.map
