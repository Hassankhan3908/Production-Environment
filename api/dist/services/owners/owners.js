var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  Owner: () => Owner,
  createOwner: () => createOwner,
  deleteOwner: () => deleteOwner,
  owner: () => owner,
  ownerCount: () => ownerCount,
  owners: () => owners,
  updateOwner: () => updateOwner
});
var import_db = __toModule(require("..\\..\\lib\\db"));
const owners = () => {
  return import_db.db.owner.findMany({
    where: {
      ui_opportunity: {
        some: {
          NOT: {
            id: void 0
          }
        }
      }
    }
  });
};
const owner = ({
  id
}) => {
  return import_db.db.owner.findFirst({
    where: {
      sf_id: id
    }
  });
};
const ownerCount = () => {
  return import_db.db.owner.count({
    where: {
      ui_opportunity: {
        some: {
          NOT: {
            id: void 0
          }
        }
      }
    }
  });
};
const createOwner = ({
  input
}) => {
  return import_db.db.owner.create({
    data: input
  });
};
const updateOwner = ({
  id,
  input
}) => {
  return import_db.db.owner.update({
    data: input,
    where: {
      id
    }
  });
};
const deleteOwner = ({
  id
}) => {
  return import_db.db.owner.delete({
    where: {
      id
    }
  });
};
const Owner = {
  ui_owner: (_obj, {
    root
  }) => import_db.db.owner.findUnique({
    where: {
      id: root.id
    }
  }).ui_owner(),
  ui_opportunity: (_obj, {
    root
  }) => import_db.db.owner.findUnique({
    where: {
      id: root.id
    }
  }).ui_opportunity(),
  other_ui_owner: (_obj, {
    root
  }) => import_db.db.owner.findUnique({
    where: {
      id: root.id
    }
  }).other_ui_owner()
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  Owner,
  createOwner,
  deleteOwner,
  owner,
  ownerCount,
  owners,
  updateOwner
});
//# sourceMappingURL=owners.js.map
