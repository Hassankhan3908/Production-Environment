var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  Opportunity: () => Opportunity,
  createOpportunity: () => createOpportunity,
  deleteOpportunity: () => deleteOpportunity,
  filterOpportunities: () => filterOpportunities,
  opportunities: () => opportunities,
  opportunity: () => opportunity,
  updateOpportunity: () => updateOpportunity
});
var import_db = __toModule(require("..\\..\\lib\\db"));
const RECS_PER_PAGE = 8;
const opportunities = () => {
  return import_db.db.opportunity.findMany({
    take: RECS_PER_PAGE
  });
};
const filterOpportunities = ({
  page,
  sfId,
  theater
}) => {
  const offset = (page - 1) * RECS_PER_PAGE;
  const whereClause = {
    ui_opportunity_account: theater ? {
      ui_account_attribs: {
        some: {
          attrib_name: "theater",
          attrib_value: theater
        }
      }
    } : void 0,
    ui_owner: sfId ? {
      sf_id: sfId
    } : void 0
  };
  return {
    opportunities: import_db.db.opportunity.findMany({
      take: RECS_PER_PAGE,
      skip: offset,
      where: whereClause
    }),
    count: import_db.db.opportunity.count({
      where: whereClause
    })
  };
};
const opportunity = ({
  id
}) => {
  return import_db.db.opportunity.findUnique({
    where: {
      id
    }
  });
};
const createOpportunity = ({
  input
}) => {
  return import_db.db.opportunity.create({
    data: input
  });
};
const updateOpportunity = ({
  id,
  input
}) => {
  return import_db.db.opportunity.update({
    data: input,
    where: {
      id
    }
  });
};
const deleteOpportunity = ({
  id
}) => {
  return import_db.db.opportunity.delete({
    where: {
      id
    }
  });
};
const Opportunity = {
  ui_opportunity_account: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_opportunity_account(),
  ui_owner: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_owner(),
  ui_dna_signature: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_dna_signature(),
  ui_contact: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_contact(),
  ui_event: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_event(),
  ui_interesting_activity_hub: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_interesting_activity_hub(),
  ui_moment: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_moment(),
  ui_opportunity_competitor: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_opportunity_competitor(),
  ui_opportunity_incumbent: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_opportunity_incumbent(),
  ui_opportunity_recommendations: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_opportunity_recommendations(),
  ui_task: (_obj, {
    root
  }) => import_db.db.opportunity.findUnique({
    where: {
      id: root.id
    }
  }).ui_task()
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  Opportunity,
  createOpportunity,
  deleteOpportunity,
  filterOpportunities,
  opportunities,
  opportunity,
  updateOpportunity
});
//# sourceMappingURL=opportunities.js.map
