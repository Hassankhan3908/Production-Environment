var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  Account: () => Account,
  account: () => account,
  accounts: () => accounts,
  createAccount: () => createAccount,
  deleteAccount: () => deleteAccount,
  updateAccount: () => updateAccount
});
var import_db = __toModule(require("..\\..\\lib\\db"));
const accounts = () => {
  return import_db.db.account.findMany();
};
const account = ({
  id
}) => {
  return import_db.db.account.findUnique({
    where: {
      id
    }
  });
};
const createAccount = ({
  input
}) => {
  return import_db.db.account.create({
    data: input
  });
};
const updateAccount = ({
  id,
  input
}) => {
  return import_db.db.account.update({
    data: input,
    where: {
      id
    }
  });
};
const deleteAccount = ({
  id
}) => {
  return import_db.db.account.delete({
    where: {
      id
    }
  });
};
const Account = {
  ui_authuser: (_obj, {
    root
  }) => import_db.db.account.findUnique({
    where: {
      id: root.id
    }
  }).ui_authuser(),
  ui_dna: (_obj, {
    root
  }) => import_db.db.account.findUnique({
    where: {
      id: root.id
    }
  }).ui_dna()
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  Account,
  account,
  accounts,
  createAccount,
  deleteAccount,
  updateAccount
});
//# sourceMappingURL=accounts.js.map
