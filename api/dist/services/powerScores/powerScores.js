var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  PowerScore: () => PowerScore,
  createPowerScore: () => createPowerScore,
  deletePowerScore: () => deletePowerScore,
  powerScore: () => powerScore,
  powerScores: () => powerScores,
  updatePowerScore: () => updatePowerScore
});
var import_db = __toModule(require("..\\..\\lib\\db"));
const powerScores = () => {
  return import_db.db.powerScore.findMany();
};
const powerScore = ({
  id
}) => {
  return import_db.db.powerScore.findUnique({
    where: {
      id
    }
  });
};
const createPowerScore = ({
  input
}) => {
  return import_db.db.powerScore.create({
    data: input
  });
};
const updatePowerScore = ({
  id,
  input
}) => {
  return import_db.db.powerScore.update({
    data: input,
    where: {
      id
    }
  });
};
const deletePowerScore = ({
  id
}) => {
  return import_db.db.powerScore.delete({
    where: {
      id
    }
  });
};
const PowerScore = {
  ui_dna_signature: (_obj, {
    root
  }) => import_db.db.powerScore.findUnique({
    where: {
      id: root.id
    }
  }).ui_dna_signature()
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  PowerScore,
  createPowerScore,
  deletePowerScore,
  powerScore,
  powerScores,
  updatePowerScore
});
//# sourceMappingURL=powerScores.js.map
