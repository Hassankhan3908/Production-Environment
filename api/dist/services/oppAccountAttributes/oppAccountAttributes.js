var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  AccountAttributes: () => AccountAttributes,
  accountAttributes: () => accountAttributes,
  accountTheaters: () => accountTheaters,
  createAccountAttributes: () => createAccountAttributes,
  deleteAccountAttributes: () => deleteAccountAttributes,
  oppAccountAttributes: () => oppAccountAttributes,
  updateAccountAttributes: () => updateAccountAttributes
});
var import_db = __toModule(require("..\\..\\lib\\db"));
const oppAccountAttributes = () => {
  return import_db.db.accountAttributes.findMany();
};
const accountTheaters = () => {
  return import_db.db.accountAttributes.findMany({
    where: {
      attrib_name: "theater",
      NOT: {
        attrib_value: null
      }
    },
    distinct: ["attrib_value"],
    select: {
      attrib_value: true
    }
  });
};
const accountAttributes = ({
  id
}) => {
  return import_db.db.accountAttributes.findUnique({
    where: {
      id
    }
  });
};
const createAccountAttributes = ({
  input
}) => {
  return import_db.db.accountAttributes.create({
    data: input
  });
};
const updateAccountAttributes = ({
  id,
  input
}) => {
  return import_db.db.accountAttributes.update({
    data: input,
    where: {
      id
    }
  });
};
const deleteAccountAttributes = ({
  id
}) => {
  return import_db.db.accountAttributes.delete({
    where: {
      id
    }
  });
};
const AccountAttributes = {
  ui_opportunity_account: (_obj, {
    root
  }) => import_db.db.accountAttributes.findUnique({
    where: {
      id: root.id
    }
  }).ui_opportunity_account()
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  AccountAttributes,
  accountAttributes,
  accountTheaters,
  createAccountAttributes,
  deleteAccountAttributes,
  oppAccountAttributes,
  updateAccountAttributes
});
//# sourceMappingURL=oppAccountAttributes.js.map
