var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  Dna: () => Dna,
  createDna: () => createDna,
  deleteDna: () => deleteDna,
  dna: () => dna,
  dnas: () => dnas,
  updateDna: () => updateDna
});
var import_db = __toModule(require("..\\..\\lib\\db"));
const dnas = () => {
  return import_db.db.dna.findMany();
};
const dna = ({
  id
}) => {
  return import_db.db.dna.findUnique({
    where: {
      id
    }
  });
};
const createDna = ({
  input
}) => {
  return import_db.db.dna.create({
    data: input
  });
};
const updateDna = ({
  id,
  input
}) => {
  return import_db.db.dna.update({
    data: input,
    where: {
      id
    }
  });
};
const deleteDna = ({
  id
}) => {
  return import_db.db.dna.delete({
    where: {
      id
    }
  });
};
const Dna = {
  ui_account: (_obj, {
    root
  }) => import_db.db.dna.findUnique({
    where: {
      id: root.id
    }
  }).ui_account(),
  ui_account_powerscore: (_obj, {
    root
  }) => import_db.db.dna.findUnique({
    where: {
      id: root.id
    }
  }).ui_account_powerscore(),
  ui_dna_signature: (_obj, {
    root
  }) => import_db.db.dna.findUnique({
    where: {
      id: root.id
    }
  }).ui_dna_signature()
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  Dna,
  createDna,
  deleteDna,
  dna,
  dnas,
  updateDna
});
//# sourceMappingURL=dnas.js.map
