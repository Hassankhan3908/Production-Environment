var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  OpportunityAccount: () => OpportunityAccount,
  createOpportunityAccount: () => createOpportunityAccount,
  deleteOpportunityAccount: () => deleteOpportunityAccount,
  filterOpportunityAccounts: () => filterOpportunityAccounts,
  opportunityAccount: () => opportunityAccount,
  opportunityAccounts: () => opportunityAccounts,
  updateOpportunityAccount: () => updateOpportunityAccount
});
var import_db = __toModule(require("..\\..\\lib\\db"));
const RECS_PER_PAGE = 8;
const opportunityAccounts = () => {
  return import_db.db.opportunityAccount.findMany({
    take: 10
  });
};
const filterOpportunityAccounts = ({
  page,
  sfId
}) => {
  const offset = (page - 1) * RECS_PER_PAGE;
  const whereClause = sfId ? {
    sf_id: sfId
  } : void 0;
  return {
    opportunityAccounts: import_db.db.opportunityAccount.findMany({
      take: RECS_PER_PAGE,
      skip: offset,
      where: whereClause
    }),
    count: import_db.db.opportunityAccount.count({
      where: whereClause
    })
  };
};
const opportunityAccount = ({
  id
}) => {
  return import_db.db.opportunityAccount.findUnique({
    where: {
      id
    }
  });
};
const createOpportunityAccount = ({
  input
}) => {
  return import_db.db.opportunityAccount.create({
    data: input
  });
};
const updateOpportunityAccount = ({
  id,
  input
}) => {
  return import_db.db.opportunityAccount.update({
    data: input,
    where: {
      id
    }
  });
};
const deleteOpportunityAccount = ({
  id
}) => {
  return import_db.db.opportunityAccount.delete({
    where: {
      id
    }
  });
};
const OpportunityAccount = {
  ui_account_attribs: (_obj, {
    root
  }) => import_db.db.opportunityAccount.findUnique({
    where: {
      id: root.id
    }
  }).ui_account_attribs(),
  ui_account_recommendations: (_obj, {
    root
  }) => import_db.db.opportunityAccount.findUnique({
    where: {
      id: root.id
    }
  }).ui_account_recommendations(),
  ui_opportunity: (_obj, {
    root
  }) => import_db.db.opportunityAccount.findUnique({
    where: {
      id: root.id
    }
  }).ui_opportunity()
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  OpportunityAccount,
  createOpportunityAccount,
  deleteOpportunityAccount,
  filterOpportunityAccounts,
  opportunityAccount,
  opportunityAccounts,
  updateOpportunityAccount
});
//# sourceMappingURL=opportunityAccounts.js.map
