var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
__export(exports, {
  handler: () => handler
});
var import_graphql_server = __toModule(require("@redwoodjs/graphql-server"));
var directives_requireAuth_requireAuth = __toModule(require("../directives/requireAuth/requireAuth"));
var directives_skipAuth_skipAuth = __toModule(require("../directives/skipAuth/skipAuth"));
var sdls_accountPowerscores_sdl = __toModule(require("../graphql/accountPowerscores.sdl"));
var sdls_accounts_sdl = __toModule(require("../graphql/accounts.sdl"));
var sdls_accRecommendations_sdl = __toModule(require("../graphql/accRecommendations.sdl"));
var sdls_dnas_sdl = __toModule(require("../graphql/dnas.sdl"));
var sdls_dnaSignatureForecasts_sdl = __toModule(require("../graphql/dnaSignatureForecasts.sdl"));
var sdls_dnaSignatures_sdl = __toModule(require("../graphql/dnaSignatures.sdl"));
var sdls_dnaSignatureStages_sdl = __toModule(require("../graphql/dnaSignatureStages.sdl"));
var sdls_interestingActivityHubs_sdl = __toModule(require("../graphql/interestingActivityHubs.sdl"));
var sdls_oppAccountAttributes_sdl = __toModule(require("../graphql/oppAccountAttributes.sdl"));
var sdls_opportunities_sdl = __toModule(require("../graphql/opportunities.sdl"));
var sdls_opportunityAccounts_sdl = __toModule(require("../graphql/opportunityAccounts.sdl"));
var sdls_opportunityCompetitors_sdl = __toModule(require("../graphql/opportunityCompetitors.sdl"));
var sdls_opportunityIncumbents_sdl = __toModule(require("../graphql/opportunityIncumbents.sdl"));
var sdls_oppRecommendations_sdl = __toModule(require("../graphql/oppRecommendations.sdl"));
var sdls_owners_sdl = __toModule(require("../graphql/owners.sdl"));
var sdls_powerScores_sdl = __toModule(require("../graphql/powerScores.sdl"));
var sdls_tasks_sdl = __toModule(require("../graphql/tasks.sdl"));
var services_accountPowerscores_accountPowerscores = __toModule(require("../services/accountPowerscores/accountPowerscores"));
var services_accounts_accounts = __toModule(require("../services/accounts/accounts"));
var services_accRecommendations_accRecommendations = __toModule(require("../services/accRecommendations/accRecommendations"));
var services_dnas_dnas = __toModule(require("../services/dnas/dnas"));
var services_dnaSignatureForecasts_dnaSignatureForecasts = __toModule(require("../services/dnaSignatureForecasts/dnaSignatureForecasts"));
var services_dnaSignatures_dnaSignatures = __toModule(require("../services/dnaSignatures/dnaSignatures"));
var services_dnaSignatureStages_dnaSignatureStages = __toModule(require("../services/dnaSignatureStages/dnaSignatureStages"));
var services_interestingActivityHubs_interestingActivityHubs = __toModule(require("../services/interestingActivityHubs/interestingActivityHubs"));
var services_oppAccountAttributes_oppAccountAttributes = __toModule(require("../services/oppAccountAttributes/oppAccountAttributes"));
var services_opportunities_opportunities = __toModule(require("../services/opportunities/opportunities"));
var services_opportunityAccounts_opportunityAccounts = __toModule(require("../services/opportunityAccounts/opportunityAccounts"));
var services_opportunityCompetitors_opportunityCompetitors = __toModule(require("../services/opportunityCompetitors/opportunityCompetitors"));
var services_opportunityIncumbents_opportunityIncumbents = __toModule(require("../services/opportunityIncumbents/opportunityIncumbents"));
var services_oppRecommendations_oppRecommendations = __toModule(require("../services/oppRecommendations/oppRecommendations"));
var services_owners_owners = __toModule(require("../services/owners/owners"));
var services_powerScores_powerScores = __toModule(require("../services/powerScores/powerScores"));
var services_tasks_tasks = __toModule(require("../services/tasks/tasks"));
var import_auth = __toModule(require("..\\lib\\auth"));
var import_db = __toModule(require("..\\lib\\db"));
var import_logger = __toModule(require("..\\lib\\logger"));
let directives = {};
directives.requireAuth_requireAuth = directives_requireAuth_requireAuth;
directives.skipAuth_skipAuth = directives_skipAuth_skipAuth;
let sdls = {};
sdls.accountPowerscores_sdl = sdls_accountPowerscores_sdl;
sdls.accounts_sdl = sdls_accounts_sdl;
sdls.accRecommendations_sdl = sdls_accRecommendations_sdl;
sdls.dnas_sdl = sdls_dnas_sdl;
sdls.dnaSignatureForecasts_sdl = sdls_dnaSignatureForecasts_sdl;
sdls.dnaSignatures_sdl = sdls_dnaSignatures_sdl;
sdls.dnaSignatureStages_sdl = sdls_dnaSignatureStages_sdl;
sdls.interestingActivityHubs_sdl = sdls_interestingActivityHubs_sdl;
sdls.oppAccountAttributes_sdl = sdls_oppAccountAttributes_sdl;
sdls.opportunities_sdl = sdls_opportunities_sdl;
sdls.opportunityAccounts_sdl = sdls_opportunityAccounts_sdl;
sdls.opportunityCompetitors_sdl = sdls_opportunityCompetitors_sdl;
sdls.opportunityIncumbents_sdl = sdls_opportunityIncumbents_sdl;
sdls.oppRecommendations_sdl = sdls_oppRecommendations_sdl;
sdls.owners_sdl = sdls_owners_sdl;
sdls.powerScores_sdl = sdls_powerScores_sdl;
sdls.tasks_sdl = sdls_tasks_sdl;
let services = {};
services.accountPowerscores_accountPowerscores = services_accountPowerscores_accountPowerscores;
services.accounts_accounts = services_accounts_accounts;
services.accRecommendations_accRecommendations = services_accRecommendations_accRecommendations;
services.dnas_dnas = services_dnas_dnas;
services.dnaSignatureForecasts_dnaSignatureForecasts = services_dnaSignatureForecasts_dnaSignatureForecasts;
services.dnaSignatures_dnaSignatures = services_dnaSignatures_dnaSignatures;
services.dnaSignatureStages_dnaSignatureStages = services_dnaSignatureStages_dnaSignatureStages;
services.interestingActivityHubs_interestingActivityHubs = services_interestingActivityHubs_interestingActivityHubs;
services.oppAccountAttributes_oppAccountAttributes = services_oppAccountAttributes_oppAccountAttributes;
services.opportunities_opportunities = services_opportunities_opportunities;
services.opportunityAccounts_opportunityAccounts = services_opportunityAccounts_opportunityAccounts;
services.opportunityCompetitors_opportunityCompetitors = services_opportunityCompetitors_opportunityCompetitors;
services.opportunityIncumbents_opportunityIncumbents = services_opportunityIncumbents_opportunityIncumbents;
services.oppRecommendations_oppRecommendations = services_oppRecommendations_oppRecommendations;
services.owners_owners = services_owners_owners;
services.powerScores_powerScores = services_powerScores_powerScores;
services.tasks_tasks = services_tasks_tasks;
const handler = (0, import_graphql_server.createGraphQLHandler)({
  getCurrentUser: import_auth.getCurrentUser,
  loggerConfig: {
    logger: import_logger.logger,
    options: {}
  },
  directives,
  sdls,
  services,
  onException: () => {
    import_db.db.$disconnect();
  }
});
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  handler
});
//# sourceMappingURL=graphql.js.map
