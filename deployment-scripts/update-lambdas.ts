import AWS from 'aws-sdk';
import fs from 'fs';
import path from 'path';

console.log(process.argv);
console.log(__dirname);
const paths = path.join(__dirname, '../api/zipballs');
console.log(paths)
let count=0
async function main() {
  try {
    fs.readdir(`${paths}`, function (err, files) {
      //handling error
      if (err) {
        return console.log('Unable to scan directory: ' + err);
      }
      const lambda = new AWS.Lambda({ region: 'us-east-1' });
      //listing all files using forEach
      files.forEach(function (file) {
        lambda.listFunctions().send((err, data) => {
          if (err) {
            console.error(err);
          } else {
            data.Functions?.forEach((f) => {
              lambda.listTags(
                { Resource: f.FunctionArn || '' },
                (err, tags) => {
                  // console.log(tags);
                  // console.log(typeof tags?.Tags,"counter");
                  // let apple=tags?.Tags
                  // count=count+1;
                  // console.log(count)
                  // console.log(file);

                //TODO: create the new SRC_FILE_NAME tag while creating lambda through terraform
                if(tags?.Tags?.SRC_BRANCH != undefined &&  tags?.Tags?.SRC_FILE_NAME != undefined){
                  // console.log(process.argv[2])
                  // console.log(tags?.Tags?.SRC_FILE_NAME,"apple")
                  // console.log(file.split('.zip')[0])
                  // console.log(tags?.Tags?.SRC_FILE_NAME == file.split('.zip')[0],tags?.Tags?.SRC_FILE_NAME)
                if (tags?.Tags?.SRC_BRANCH == process.argv[2] && tags?.Tags?.SRC_FILE_NAME == file.split('.zip')[0]) {
                    console.log(
                      `Updating function ${f.FunctionName} using bucketname: ${process.argv[3]} + filename:${process.argv[2]}`
                    );
                    setTimeout(function () {
                      if (f.FunctionName == file.split('.zip')[0]) {
                        console.log(file);
                        lambda
                          .updateFunctionCode({
                            FunctionName: f.FunctionName!,
                            S3Bucket: process.argv[3],
                            S3Key: file,
                          })
                          .send((err) => {
                            if (err) {
                              console.error(err);
                            } else {
                              setTimeout(function () {
                                lambda
                                  .publishVersion({
                                    FunctionName: f.FunctionName!,
                                  })
                                  .send((err) => {
                                    if (err) {
                                      console.error(
                                        'Publish failed with error'
                                      );
                                      console.error(err);
                                    } else {
                                      console.log('successfully published');
                                    }
                                  });
                              }, 5000);
                            }
                          });
                      }
                    }, 10000);
                  } else {
                    console.error(
                      `The ${err} part is ${tags?.Tags?.SRC_BRANCH}`
                    );
                  }
                }
                }
              );
            });
          }
        });

        // console.log(file.split('.zip')[0]);
      });
    });
  } catch (err) {
    console.log('Oops: extractZip failed', err);
  }
}
main();
