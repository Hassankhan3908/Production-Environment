import AWS from 'aws-sdk';
import fs from 'fs';

console.log(process.argv);
const Get_Name = process.argv[2];
const Branch_Name = Get_Name.replace('/', '-') + ".hassan.ivl.test";
fs.writeFileSync('bucketname.txt', Branch_Name);
console.log(Branch_Name);

//TODO: pass region as a parameters
AWS.config.update({ region: 'us-east-1' });
const s3 = new AWS.S3();

let bucketParams = {
  Bucket: Branch_Name,
};

s3.headBucket(
  {
    Bucket: Branch_Name,
  },
  function (err, data) {
    if (err) {
      s3.createBucket(bucketParams, function (err, data) {
        if (err) {
          console.log('Error');
        } else {
          console.log('Success', data.Location);
        }
      });
    } else {
      console.log('Bucket Already Exist');
    }
  }
);
