import AWS from 'aws-sdk';
import { CreateInvalidationRequest, Paths } from 'aws-sdk/clients/cloudfront';
console.log(process.argv);
const branchName = process.argv[2];
const bucketName = branchName.replace('/', '-');

console.log(`bucket name for invalidation ${bucketName}`);
//TODO: pass region as a parameters
AWS.config.update({ region: 'us-east-1' });
const cloudFront = new AWS.CloudFront();

cloudFront.listDistributions((err, result) => {
  if (err) {
    console.log('No distributions found');
  } else {
    result.DistributionList?.Items?.forEach((distSummary) => {
      console.log(`Checking origins of ${distSummary.Id}`);
      distSummary.Origins.Items.filter((origin) => {
        console.log(
          `Evaluating Origin with domain name ${
            origin.DomainName
          } domain startsWith check ${origin.DomainName.startsWith(
            bucketName
          )} `
        );
        console.log(origin.S3OriginConfig);
        return (
          origin.S3OriginConfig && origin.DomainName.startsWith(bucketName)
        );
      }).forEach((origin) => {
        console.log(`Creating invalidation for origin ${origin.DomainName}`);
        const invalidationPath: Paths = { Quantity: 1, Items: ['/*'] };
        const invalidationRequest: CreateInvalidationRequest = {
          DistributionId: distSummary.Id,
          InvalidationBatch: {
            Paths: invalidationPath,
            CallerReference: new Date().toDateString(),
          },
        };
        cloudFront.createInvalidation(invalidationRequest).send((err, data) => {
          if (err) {
            console.error('Failed to create invalidation', err);
          } else {
            console.log(`Invalidation created for ${distSummary.Id}`);
          }
        });
      });
    });
  }
});
